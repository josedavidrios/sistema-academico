




function filtrarEstudiante() {

    event.preventDefault();


    var nombres = $('#filtro-estudiante').val();


    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/filtrarEstudiante",
        data: {nombres: nombres},
        success: function (resp) {

            $('#agrega-registros').html(resp);
        }
    });



}




function consultarInscripciones() {

    event.preventDefault();

    $.ajax({

        url: $('#form-consulta').attr("action"),
        type: "POST",
        data: $('#form-consulta').serialize(),

        success: function (resp) {



            // valores = eval(resp);
            $('#consulta-por-tipo').html(resp);
       //     $('#datatable-tipos-propuesta').DataTable();







        }, error: function () {

            alert("Error");

        }
    });



}




function consultarMatriculas() {

    event.preventDefault();

    $.ajax({

        url: BASE_URL+'admin/consultarMatriculas',
        type: "POST",
        data: $('#form-consulta').serialize(),

        success: function (resp) {



            // valores = eval(resp);
            $('#consulta-por-tipo').html(resp);
            //     $('#datatable-tipos-propuesta').DataTable();



         $("#datatable-matriculas").DataTable();



        }, error: function () {

            alert("Error");

        }
    });



}





$('#eps').select2({
    placeholder: 'SELECCIONE UNA EPS',
    minimumInputLength: 1,
    dropdownParent: $("#modal-registrar"),
    theme: "bootstrap",
    width: '100%',
    ajax: {
        url: BASE_URL+"usuario/filtrarEPS/",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});

function abrirModalRestablecerClave(documento) {




    $.ajax({
        url: BASE_URL + "admin/consultarEstudiante",
        type: "POST",
        data: {documento: documento},
        success: function (resp) {

            var estudiante = JSON.parse(resp)[0];

            $("#documento-restablecer-clave").val(estudiante.documento);
            $("#nueva-clave").val(estudiante.documento);


            abrirModal("modal-restablecer-clave");


        }, error: function () {

            alert("Error");

        }
    });


}


function abrirModalEditarEstudiante(documento) {




    $.ajax({
        url: BASE_URL + "admin/consultarEstudiante",
        type: "POST",
        data: {documento: documento},
        success: function (resp) {


            var estudiante = JSON.parse(resp)[0];

            $("#documento").val(estudiante.documento);




            $("#nombres").val(estudiante.nombres);
            $("#nuevo-documento").val(estudiante.documento);
            $("#cambio-clave").val(estudiante.cambio_clave);

            $("#apellidos").val(estudiante.apellidos);
            $("#fecha-nacimiento").val(estudiante.fecha_nacimiento);
            $("#sexo").val(estudiante.sexo);
            $("#direccion").val(estudiante.direccion);
            $("#barrio").val(estudiante.barrio);
            $("#tipo-documento").val(estudiante.tipo_documento);
            $("#tipo-sangre").val(estudiante.tipo_sangre);
            $("#zona").val(estudiante.zona);
            $("#estrato").val(estudiante.estrato);
            $("#sisben").val(estudiante.sisben);
            $("#ips").val(estudiante.ips);
            $("#eps").val(estudiante.eps);
            $("#lugar-residencia").val(estudiante.lugar_residencia);
            $("#lugar-expedicion").val(estudiante.lugar_expedicion);

            $("#correo").val(estudiante.correo);
            $("#celular").val(estudiante.celular);
            $("#titulo").val(estudiante.titulo);




            $('#lugar-expedicion-documento').html('<option value="' + estudiante.lugar_expedicion_documento + '">' + estudiante.municipio_expedicion_documento + '</option>');
            $("#lugar-expedicion-documento").val(estudiante.lugar_expedicion_documento);








            $('#lugar-residencia').html('<option value="' + estudiante.lugar_residencia + '">' + estudiante.municipio_residencia + '</option>');
            $("#lugar-residencia").val(estudiante.lugar_residencia);


            $('#lugar-nacimiento').html('<option value="' + estudiante.lugar_nacimiento + '">' + estudiante.municipio_nacimiento + '</option>');
            $("#lugar-nacimiento").val(estudiante.lugar_nacimiento);

            $('#eps').html('<option value="' + estudiante.eps + '">' + estudiante.nombre_eps + '</option>');
            $("#eps").val(estudiante.eps);

            $("#estado-civil").val(estudiante.estado_civil);
            $("#nivel-educacion").val(estudiante.nivel_educacion);


            $('#estado-civil,#nivel-educacion ').trigger('change');






            abrirModal("modal-registrar");


        }, error: function () {

            alert("Error");

        }
    });


}

$(".mi-select2").select2({

    dropdownParent: $("#modal-registrar"),
});

$('#institucion').select2({
    placeholder: 'BUSCAR INSTITUCIÓN',
    theme: "bootstrap",
    minimumInputLength: 1,
    dropdownParent: $("#modal-registrar"),
    ajax: {
        url: BASE_URL + "admin/filtrarInstitucion",
        data: function (params) {

            return {
                nombre: params.term, // search term
                //page: params.id
            };
        },
        processResults: function (data) {
            return {
                results: $.map(JSON.parse(data), function (obj) {
                    return {id: obj.codigo, text: obj.nombre + " - " + obj.municipio};
                })
            };
        },
        cache: true
    }
});



$('.lugares').select2({
    placeholder: 'BUSCAR',
    theme: "bootstrap",

    minimumInputLength: 1,
    allowClear:true,
    dropdownParent: $("#modal-registrar"),

    ajax: {
        url: BASE_URL + "admin/filtrarMunicipios",
        data: function (params) {
            //console.log(params);
            return {
                nombre: params.term, // search term
                //page: params.id
            };
        },
        processResults: function (data) {
            return {
                results: $.map(JSON.parse(data), function (obj) {
                    return {id: obj.codigo, text: obj.nombre+" - "+obj.dpto };
                })
            };
        },
        cache: true
    }
});


function consultarEstudiante() {

   var documento = $("#documento").val();

   console.log(documento);


   if(documento!="" && documento!=undefined){



    $.ajax({
        url: BASE_URL + "admin/consultarEstudiante",
        type: "POST",
        data: {documento: documento},
        success: function (resp) {

            var estudiante = eval(resp);





            $.each(estudiante, function (i, item) {

                $("#documento").val(estudiante[i].documento);
                $("#nombres").val(estudiante[i].apellidos_nombres);

                $("#fecha-nacimiento").val(estudiante[i].fecha_nacimiento);
                $("#tipo-documento").val(estudiante[i].tipo_documento);
                $("#correo").val(estudiante[i].correo);
                $("#sexo").val(estudiante[i].sexo);
                $("#direccion").val(estudiante[i].direccion);

                $("#telefono").val(estudiante[i].telefono_fijo);
                $("#celular").val(estudiante[i].telefono_celular);



                $('#municipio').html('<option value="' + estudiante[i].municipio + '">' + estudiante[i].nombre_municipio+ '</option>');


                $("#municipio").val(estudiante[i].municipio);


                deshablitarCamposDelFormularioDeInscripcion(true);


            });


            /*
            $("#titulo-modal").html("Edición de estudiantes");

            $('#operacion').val("editar");
            $('#bt-operacion').val("Editar");

            abrirModal("modal-registrar");

            */


        }, error: function () {

            alert("Error");

        }
    });

   }

}




