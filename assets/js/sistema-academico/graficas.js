
function graficaCantidadEvaluacionesDocentesPeriodos() {


    Chart.defaults.global.legend.display = false;
    var labels = [];
    var data = [];





    $.getJSON(BASE_URL + "admin/consultarCantidadDeEvaluacionesDocentes/", function (result) {
        $.each(result, function (i, field) {


            labels.push(field.periodo);
            data.push(field.cant);

        });
    });



    var myChart = new Chart($("#chart-evaluaciones-docente"), {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: [

                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [

                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {

            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        maxRotation: 90,
                        minRotation: 80
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });


}



function graficaMatriculasPeriodos() {


    Chart.defaults.global.legend.display = false;
    var labels = [];
    var data = [];





    $.getJSON(BASE_URL + "admin/consultarMatriculasPorPeriodos/", function (result) {
        $.each(result, function (i, field) {


            labels.push(field.periodo);
            data.push(field.cant);

        });
    });



    var myChart = new Chart($("#matriculas-periodos"), {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: [

                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [

                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {

            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        maxRotation: 90,
                        minRotation: 80
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });


}


function graficasCantidadSesionesAlSistema() {


    Chart.defaults.global.legend.display = false;

    var meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var cantidades = [];
    var nombresMeses = [];


    $.getJSON(BASE_URL + "admin/consultarVisitasAlSistema/", function (result) {
        $.each(result, function (i, field) {


            nombresMeses.push(meses[field.mes]);
            cantidades.push(field.cant);

        });
    });


    var myChart = new Chart($("#sesiones-sistema"), {
        type: 'bar',
        data: {
            labels: nombresMeses,
            datasets: [{
                data: cantidades,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {

            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        maxRotation: 90,
                        minRotation: 80
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });


}



function graficasCantidadInscripciones(){

    var cantidadesInscriciones=[];
    var estadosInscriciones=[];

    $.getJSON(BASE_URL+"admin/consultarCantidadInscripciones/", function(result){
        $.each(result, function(i, item){


            cantidadesInscriciones.push(item.cant);

            if (item.matriculado=="SI"){

                estadosInscriciones.push("MATRÍCULADOS");

            }else{

                estadosInscriciones.push("NO MATRÍCULADOS");


            }





        });
    });




    var pieChart2 = new Chart($("#chart-inscripciones"), {
        type: 'doughnut',
        data: {
            labels: estadosInscriciones,
            datasets: [
                {
                    data: cantidadesInscriciones,
                    backgroundColor: [
                        "#5DADE2",
                        "#F39C12",


                    ]
                }]
        }
    });





}

function graficasVisistasAlSistemaSistemasOperativos(){



    var cantidadesVisistasPorSO = [];
    var nombresSO = [];


    var cant=0;

    $.getJSON(BASE_URL + "admin/consultarVisitasAlsistemaPorPlataforma/", function (result) {
        $.each(result, function (i, item) {

            cant+=item.cant;


            cantidadesVisistasPorSO.push(item.cant);
            nombresSO.push(item.plataforma);


        });
    });



    var pieChart3 = new Chart($("#chart-visitaa-so"), {
        type: 'doughnut',
        data: {
            labels: nombresSO,
            datasets: [
                {
                    data: cantidadesVisistasPorSO,
                    backgroundColor: [
                        "#5DADE2",
                        "#F39C12",
                        "#85929E",
                        "#16A085",
                        "#F1948A",
                        "#F9E79F",
                        "#8E44AD",
                        "#E6B0AA",
                        "#D5F5E3",


                    ]
                }]
        }
    });


}


$(document).ready(function () {


    $("#menu_toggle").click();

    graficaMatriculasPeriodos();

    graficasCantidadSesionesAlSistema();

    graficasCantidadInscripciones();

    graficasVisistasAlSistemaSistemasOperativos();
    graficaCantidadEvaluacionesDocentesPeriodos();



    $("#menu_toggle").click();




});