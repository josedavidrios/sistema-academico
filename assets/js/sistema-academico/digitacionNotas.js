notas = [];

function terminarRegistroDeNotas() {


    event.preventDefault();


    var corte = $("#corte").val();
    var carga= $("#carga").val();

    var programa= $("#codigo-programa").val();


    $.ajax({
        type: 'POST',
        url: BASE_URL+"docente/registrarNota",
        data:  {carga:carga,corte:corte,notas:notas},
        success: function (resp) {


            if(parseInt(resp) >0){


                notas=[];

                redirecionar(corte,programa);


            }else {





                $.confirm({

                    title: 'Advertencia',
                    icon: 'fa fa-warning',
                    type: 'orange',
                    content: 'No se presentaron cambios en las notas, ¿desea contunuar?',
                    buttons: {
                        "SÍ": function(){


                            redirecionar(corte,programa);


                        }
                    }
                });



            }



        }, error: function () {

            alert("Error: Por favor comuníquese al correo: soporte@ebah.edu.co");

        }
    });


}

function redirecionar(corte,programa) {

    if (corte==="0"){


        window.location.href = BASE_URL+"docente/notas/s/asignatura/"+programa;

    } else{

        window.location.href = BASE_URL+"docente/notas/lv/asignatura/"+programa;
    }

}

function registrarNota(matricula, nota) {

    if(parseInt(nota)>=1 && parseInt(nota)<=5){

        notas.push(matricula+"-"+nota);

        $("#confirmacion").prop('checked', false);

    }




}
