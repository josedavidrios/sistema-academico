var docenteDocumento;
var jornadaCodigo;
var asignatura;
var numeroGrupo;
var nombreDocente;

$(document).ready(function () {

    $("#listado-carga-academica").hide();




    $(".select2").select2({

        theme: "bootstrap"

    });

    $("#nuevo-docente").select2({
        dropdownParent: $("#modal-cambiar-docente"),
        theme: "bootstrap"

    });


    $('#docente').select2({
        placeholder: 'BUSCAR EL DOCENTE',
        minimumInputLength: 1,
        theme: "bootstrap",

        ajax: {
            url: BASE_URL+"admin/filtrarDocente2",
            data: function (params) {
                //console.log(params);
                return {
                    q: params.term, // search term
                    //page: params.id
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (obj) {
                        return {id: obj.documento, text: obj.nombres};
                    })
                };
            },
            cache: true
        }
    });



});

$('#docente').on('select2:select', function (e) {

    docenteDocumento = e.params.data.id;

    nombreDocente= e.params.data.text;



});



function selecionarDocentePlanillas(documento) {





    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/consultarJorndasCargasAcademicas",
        data: {documento:documento},

        success: function (resp) {



            $("#agrega-registros").html(resp);



        }
    });




}

function selecionarDocente(documento) {




    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/consultarCargaAcademicaPorDocente",
        data: {documento:documento},

        success: function (resp) {



            $("#agrega-registros").html(resp);



        }
    });




}


function seleccionarCargaAcademica2(codigo) {


    $("#codigo-carga").val(codigo);

  abrirModal("modal-cambiar-docente");

}


function seleccionarCargaAcademica(codigo) {



    $.confirm({
        title: 'Sistema Académico',
        content: '¿Está seguro que desea remover la carga académica?',
        icon: 'fa fa-question-circle',
        buttons: {
            confirmar: function () {




                $.ajax({
                    type: 'POST',
                    url: BASE_URL + "admin/removerCargaAcademica",
                    data: {codigo: codigo},
                    success: function (resp) {



                        if (resp == "1") {

                            mjsConfirmacion("La carga académica se ha removido con éxito");


                            $("#"+codigo).html("");
                            //consultarAsignaturasSemestrales();

                        }


                    } ,error:function () {

                        alert("Error");
                    }
                });


            },
            cancelar: function () {

            }
        },


    });

}







$('#grupos').on('select2:select', function (e) {

    var data = e.params.data;

    var programa = $("#programa").val();
    var semestre = $("#numero-semestre").val();
    var jornada = data.id;
    var grupo = data.title;


    console.log(programa);
    console.log(semestre);
    console.log(grupo);
    console.log(jornada);


    if (programa != "" && semestre != "" && jornada != "" && grupo != "") {


        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/consultarAsignaturasPorGrupos",
            data: {programa: programa, semestre: semestre, jornada: jornada},
            success: function (resp) {



                jornadaCodigo = jornada;
                numeroGrupo = grupo;


                console.log(resp);



                $("#asignaturas").html('<option value="">SELECCIONE: </option>');




                $.each(eval(resp), function (key, value) {




                    var option = '<option value="' + value.codigo + '">' + value.asignatura + '</option>';
                    $("#asignaturas").append(option);



                }); // close each()




            }, error: function () {

                alert("error");
            }
        });


    }


});

$('#asignaturas').on('select2:select', function (e) {

    var data = e.params.data;

    asignatura = data.id;


    var x= "Jornada: "+jornadaCodigo+" Docente "+docenteDocumento+" Asignatura "+asignatura +" grupo: "+numeroGrupo;


   // alert(x);


});



function cambiarDocenteCargaAcademica() {

    event.preventDefault();

    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/cambiarDocenteCargaAcademica",
        data: $("#form-cambio-docente").serialize(),
        success: function (resp) {






            mjsConfirmacion("La carga académica se ha editado correctamente");


            cerrarModal("modal-cambiar-docente");



        }, error:function () {

            mjsError("La carga académica NO  se pudo realizar")

        }
    });



}

function registrarCargarAcademica() {

    event.preventDefault();

    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/registrarCargaAcademica",
        data: {docenteDocumento: docenteDocumento,jornadaCodigo:jornadaCodigo,asignatura:asignatura,numeroGrupo:numeroGrupo},
        success: function (resp) {




            if (resp==="-1"){


                mjsError("La carga académica que intemtar realizar ya existe");


            } else if (resp==="0"){


                mjsError("La carga académica que intenta asignar ya la tiene otro docente");


            }  else{

                mjsConfirmacion("La carga académica se ha realizado correctamente");

            }








        }, error:function () {

            mjsError("La carga académica NO  se pudo realizar")

        }
    });



}


function seleccionarGruposPorAsignatura() {

    event.preventDefault();

    var semestre = $("#numero-semestre").val();
    var programa = $("#programa").val();



    if (semestre != "") {

        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/consultarGruposPorPrograma",
            data: {programa: programa, semestre: semestre},
            success: function (resp) {

                $("#grupos").html('<option value="">SELECCIONE: </option>');







                var json=eval(resp);

              //  console.log(json);

                $.each(json, function (key, value) {

                    var option = '<option value="'+value.jornada + '"  name="'+value.semestre+'"  title="'+value.grupo+'">'+value.nombre+ '</option>';
                    $("#grupos").append(option);

                }); // close each()








            }
        });

    }

}



function VerFormularioCrearCargaAcademica() {



    $("#form-carga-academica").show();



}




function consultarCargaAcademica() {

    var documento = $('#documento-docente').val();


    $.ajax({
        type: 'POST',
        url: BASE_URL + "docente/consultarCargaAcademica",
        data: {documento: documento},
        success: function (resp) {


            $('#agrega-registros').html(resp);
        }
    });


    return false;
}

function qutarDocente() {


    $("#filtro-docente").prop("disabled", false);
    $("#filtro-docente").val("");
    $("#filtro-docente").focus();
    $("#documento-docente").val("");

    $('#agrega-registros').html("");

}


