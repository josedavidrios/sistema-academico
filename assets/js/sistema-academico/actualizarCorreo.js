$(document).ready(function () {



    $.confirm({
        title: 'Datos',
        content: 'Hemos detectado que no has registrado un correo electrónico.' +
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '<label></label>' +
        '<input type="text" placeholder="puedes actualizarlo aquí." class="name form-control" required />' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Actualizar',
                btnClass: 'btn-blue',
                action: function () {
                    var name = this.$content.find('.name').val();
                    if (!name) {
                        $.alert('provide a valid name');
                        return false;
                    }

                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });







});