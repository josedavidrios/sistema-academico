

function abrirModalBuscarEstudiante() {



    abrirModal("modal-buscar-estudiante");


}


$('#nombres-usuarios').on('keyup', function () {

    var nombres = $('#nombres-usuarios').val();



    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/filtrarEstudianteParaMatricula",
        data: {nombres: nombres},
        success: function (datos) {
            $('#agrega-usuarios').html(datos);
        }
    });



    return false;
});


function seleccionarEstudiante(documento, nombresApellidos) {

    $("#documento").val(documento);
    $("#nombres-apellidos").val(nombresApellidos);


    $("#btn-matricular").prop("disabled", false);

    cerrarModal("modal-buscar-estudiante");




    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/consultarAsignaturasMatriculadas",
        data: {documento: documento},
        success: function (datos) {
            $('#notas').html(datos);



        },

        error:function () {

            alert("Error");
        }

    });


}

$( document ).ready(function() {


    $("#grupo").select2({

        placeholder: 'BUSCAR',
        theme: "bootstrap"

    });


});


function consultarGruposPorPerido(codigo) {


    event.preventDefault();


    $.ajax({


        type: "POST",
        url: BASE_URL + "admin/consultarGrupoPorPeriodo",
        data: {codigo:codigo},
        success: function (resp) {


            $("#grupo").html(resp);






        },

        error: function () {
            alert("Error");

        }

    });



    $('#grupo').on('select2:select', function (e) {


      var codigo = $("#grupo").val();

        $("#btn-generar-reporte" ).prop( "disabled", false );


        var formato = $("#formato").val();



        if (formato=="excel"){


            $("#btn-generar-reporte" ).prop( "href", BASE_URL+'reporte/notasGrupalExcel/'+codigo);

        }else if (formato=="pdf") {

            $("#btn-generar-reporte" ).prop( "href", BASE_URL+'reporte/notasGrupalPDF/'+codigo);

        }


    });




}



