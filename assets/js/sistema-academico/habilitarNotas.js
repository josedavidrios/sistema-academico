


function abrirModalBuscarEstudiante() {



    abrirModal("modal-buscar-estudiante");


}




$('#nombres-usuarios').on('keyup', function () {

    var nombres = $('#nombres-usuarios').val();



    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/filtrarEstudianteParaMatricula",
        data: {nombres: nombres},
        success: function (datos) {
            $('#agrega-usuarios').html(datos);
        }
    });



    return false;
});


function seleccionarEstudiante(documento, nombresApellidos) {

    $("#documento").val(documento);
    $("#nombres-apellidos").val(nombresApellidos);


    var jornada = $("#jornada").val();

    $("#btn-matricular").prop("disabled", false);

    cerrarModal("modal-buscar-estudiante");




    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/consultarAsignaturasParaHabilitar",
        data: {documento: documento},
        success: function (datos) {
            $('#notas').html(datos);



        },

        error:function () {

            alert("Error");
        }

    });


}


function verModalEditarNotas(codigo) {



    $("#documento-estudiante").val($("#documento").val());


    /*

    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/consultarNotas",
        data: {codigo: codigo},
        success: function (resp) {




            var nota = eval(resp);

            $.each(nota, function (i, item) {

                $("#nota1").val(nota[i].nota1);
                $("#nota2").val(nota[i].nota2);
                $("#nota3").val(nota[i].nota3);



            });




        },

        error:function () {

            alert("Error");
        }

    });


    */



    abrirModal("editar-notas");

    $("#codigo").val(codigo);

}