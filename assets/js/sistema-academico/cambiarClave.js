

function comprobarClaves() {

    var claveNueva = $('#clave_nueva').val();
    var claveConfirmada = $('#clave_confirmada').val();

    if (claveConfirmada != claveNueva && claveConfirmada!="") {

        $(".claves-nuevas").addClass("has-error");
        $(".claves-nuevas").prop("title","Las claves de acceso no coinciden");


        $('input[type="submit"]').attr('disabled', 'disabled');

    } else if(claveConfirmada==claveNueva) {

        $("#c").removeClass("has-error");

        $('#mensaje').html("");

        $('input[type="submit"]').removeAttr('disabled','disabled');
    }


  //  alert("Clave nueva :"+claveNueva+" - "+"Clave confirmada :" +claveConfirmada );

    return false;
}



