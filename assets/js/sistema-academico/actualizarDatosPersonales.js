$(document).ready(function () {


    $('.lugares').select2({
        placeholder: 'SELECCIONE UN MUNICIPIO',
        minimumInputLength: 1,
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: BASE_URL+"usuario/filtrarMunicipios/",
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });


    $('.eps').select2({
        placeholder: 'SELECCIONE UNA EPS',
        minimumInputLength: 1,
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: BASE_URL+"usuario/filtrarEPS/",
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });



});