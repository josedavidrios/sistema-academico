/**
 * Created by José Ríos on 30/09/2018.
 */






$( document ).ready(function() {


    $("#grupo").select2({


        placeholder: 'SELECIONAR GRUPO',
        theme: "bootstrap"

    });

    $("#curso").select2({


        placeholder: 'SELECIONAR CURSOS',
        theme: "bootstrap"

    });

    $("#asignaturas").select2({


        placeholder: 'BUSCAR',
        theme: "bootstrap"

    });




    $("#nuevo-grupo").select2({
        dropdownParent: $("#editar-matricula"),
        placeholder: 'BUSCAR',
        theme: "bootstrap"

    });


});



function matricularAsignaturaIndivual() {

    event.preventDefault();


    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/matricularAsignaturasIndividuales",
        data: $('#form-matricula').serialize(),
        success: function (resp) {


            if(resp=="1"){
                mjsConfirmacion("Matricula realizado con éxito");

                $('#form-matricula-primer-ingreso')[0].reset();
                //  $("#lugar-residencia").select2("val", "");

                $(".select2-reset").val('').trigger('change');

            }else if(resp=="-1"){

                mjsError("El estudiante ya se encuentra matriculado en ese grupo");

            }





        }, error:function () {

            alert("Error");
        }
    });



}





function matriculasPrimerIngreso() {

    event.preventDefault();


    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/matriculasPrimerIngreso",
        data: $('#form-matricula-primer-ingreso').serialize(),
        success: function (resp) {


            if(resp=="1"){
                mjsConfirmacion("Matricula realizado con éxito");

                $('#form-matricula-primer-ingreso')[0].reset();
              //  $("#lugar-residencia").select2("val", "");

                $(".select2-reset").val('').trigger('change');

            }else if(resp=="-1"){

                mjsError("El estudiante ya se encuentra matriculado en ese grupo");

            }





        }, error:function () {

            alert("Error");
        }
    });



}


function buscarGrupo() {


    var grupo = $('#grupo').val();


    if (grupo != "") {

        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/consultarDetallesDeGrupo",
            data: {grupo: grupo},
            success: function (resp) {


                var grupo = eval(resp);


                $.each(grupo, function (i, item) {


                    $("#programa").val(grupo[i].programa);
                    $("#jornada").val(grupo[i].jornada);
                    $("#semestre").val(grupo[i].semestre);
                    $("#numero").val(grupo[i].grupo);
                    $("#codigo-programa").val(grupo[i].codigo_programa);


                });

            }, error: function () {

                alert("Error");
            }

        });

    }


    return false;


}


function consultarMatriculasEstudiantes() {


    var documento = $('#documento').val();


    if (documento != "") {

        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/visualizarProgramasMatriculados",
            data: {documento: documento},
            success: function (resp) {


                $("#listado").html(resp);


            }, error: function () {

                alert("Error");
            }

        });

    }


    return false;


}


function consultarEstudiante(documento) {





    if (documento.length>0){


    $.ajax({
        url: BASE_URL + "admin/consultarEstudiante",
        type: "POST",
        data: {documento: documento},
        success: function (resp) {





            var estudiante = JSON.parse(resp)[0];


            if (estudiante != undefined){




            $("#documento").val(estudiante.documento);


            $("#nombres").val(estudiante.nombres);
            $("#apellidos").val(estudiante.apellidos);
            $("#fecha-nacimiento").val(estudiante.fecha_nacimiento);
            $("#sexo").val(estudiante.sexo);
            $("#direccion").val(estudiante.direccion);
            $("#barrio").val(estudiante.barrio);
            $("#celular").val(estudiante.celular);
            $("#correo").val(estudiante.correo);
            $("#tipo-documento").val(estudiante.tipo_documento);
            $("#estrato").val(estudiante.estrato);
       //     $("#lugar-residencia").html('<span class="select2-selection__rendered" id="select2-lugar-residencia-container" title="ABEJORRAL, ANTIOQUIA">ABEJORRAL, ANTIOQUIA</span>');
            $("#nivel-educacion").val(estudiante.nivel_educacion);
            $("#zona").val(estudiante.zona);
            $("#tipo-sangre").val(estudiante.tipo_sangre);
                $("#estado-civil").val(estudiante.estado_civil);



                $('#lugar-residencia')
                    .empty()
                    .append("<option selected value="+estudiante.lugar_residencia+">"+estudiante.municipio_residencia+"</option>");
                $('#lugar-residencia').trigger('change');

                $('#lugar-nacimiento')
                    .empty()
                    .append("<option selected value="+estudiante.lugar_nacimiento+">"+estudiante.municipio_nacimiento+"</option>");
                $('#lugar-nacimiento').trigger('change');






            }
        }, error: function () {

            alert("Error");

        }
    });


    }

}


$('.lugares').select2({
    placeholder: 'SELECCIONE UN MUNICIPIO',
    minimumInputLength: 1,
    theme: "bootstrap",
    width: '100%',
    ajax: {
        url: BASE_URL+"/admin/filtrarMunicipios/",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});


$('#eps').select2({
    placeholder: 'SELECCIONE UNA EPS',
    minimumInputLength: 1,
    theme: "bootstrap",
    width: '100%',
    ajax: {
        url: BASE_URL+"usuario/filtrarEPS/",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});


/*

$('#municipio').select2({


    templateResult: format,
    placeholder: 'BUSCAR',
    theme: "bootstrap",

    ajax: {
        url: BASE_URL+"/admin/filtrarMunicipios/",
        dataType: 'json',
        data: function (params) {
            var query = {
                term: params.term
            }
            return query;
        },
        delay: 200,
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true,
        error:function () {

            console.log("Error");
        }
    }
});

function format (result) {
    var $state = $(

        '<span><strong>'+' '+result.text +' - '+ result.dpto+'</strong></span>'

    );
    return $state;
};

*/

function consultarEstudiantesPorGrupo(grupo) {


    var estado = $("#estado").val();



    if (grupo != "") {

        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/consultarEstudiantesPorGrupo",
            data: {grupo: grupo, estado:estado},
            success: function (resp) {


                $("#listado").html(resp);

                $("#btn-exportar-excel" ).prop( "disabled", false );
                $("#btn-exportar-excel" ).prop( "href", BASE_URL+'reporte/listatoEstudiantesGrupalExcel/'+grupo+"/"+estado);


            }, error: function () {

                alert("Error");
            }

        });

    }


    return false;


}


function abrirModalBuscarEstudiante() {


    abrirModal("modal-buscar-estudiante");
    $("#nombres-estudiantes").focus();


}


function consultarGruposPorPerido2() {


    event.preventDefault();
    var codigo = $("#periodo").val();

    $.ajax({


        type: "POST",
        url: BASE_URL + "admin/consultarGrupoPorPeriodo",
        data: {codigo:codigo},
        success: function (resp) {


            $("#grupo").html(resp);





        },

        error: function () {
            alert("Error");

        }

    });


}


function consultarGruposPorPerido(codigo) {


    event.preventDefault();


    $.ajax({


        type: "POST",
        url: BASE_URL + "admin/consultarGrupoPorPeriodo",
        data: {codigo:codigo},
        success: function (resp) {


            $("#grupo").html(resp);





        },

        error: function () {
            alert("Error");

        }

    });


}


function matricular() {

    event.preventDefault();
    $.ajax({


        type: "POST",
        url: BASE_URL + "admin/matriculaEstudiantesAntiguos",
        data: $('#form-matricula').serialize(),
        success: function (resp) {





            if (resp != '-1') {

                mjsConfirmacion("El estudiante se ha matriculado correctamente");
                $('#form-matricula')[0].reset();

                $(".select2-reset").val('').trigger('change');

                $("#btn-matricular").prop("disabled", true);


            } else {


                mjsAdvertencia("El estudiante ya se encuentra matriculado en ese grupo");


            }


        },

        error: function () {
            alert("Error");

        }

    });


}


function editarMatricula(codigo) {



    $("#codigo").val(codigo);

    $("#documento-estudiante").val($("#documento").val());


    abrirModal("editar-matricula");




}

function cancelarMatricula(codigo) {

    var estado = $("#" + codigo).prop("title");

    if (estado != "0") {

        $.confirm({
            title: 'Sistema Académico',
            content: '¿Está seguro que desea cancelar la matricula?',
            icon: 'fa fa-question-circle',
            buttons: {
                confirmar: function () {

                    var nombreEstudiante = $("#nombres-apellidos").val();

                    $.ajax({
                        type: 'POST',
                        url: BASE_URL + "admin/cancelarMatricula",
                        data: {codigo: codigo},
                        success: function (resp) {



                            if (resp == "1") {

                                $("#" + codigo).html('<span class="label label-success" title="Activa"> <i  class="fa fa-check"></i></span>');
                                mjsConfirmacion("La matrícula del estudiante " + nombreEstudiante + " ha sida cancelada con éxito");

                            } else if (resp == "-1") {


                                mjsError("NO se pueden cancelar matrículas de periodos académicos anteriores")


                            }


                        } ,error:function () {

                            alert("Error");
                        }
                    });

                },
                cancelar: function () {

                }
            },


        });


    } else {

        mjsError("La matrícula ya se encuentra cancelada");

    }


    /*
     var r = confirm("¿ Está seguro que desea cancelar la matricula ?");
     if (r == true) {




     }

     */


}

$('#nombres-usuarios').on('keyup', function () {

    var nombres = $('#nombres-usuarios').val();

    if(nombres.length>5){

    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/filtrarEstudianteParaMatricula",
        data: {nombres: nombres},
        success: function (datos) {
            $('#agrega-usuarios').html(datos);
        }
    });

}else{

    console.log("Nooo....");

}
    return false;
});

$('#nombres-estudiantes').on('keyup', function () {

    var nombres = $('#nombres-estudiantes').val();

    if(nombres.length>2){

    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/filtrarEstudianteMatriculados",
        data: {nombres: nombres},
        success: function (datos) {
            $('#agrega-usuarios').html(datos);
        }
    });

    }
    return false;
});


function seleccionarEstudiante(documento, nombresApellidos) {

    $("#documento").val(documento);
    $("#nombres-apellidos").val(nombresApellidos);


    $("#btn-matricular").prop("disabled", false);

    cerrarModal("modal-buscar-estudiante");


}


function seleccionarEstudianteMatriculas(documento, nombresApellidos) {

    $("#documento").val(documento);
    $("#nombres-apellidos").val(nombresApellidos);


    $("#btn-matricular").prop("disabled", false);

    cerrarModal("modal-buscar-estudiante");


    consultarMatriculasEstudiantes();


}



