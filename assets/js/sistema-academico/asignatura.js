




$( document ).ready(function() {


    $("#asignatura").select2({


        placeholder: 'SELECIONE',
        theme: "bootstrap",
        width:'100%',
        dropdownParent: $("#modal-asignatura-semestral"),

    });



});


function consultarAsignaturasSemestrales() {


    event.preventDefault();


    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/consultarAsignaturasSemestrales",
        data:  $("#asignaturas-semestrales").serialize(),
        success: function (datos) {

            $('#agrega-registros').html(datos);
        }
    });

    return false;
}


function filtrarAsignatura() {


    var nombre = $('#filtro-asignatura').val();


    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/filtrarAsignatura",
        data:  {nombre:nombre},
        success: function (datos) {

            $('#agrega-registros').html(datos);
        }
    });

    return false;
}




function abrirModalCrearAsignatura() {


    $("#titulo-modal").html("Registro de asignatura");
    $('#operacion').val("registrar");
    $('#bt-operacion').val("Registrar");

    $('#crear-asignatura')[0].reset();

    abrirModal("modal-crear-asignatura");
    $("#nombre").focus();
}

function abrirModalAignarAsignatura() {




    abrirModal("modal-asignatura-semestral");
    $("#nombre").focus();
}

function seleccionarAsignaturaSemestral(codigo) {



    $.confirm({
        title: 'Sistema Académico',
        content: '¿Está seguro que desea remover la asignatura del plan de estudios?',
        icon: 'fa fa-question-circle',
        buttons: {
            confirmar: function () {



                $.ajax({
                    type: 'POST',
                    url: BASE_URL + "admin/removerAsignatura",
                    data: {codigo: codigo},
                    success: function (resp) {



                        if (resp == "1") {

                             mjsConfirmacion("La asignatura se ha removido con éxito");

                            consultarAsignaturasSemestrales();

                        }


                    } ,error:function () {

                        alert("Error");
                    }
                });

            },
            cancelar: function () {

            }
        },


    });

}


function abrirModalEditarAsignatura(codigo) {

    $.ajax({
        url: BASE_URL+"admin/consultarAsignatura",
        type: "POST",
        data: {codigo: codigo},
        success: function (resp) {

            var asignatura = eval(resp);

            $.each(asignatura, function (i, item) {

                $("#codigo").val(asignatura[i].codigo);
                $("#nombre").val(asignatura[i].nombre);
                $("#abreviatura").val(asignatura[i].abreviatura);
                $("#programa").val(asignatura[i].programa);
                $("#creditos").val(asignatura[i].creditos);


            });


            $("#titulo-modal").html("Edición de docente");

            $('#operacion').val("editar");
            $('#bt-operacion').val("Editar");

            abrirModal("modal-crear-asignatura");


        }, error: function () {

            alert("Error");

        }
    });


}


function registrarAsiganatura() {


    $.ajax({
        type: $('#crear-asignatura').attr('method'),
        url: $('#crear-asignatura').attr('action'),
        data: $('#crear-asignatura').serialize(),
        success: function (resp) {


            if (resp != '-1') {



                var op= $('#operacion').val();




                    if (op == 'editar') {


                        if(resp == '2'){

                            location.reload(true);

                        }else{
                            var mensaje = '<div class="alert alert-warning"><strong>Advertencia!</strong>No se pudo editar </div>';


                        }

                }else{


                    var mensaje = '<div class="alert alert-info"><strong>Éxito!</strong>La asignatura de registro correcetamente</div>';

                    $('#mensaje').html(mensaje).show(200).delay(4000).hide(200);




                    $('#crear-asignatura')[0].reset();

                    $("#nombre").focus();
                }



            } else {

                var mensaje = '<div class="alert alert-danger"><strong>Error!</strong> Ya existe una asiganatura con es nombre</div>';

                $('#mensaje').html(mensaje).show(200).delay(4000).hide(200);

            }

        },

        error: function () {
            alert("Error");

        }

    });


    return false;
}

function registrarAsiganaturaSemestral() {


    event.preventDefault();


    $.ajax({
        type: 'POST',
        url: BASE_URL+"admin/registrarAsignaturaSemestral",
        data:  $("#asignatura-semestral").serialize(),
        success: function (resp) {


            if(resp=="1"){

                mjsConfirmacion("La asignatura se ha registrado al semestre");
            }else{

                mjsError("La asignatura ya se encuentra registrada");
            }






        }, error:function () {

            alert("Error");
        }
    });

    return false;
}
