

function  mjsConfirmacion(contenido) {

    $.confirm({

        title: 'Información',
        content:contenido ,
        icon: 'fa fa-info-circle',
        type: 'green',

        buttons: {
            aceptar: function () {
            }
        }
    });

}


function mjsAdvertencia(contenido) {

    $.alert({
        title: 'Advertencia',
        icon: 'fa fa-warning',
        type: 'orange',

        content: contenido,

        buttons: {
            aceptar: function () {
            }
        }

    });

}

function mjsError(contenido) {

    $.alert({
        title: 'Error',
        icon: 'fa fa-warning',
        type: 'red',

        content: contenido,

        buttons: {
            aceptar: function () {
            }
        }

    });

}

