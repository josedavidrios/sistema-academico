<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema Académico - EBAH</title>
    <meta name="description" content="Inscripciones de la Escuela de Bellas Artes y Humanidades de Sucre - EBAH">
    <meta name="keywords" content="Sistema, Inscripciones, Bellas Artes,Notas, EBAH,  Bellas Artes De Sucre">


    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?=base_url('assets/img/favicon.ico')?> " type="image/x-icon">


    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/inicio_sesion/bootstrap.css')?> ">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/fonts/inicio_sesion/font-awesome/css/font-awesome.css')?>">

    <!-- Nivo Lightbox
    ================================================== -->
    <link rel="stylesheet" href="<?=base_url('assets/css/inicio_sesion/nivo-lightbox.css') ?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/inicio_sesion/nivo_lightbox_themes/default/default.css') ?>">

    <!-- Slider
    ================================================== -->
    <link href="<?=base_url('assets/css/inicio_sesion/owl.carousel.css') ?>" rel="stylesheet" media="screen">
    <link href="<?=base_url('assets/css/inicio_sesion/css/owl.theme.css')?>" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/inicio_sesion/style.css')?> ">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/responsive.css')?> ">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/select2/select2.min.css')?> ">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/select2/select2-bootstrap.css')?> ">


    <!-- Google Fonts
    ================================================== -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="<?=base_url('assets/js/inicio_sesion/modernizr.custom.js')?> "></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>



    <![endif]-->
</head>

<body>

<!-- Main Navigation
================================================== -->
<nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="<?=base_url('assets/img/logo-ebah.png')?> " alt="..."></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="https://www.ebah.edu.co/" class="scroll">IR Al SITIO </a></li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Home Section
================================================== -->
<div id="tf-home" class="inscripciones">
    <div class="overlay">
        <!-- Overlay Color -->
        <div class="container">
            <!-- container -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-7">
                    <div class="content-heading text-left">
                        <!-- Input Your Home Content Here -->
                        <h1>Sistema Académico</h1>
                        <p class="lead">La inscripción aplica solo para <strong> estudiantes de primer ingreso</strong>, si ya te has matriculado en algunos de nuestros programas anteriormente, debes acercarte a nuestras instalaciones a realizar de nuevo tu matricula. </p>



                    </div>
                    <!-- End Input Your Home Content Here -->
                </div>

                <div class="col-md-5 col-sm-12 col-lg-5">





                    <div class="form-login form-inscripcion">
                        <header>
                            <h3>Inscripción</h3>
                        </header>
                        <hr>

                        <form role="form" id="form-slider" class="form-horizontal" method="post"  action="<?=base_url('usuario/inscribir')?>">
                            <div class="form-group">


                                <div class="col-md-4">

                                    <select class="form-control" required="" name="tipo-documento" id="tipo-documento">
                                        <option value="">TIPO DOCUMENTO</option>

                                        <option value="CC">CEDULA DE CIUDADANÍA</option>
                                        <option value="TI">TARJETA DE IDENTIDAD</option>
                                        <option value="RC">REGISTRO CIVIL</option>
                                        <option value="CE">Cédula de Extranjería</option>

                                    </select>

                                </div>

                                <div class="col-md-8">


                                    <input type="number" class="form-control mayus" placeholder="Documento de IDENTIDAD"  name="documento" required="">
                                </div>

                            </div>
                            <!-- /.form-group -->




                            <div class="form-group">

                                <div class="col-md-12">

                                    <input type="text" class="form-control mayus" placeholder="Nombres"  name="nombres" required="">
                                </div>


                            </div>


                            <div class="form-group">


                                <div class="col-md-12">

                                    <input type="text" class="form-control mayus" placeholder="Apellidos"  name="apellidos" required="">
                                </div>

                            </div>




                            <!-- /.form-group -->

                            <div class="form-group">

                                <div class="col-md-6">

                                    <input type="email" class="form-control mayus" placeholder="Correo" id="correo" name="correo">


                                </div>

                                <div class="col-md-6">
                                    <input type="text" class="form-control mayus" id="celular" placeholder="Celular" name="celular" required="">

                                </div>


                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">


                                <div class="col-md-6">

                                    <input  type="date"  onClick="$(this).removeClass('placeholderclass')"  id="fecha-nacimiento"  placeholder="NACIMIENTO" required name="fecha-nacimiento" class="form-control mayus dateclass placeholderclass">
                                </div>

                                <div class="col-md-6">

                                    <select required class="form-control" id="sexo" name="sexo">
                                        <option value="">SEXO</option>
                                        <option value="M">Masculino</option>
                                        <option value="F">Femenino</option>
                                        <option value="NE">No especifica</option>

                                    </select>

                                </div>

                            </div>


                            <div class="form-group">

                                <div class="col-md-12">

                                    <select required  class="form-control select2-reset" name="lugar-residencia"  id="lugar-residencia" >
                                        <option value="">LUGAR DE RESIDENCIA</option>

                                    </select>

                                </div>



                            </div>

                            <div class="form-group">

                                <div class="col-md-8">

                                <select class="form-control mayus" name="programa" id="">

                                    <option value="">Selecione el Programa</option>
                                    <option value="DG">Diseño gráfico</option>
                                    <option value="MI">Música Instrumental</option>
                                    <option value="AP">Artes Plásticas</option>
                                    <option value="CD">Coreografía para la Danza</option>

                                </select>

                                </div>

                                <div class="col-md-4">

                                    <select class="form-control mayus" name="jornada" id="">

                                        <option value="">JORNADA</option>
                                        <option value="M">Mañana</option>

                                        <option value="S">Sábado</option>


                                    </select>


                                </div>


                            </div>





                            <!-- /.form-group -->

                            <div class="form-group">

                                <div class="col-md-offset-2 col-md-8">
                                    <div id="form-slider-status"></div>
                                    <button type="submit" id="form-slider-submit" class="btn btn-default">Inscribir ya!</button>

                                </div>

                               </div>
                            <!-- /.form-group -->
                        </form>

                        <!-- /#form-slider -->
                        <figure>*Todos los campos son obligatorios</figure>
                    </div>

                </div>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- End Overlay Color -->
</div>

<!-- Intro Section
================================================== -->
<div id="tf-intro">
    <div class="container">
        <!-- container -->
        <div class="row">
            <!-- row -->

            <div class="col-md-8 col-md-offset-2">

            </div>

        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>

<!-- Service Section
================================================== -->
<div id="tf-services">
    <div class="container">
        <!-- container -->

        <div class="section-header">
            <h2>¿Quienes<span class="highlight"><strong> Somos</strong></span>?</h2>
            <h5><em>La Escuela de Bellas Artes y Humanidades del Departamento de Sucre, es una institución dedicada a la formación integral y funcional, con un talento humano idóneo que estimula la creación, producción e innovación para el mejoramiento continuo de la sociedad.</em></h5>
        </div>


    </div>
    <!-- end container -->
</div>

<div id="tf-footer">
    <div class="container">
        <!-- container -->
        <p class="pull-left">© 2019 EBAH. Todos los derechos reservados</p>
        <!-- copyright text here-->
        <ul class="list-inline social pull-right">
            <li><a href="https://www.facebook.com/BellasArtesEbah/"><i class="fa fa-facebook"></i></a></li>
            <!-- Change # With your FB Link -->
            <li><a href="https://twitter.com/BellasArteSucre"><i class="fa fa-twitter"></i></a></li>
            <!-- Change # With your Twitter Link -->
            <li><a href="https://www.instagram.com/bellasartesdesucre/"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCqAQkQdeQYbmMJ-mkDZHyRw"><i class="fa fa-youtube"></i></a></li>

        </ul>
    </div>
    <!-- end container -->
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="<?=base_url('assets/js/config.js')?>"></script>


<script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>


<script src="<?=base_url('assets/js/select2/select2.min.js')?>"></script>
<script src="<?=base_url('assets/js/select2/es.js')?>"></script>


<script>

    $('#lugar-residencia').select2({
        placeholder: 'SELECCIONE UN MUNICIPIO',
        minimumInputLength: 1,
        theme: "bootstrap",
        ajax: {
            url: BASE_URL+"/usuario/filtrarMunicipios/",
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });



</script>



<?php


if(strpos(base_url(), "http://localhost")=== false){

    ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116852112-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116852112-2');
    </script>

    <?php
}


?>
</body>

</html>
