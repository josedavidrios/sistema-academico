<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema Académico - EBAH</title>
    <meta name="description" content="Sistema académico de la Escuela de Bellas Artes y Humanidades de Sucre - EBAH">
    <meta name="keywords" content="Sistema, Bellas Artes,Notas, EBAH,  Bellas Artes De Sucre">


    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?=base_url('assets/img/favicon.ico')?> " type="image/x-icon">


    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/inicio_sesion/bootstrap.css')?> ">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/fonts/inicio_sesion/font-awesome/css/font-awesome.css')?>">

    <!-- Nivo Lightbox
    ================================================== -->
    <link rel="stylesheet" href="<?=base_url('assets/css/inicio_sesion/nivo-lightbox.css') ?>">


    <link rel="stylesheet" href="<?=base_url('assets/css/inicio_sesion/nivo_lightbox_themes/default/default.css') ?>">

    <!-- Slider
    ================================================== -->
    <link href="<?=base_url('assets/css/inicio_sesion/owl.carousel.css') ?>" rel="stylesheet" media="screen">
    <link href="<?=base_url('assets/css/inicio_sesion/css/owl.theme.css')?>" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/inicio_sesion/style.css')?> ">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/responsive.css')?> ">

    <!-- Google Fonts
    ================================================== -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="<?=base_url('assets/js/inicio_sesion/modernizr.custom.js')?> "></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Main Navigation
================================================== -->
<nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="<?=base_url('assets/img/logo-ebah.png')?> " alt="..."></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="https://www.ebah.edu.co/" class="scroll">IR Al SITIO </a></li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Home Section
================================================== -->
<div id="tf-home" class="app">
    <div class="overlay">
        <!-- Overlay Color -->
        <div class="container">
            <!-- container -->
            <div class="row">
                <div class="col-md-8">
                    <div class="content-heading text-left">
                        <!-- Input Your Home Content Here -->
                        <h1>Sistema Académico</h1>
                        <p class="lead">Una oportunidad para expresar tu talento!!! </p>
                    </div>
                    <!-- End Input Your Home Content Here -->
                </div>

                <div class=" col-md-4">





                    <div class="form-login">
                        <header>
                            <h3>Inicio de Sesión</h3>
                        </header>
                        <hr>
                        <form role="form" method="post" action="<?=base_url('sesion/iniciar')?>" id="form-slider">
                            <div class="form-group">


                                <label for="name">Documento de Identidad</label>
                                <input autofocus type="text" class="form-control" placeholder="" name="usuario" required="">
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label for="name">Contraseña</label>
                                <input type="password" class="form-control" placeholder=""  name="clave" required="">
                            </div>





                            <?php


                            $correcto = $this->session->flashdata('login');

                            if(isset($correcto)){


                                ?>


                                <div class="form-group">

                                    <div class="alert alert-danger">
                                        <strong>!Error!</strong> El usuario y la contraseña no coinciden
                                    </div>


                                </div>

                                <?php

                            }

                            ?>





                            <!-- /.form-group -->

                            <div class="form-group">
                                <div id="form-slider-status"></div>
                                <button type="submit" id="form-slider-submit" class="btn btn-default">Iniciar</button>
                            </div>
                            <!-- /.form-group -->


                        </form>
                        <!-- /#form-slider -->
                      <!--  <figure>¿ Olvidó su contraseña ?</figure> -->
                    </div>

                </div>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- End Overlay Color -->
</div>

<!-- Intro Section
================================================== -->
<div id="tf-intro">
    <div class="container">
        <!-- container -->
        <div class="row">
            <!-- row -->

            <div class="col-md-8 col-md-offset-2">

            </div>

        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>

<!-- Service Section
================================================== -->
<div id="tf-services">
    <div class="container">
        <!-- container -->

        <div class="section-header">
            <h2>¿NO HAS PODIDO <span class="highlight"><strong>INGRESAR </strong></span>?</h2>
            <h5><em>

                    Comunícate a soporte@cegolfo.edu.co, indica tu documento de identidad y nombres completos, te atenderemos los más pronto posible, si tienes dudas sobre tu matrícula, debes informar directamente a admisiones@ebah.edu.co.

                </em></h5>



            <!--
            <h2>¿Quienes<span class="highlight"><strong> Somos</strong></span>?</h2>
            <h5><em>La Escuela de Bellas Artes y Humanidades de Sucre, es una institución dedicada a la formación integral y funcional, con un talento humano idóneo que estimula la creación, producción e innovación para el mejoramiento continuo de la sociedad.</em></h5>
-->


        </div>


    </div>
    <!-- end container -->
</div>

<div id="tf-footer">
    <div class="container">
        <!-- container -->
        <p class="pull-left">© <?=date("Y")?> EBAH. Todos los derechos reservados</p>
        <!-- copyright text here-->
        <ul class="list-inline social pull-right">
            <li><a href="https://www.facebook.com/BellasArtesEbah/"><i class="fa fa-facebook"></i></a></li>
            <!-- Change # With your FB Link -->
            <li><a href="https://twitter.com/BellasArteSucre"><i class="fa fa-twitter"></i></a></li>
            <!-- Change # With your Twitter Link -->
            <li><a href="https://www.instagram.com/bellasartesdesucre/"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCqAQkQdeQYbmMJ-mkDZHyRw"><i class="fa fa-youtube"></i></a></li>

        </ul>
    </div>
    <!-- end container -->
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>


<?php


if(strpos(base_url(), "http://localhost")=== false){

    ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116852112-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116852112-2');
    </script>

    <?php
}


?>
</body>

</html>
