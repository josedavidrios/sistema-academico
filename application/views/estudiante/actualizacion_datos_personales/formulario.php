<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Actualiazación de Datos </h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!--

                        onsubmit="matriculasPrimerIngreso()"
                        -->

                        <form id="form-matricula-primer-ingreso" class="form-horizontal" method="post"
                              onsubmit="matriculasPrimerIngreso()"
                              action="<?= base_url('estudiante/actualizarDatosPersonales') ?>">

                            <div class="form-group">


                                <label class="col-md-1 col-xs-12 control-label" for="name">Documento</label>
                                <div class="col-md-3 col-xs-12">

                                    <input type="text" value="<?= $estudiante['documento'] ?>"
                                           disabled class="form-control mayus">

                                </div>


                                <label class="col-md-1 col-xs-12 control-label" title="Tipo de Documento" for="name">Tipo</label>
                                <div class="col-md-3 col-xs-12">


                                    <select class="form-control form-control-input" required name="tipo-documento">
                                        <option selected value="">SELECIONAR</option>


                                        <?php


                                        foreach ($tipos_documentos as $tipo) {

                                            $selected = "";


                                            if ($tipo['codigo'] == $estudiante['tipo_documento']) {

                                                $selected = "selected";


                                            }

                                            ?>
                                            <option value="<?= $tipo['codigo'] ?>" <?= $selected ?> ><?= $tipo['nombre'] ?></option>


                                            <?php

                                        }

                                        ?>


                                    </select>


                                </div>


                                <label class="col-md-1 control-label col-xs-12" for="name">Expedida</label>
                                <div class="col-md-3 col-xs-12">


                                    <select required class="form-control select2-reset lugares"
                                            name="lugar-expedicion-documento"
                                            id="lugar-expedicion-documento">





                                        <?php


                                        echo  var_dump( $estudiante['lugar_expedicion_documento'] ) ;

                                        if ($estudiante['lugar_expedicion_documento'] == 0) {

                                            ?>
                                            <option value="">SELECCIONE</option>

                                            <?php
                                        } else {

                                            ?>
                                            <option value="<?= $estudiante['lugar_expedicion_documento'] ?>"><?= $estudiante['municipio_expedicion_documento'] ?></option>

                                            <?php
                                        }


                                        ?>


                                    </select>


                                </div>


                            </div>

                            <div class="form-group">

                                <label class="col-md-1 col-xs-12 control-label" for="name">Nombres</label>
                                <div class="col-md-3 col-xs-12">

                                    <input type="text" id="nombres" value="<?= $estudiante['nombres'] ?>"
                                           name="nombres" class="form-control mayus">

                                </div>


                                <label class="col-md-1 col-xs-12 control-label" for="name">Apellidos</label>
                                <div class="col-md-3 col-xs-12">

                                    <input type="text" id="apellidos" value="<?= $estudiante['apellidos'] ?>"
                                           name="apellidos" class="form-control mayus">

                                </div>


                                <label class="col-md-1 col-xs-12 control-label" for="name">Sexo</label>
                                <div class="col-md-3 col-xs-12">


                                    <select required class="form-control mayus" name="sexo">


                                        <?php

                                        $selectedM = "";
                                        $selectedF = "";

                                        if ($estudiante['sexo'] == "M") {

                                            $selectedM = "selected";

                                        } else if ($estudiante['sexo'] == "F") {

                                            $selectedF = "selected";

                                        } else {

                                            $selectedNE = "selected";

                                        }

                                        ?>

                                        <option <?= $selectedM ?> value="M">Masculino</option>
                                        <option <?= $selectedF ?> value="F">Femenino</option>
                                        <option <?= $selectedNE ?> value="NE">No especifica</option>

                                    </select>


                                </div>


                            </div>


                            <div class="form-group">


                                <label class="col-md-1 col-xs-12 control-label" title="Fecha de Nacimiento"
                                       for="name">Naciste el</label>
                                <div class="col-md-3 col-xs-12">

                                    <input type="date" id="fecha-nacimiento"
                                           value="<?= $estudiante['fecha_nacimiento'] ?>" required
                                           name="fecha-nacimiento"
                                           class="form-control mayus">
                                </div>


                                <label class="col-md-1 control-label col-xs-12" for="name">Naciste en</label>
                                <div class="col-md-4 col-xs-12">


                                    <select required class="form-control select2-reset lugares" name="lugar-nacimiento"
                                    >


                                        <?php

                                        if ($estudiante['lugar_nacimiento'] == 0) {

                                            ?>
                                            <option value="">SELECCIONE</option>

                                            <?php
                                        } else {

                                            ?>
                                            <option value="<?= $estudiante['lugar_nacimiento'] ?>"><?= $estudiante['municipio_nacimiento'] ?></option>

                                            <?php
                                        }


                                        ?>


                                    </select>


                                </div>


                                <label class="col-md-1 col-xs-12 control-label" title="Tipo de Sangre" for="name">T.
                                    Sangre</label>

                                <div class="col-md-2 col-xs-12">
                                    <select class="form-control mayus" required type="text" name="tipo-sangre">


                                        <option selected value="">SELECIONAR</option>

                                        <?php




                                        $tiposSangre = ["O-", "O+", "A-", "A+", "B-", "B+", "AB-", "AB+"];


                                        for ($i = 0; $i < count($tiposSangre); $i++) {


                                            $selected = "";

                                            if (strcmp($tiposSangre[$i], $estudiante['tipo_sangre']) == 0) {

                                                $selected = "selected";



                                        }
                                            ?>

                                            <option value="<?= $tiposSangre[$i] ?>" <?= $selected ?> ><?= $tiposSangre[$i] ?></option>


                                            <?php
                                        }


                                        ?>


                                    </select>


                                </div>


                            </div>

                            <div class="form-group">

                                <label class="col-md-1 col-xs-12 control-label" for="name">Celular</label>
                                <div class="col-md-3 col-xs-12">

                                    <input type="number" required name="celular" value="<?= $estudiante['celular'] ?>"
                                           id="celular" class="form-control">

                                </div>


                                <label class="col-md-1 col-xs-12 control-label" for="name">Correo</label>
                                <div class="col-md-4 col-xs-12">

                                    <input type="email" name="correo" placeholder="Correo Personal" required
                                           value="<?= $estudiante['correo'] ?>"
                                           id="correo"
                                           class="form-control mayus">

                                </div>

                                <label class="col-md-1 col-xs-12 control-label" for="name">Est. Civil</label>

                                <div class="col-md-2 col-xs-12">
                                    <select name="estado-civil" required
                                            class="form-control mi-select2 form-control-input">
                                        <option value="">SELECCIONE</option>

                                        <?php


                                        foreach ($estados_civiles as $estado) {

                                            $selected = "";


                                            if ($estado['codigo'] != 0) {

                                                if ($estado['codigo'] == $estudiante['estado_civil']) {

                                                    $selected = "selected";

                                                }

                                            }

                                            ?>
                                            <option value="<?= $estado['codigo'] ?>" <?= $selected ?> ><?= $estado['nombre'] ?></option>
                                            <?php


                                        }

                                        ?>

                                    </select>


                                </div>





                            </div>


                            <div class="form-group">



                                <label class="col-md-1 control-label col-xs-12" for="name">Vives en</label>
                                <div class="col-md-3 col-xs-12">


                                    <select required class="form-control select2-reset lugares" name="lugar-residencia"
                                            id="lugar-residencia">



                                        <?php

                                        if ($estudiante['lugar_residencia']==0){

                                            ?>
                                            <option value="">SELECCIONE</option>

                                            <?php
                                        }else{

                                            ?>
                                            <option value="<?=$estudiante['lugar_residencia']?>"><?=$estudiante['municipio_residencia']?></option>

                                            <?php
                                        }


                                        ?>



                                    </select>


                                </div>



                                <label class="col-md-1 col-xs-12 control-label" for="name">Estrato</label>
                                <div class="col-md-3 col-xs-12">

                                    <select required class="form-control" id="estrato" name="estrato">


                                        <option  value="">SELECIONAR</option>

                                        <?php


                                        $estratos = ["1", "2", "3", "4", "5", "6"];


                                        for ($i = 0; $i < count($estratos); $i++) {
                                            $selected = "";

                                            if ($estratos[$i] == $estudiante['estrato']) {

                                                $selected = "selected";


                                            }

                                            ?>


                                            <option value="<?= $estratos[$i] ?>" <?= $selected ?>><?= $estratos[$i] ?></option>

                                            <?php
                                        }


                                        ?>

                                    </select>


                                </div>

                                <label class="col-md-1 col-xs-12 control-label" for="name">Zona</label>

                                <div class="col-md-3 col-xs-12">


                                    <select name="zona" required class="form-control" id="zona">

                                        <?php


                                        $selectedUrbana = "";
                                        $selectedRural = "";

                                        if ($estudiante['zona'] == "URBANA") {

                                            $selectedUrbana = "selected";

                                        } else if ($estudiante['zona'] == "RURAL") {

                                            $selectedRural = "selected";

                                        }

                                        ?>

                                        <option selected value="URBANA">SELECIONAR</option>
                                        <option <?= $selectedUrbana ?> value="URBANA">URBANA</option>
                                        <option <?= $selectedRural ?> value="RURAL">RURAL</option>


                                    </select>


                                </div>


                            </div>
                            <div class="form-group">


                                <label class="col-md-1 col-xs-12 control-label" for="name">Barrio</label>
                                <div class="col-md-3 col-xs-12">

                                    <input type="text" required id="barrio" value="<?= $estudiante['barrio'] ?>"
                                           name="barrio"
                                           class="form-control mayus">

                                </div>

                                <label class="col-md-1 col-xs-12 control-label" for="name">Dirección</label>
                                <div class="col-md-3 col-xs-12">

                                    <input type="text" name="direccion" value="<?= $estudiante['direccion'] ?>"
                                           id="direccion" class="form-control mayus">

                                </div>







                                <label class="col-md-1 control-label col-xs-12" for="name">EPS</label>
                                <div class="col-md-3 col-xs-12">


                                    <select required class="form-control select2-reset eps" name="eps"
                                    >


                                        <?php

                                        if ($estudiante['eps'] == "0") {

                                            ?>
                                            <option value="">SELECCIONE</option>

                                            <?php
                                        } else {

                                            ?>
                                            <option value="<?= $estudiante['eps'] ?>"><?= $estudiante['nombre_eps'] ?></option>

                                            <?php
                                        }


                                        ?>


                                    </select>


                                </div>






                            </div>


                            <div class="form-group">


                                <label class="col-md-1 col-xs-12 control-label " title="Nivele de Educación" for="name">N.
                                    Estud</label>
                                <div class="col-md-4 col-xs-12">


                                    <select name="nivel-educacion" required
                                            class="form-control mi-select2 form-control-input">


                                        <?php


                                        foreach ($niveles as $nivel) {

                                            $selected = "";

                                            if ($nivel['codigo'] == $estudiante['nivel_educacion']) {

                                                $selected = "selected";

                                            }
                                            ?>
                                            <option value="<?= $nivel['codigo'] ?>" <?= $selected ?> ><?= $nivel['nombre'] ?></option>
                                            <?php


                                        }

                                        ?>

                                    </select>


                                </div>


                                <label class="col-md-1 col-xs-12 control-label" for="name">Título</label>
                                <div class="col-md-6 col-xs-12">

                                    <input type="text" name="titulo" placeholder="Título Optenido" required
                                           value="<?= $estudiante['titulo'] ?>"

                                           class="form-control mayus">

                                </div>


                            </div>


                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">


                                    <input class="btn btn-primary pull-right" id="btn-matricular" type="submit"
                                           value="Actualizar">

                                </div>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


