<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="contenido">


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Notas del semestre </h2>


                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content x_content_notas">


                            <table id="datatable-responsive"
                                   class="tabla-notas table table-striped  table-bordered dt-responsive nowrap table-hover">
                                <thead>
                                <tr>


                                    <th width="7">#</th>
                                    <th>Nombre</th>


                                    <th width="80" class="text-center">Nota</th>


                                </tr>
                                </thead>
                                <tbody>


                                <?php

                                foreach ($asignaturas as $asignatura) {


                                    ?>


                                    <tr>

                                        <td><?=$asignatura['codigo_asignatura_matriculada'] ?></td>
                                        <td><?=$asignatura['nombre'] ?></td>
                                        <td class="text-center"><b><?= $asignatura['nota_definitiva'] ?></b></a></td>

                                    </tr>


                                    <?php

                                }

                                ?>


                                </tbody>

                            </table>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /page content -->

