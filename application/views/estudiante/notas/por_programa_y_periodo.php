<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="contenido">


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Notas del semestre</h2>


                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content x_content_notas">


                            <table id="datatable-responsive"
                                   class="tabla-notas table table-striped  table-bordered dt-responsive nowrap table-hover">
                                <thead>
                                <tr>


                                    <th>Asignatura</th>

                                    <th width="68"  title="Nota corte 1" class="text-center">Corte 1</th>
                                    <th width="68" title="Nota corte 1" class="text-center">Corte 2</th>
                                    <th width="68" title="Nota corte 1" class="text-center">Corte 3</th>
                                    <th width="68" title="Nota corte 1" class="text-center">def</th>


                                </tr>
                                </thead>
                                <tbody>


                                <?php

                                foreach ($asignaturas as $asignatura) {


                                    echo '<tr>
                                        
                                               
                                                  <td >' . $asignatura['nombre'] . '</td>
                                                  <td class="text-center">' . $asignatura['nota1'] . '</td>
                                                  <td class="text-center">' . $asignatura['nota2'] . '</td>
                                                  <td class="text-center">' . $asignatura['nota3'] . '</td>
                                                  <td class="text-center"> <b> ' . $asignatura['nota_definitiva'] . '</b></td>

                                            </tr>';


                                }

                                ?>


                                </tbody>

                            </table>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /page content -->

