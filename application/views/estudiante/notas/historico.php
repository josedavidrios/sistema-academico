
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">


                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 >programas matriculados</h2>


                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">




                                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-hover" cellspacing="0" width="100%"  >
                                    <thead>
                                    <tr>

                                        <th>Programa</th>
                                        <th width="30">Sem</th>
                                        <th width="50">Jornada</th>
                                        <th width="50">Período</th>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    <?php

                                    foreach ($programas as $programa){


                                        echo '<tr>
                                        
                                               
                                                <td><a  href="'.base_url('estudiante/notas/de/'.strtolower( $programa['codigo'])).'/'.$programa['periodo'].'">' . $programa['nombre'] . '</a></td>
                                            
                                                    <td class="text-center">' . $programa['semestre'] . '</td>
                                                    <td class="text-center">' . $programa['nombre_jornada'] . '</td>
                                                    
                                                    <td class="text-center">' . $programa['periodo'] . '</td>
                                                    
                                            

                                            </tr>';


                                    }

                                    ?>


                                    </tbody>

                                </table>





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

