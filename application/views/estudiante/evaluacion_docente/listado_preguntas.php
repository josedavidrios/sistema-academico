<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>EVALUACIÓN DOCENTE</h2>


                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!--
                    <div class="alert alert-info" role="alert">


                        <p><strong>CRITERIOS DE EVALUACIÓN</strong>

                            <br>

                            5)Excelente, 4)Buena, 3)Aceptable, 2)Deficiente, 1) Pésimo, NS)No sabe o No aplica
                        </p>

                    </div>



                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"  >
                        <thead>
                        <tr>

                            <th colspan="10" class="text-center">CRITERIOS DE EVALUACIÓN</th>

                        </tr>





                        </thead>

                        <tbody>

                            <tr>

                                <td width="10"><strong>5</strong></td>
                                <td>Excelente</td>

                                <td width="10"><strong>4</strong></td>
                                <td>Buena</td>

                                <td width="10"><strong>3</strong></td>
                                <td>Aceptable</td>



                            </tr>

                        <tr>

                            <td width="10"><strong>2</strong></td>
                            <td>Deficiente</td>


                            <td width="10"><strong>1</strong></td>
                            <td>Pésimo</td>

                            <td width="10"><strong>NS</strong></td>
                            <td>No sabe o No aplica</td>

                        </tr>

                        </tbody>




                    </table>

                    -->

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>


                                <th width="10">N</th>
                                <th>Pregunta</th>
                                <th width="280" class="text-center">CALIFICACIÓN</th>


                            </tr>
                            </thead>
                            <tbody>

                            <form id="formu" class="form-horizontal" method="post"
                                  action="<?= base_url('estudiante/registrarEvaluacionDocente') ?>">





                                <input type="hidden" name="carga" value="<?=$carga?>">
                                <input type="hidden" name="evaluacion" value="<?=$evaluacion?>">


                                <?php


                                for ($j=0; $j<count($preguntas); $j++ ){


                                    ?>


                                <tr>

                                    <td colspan="3" class="text-left">
                                        <strong> <?=($j+1).".   " ?> <?= " ".$preguntas[$j][0]['criterio'] ?></strong>
                                </td>

                                </tr>

                                <?php


                                $i = 1;

                                foreach ($preguntas[$j] as $pregunta) {


                                    ?>

                                    <tr>

                                        <td>

                                            <?= $i ?>

                                        </td>

                                        <td>

                                            <?= $pregunta['nombre'] ?>

                                        </td>

                                        <td>

                                            <label class="radio-inline primer">
                                                <input type="radio" title="Pésimo" name="<?= $pregunta['codigo'] ?>" value="1" required> 1


                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" title="Deficiente"  name="<?= $pregunta['codigo'] ?>" value="3">2
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" title="Aceptable" name="<?= $pregunta['codigo'] ?>" value="3">3

                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" title="Buena" name="<?= $pregunta['codigo'] ?>" value="4">4
                                            </label>

                                            <label class="radio-inline">
                                                <input type="radio" title="Excelente" name="<?= $pregunta['codigo'] ?>" value="5">5

                                            </label>

                                            <label class="radio-inline">
                                                <input type="radio" title="No sabe o No aplica" name="<?= $pregunta['codigo'] ?>" value="-1">NS
                                            </label>


                                        </td>


                                    </tr>





                                    <?php

                                    $i++;
                                }

                                }

                                ?>



                            </tbody>

                        </table>

                        <label for="">Observaciones</label>
                        <textarea style="width: 100%;" name="observaciones" id="" cols="30" rows="6"></textarea>


                        <div style="padding-top: 20px">

                        <input type="reset" class="btn btn-success pull-right" value="Cancelar">
                        <input type="submit" class="btn btn-primary pull-right" value="Realizar Evaluación">

                        </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

