
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">


                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Portal estudiantes</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content"></div>





                            <div class="jumbotron">



                                <p style="font-weight: 400;">

                                    Para poder consultar tus notas debes realizar la evalucion docente, clic <a style="color: #37a12b" href="<?=base_url("estudiante/vistaEvaluacionesDocentes")?>">Aquí</a> para empezar

                                </p>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Evaluación docente</h4>
                    </div>
                    <div class="modal-body">
                        <p>Tu opinión nos hace ser mejores, te invitamos a realizar la evaluación docente
                        </p>
                    </div>
                    <div class="modal-footer">
                       <a href="<?=base_url('estudiante/vistaEvaluacionesDocentes')?>" class="btn btn-primary" >Evaluar</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
                    </div>
                </div>

            </div>
        </div>