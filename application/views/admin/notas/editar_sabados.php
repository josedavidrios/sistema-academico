<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>EDITAR NOTAS ESTUDIANTE JORNADA SABATINA</h2>



                                <input type="hidden" id="jornada" value="<?=$jornada?>" >

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <form id="form-matricula" class="form-horizontal" onsubmit=" matricular()">


                            <div class="form-group">

                                <label class="col-md-1 control-label" for="name">Documento</label>
                                <div class="col-md-2">


                                    <input readonly required id="documento" name="documento" value="<?=$estudiante['documento']?>" type="text"
                                           class="form-control">

                                </div>

                                <label class="col-md-1 control-label" for="name">Nombres</label>
                                <div class="col-md-7">

                                    <input disabled type="text" id="nombres-apellidos"   value="<?=$estudiante['apellidos']." ".$estudiante['nombres']?>" class="form-control">

                                </div>


                                <div class="col-md-1">


                                    <button type="button" class="btn btn-primary full-width"
                                            onclick="abrirModalBuscarEstudiante()">
                                        <i class="fa fa-search"></i>
                                    </button>

                                </div>


                            </div>


                            <div class="ln_solid"></div>


                        </form>


                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-hover" cellspacing="0"   >
                            <thead>
                            <tr>

                                <th width="20">Código</th>
                                <th >Asignatura</th>

                                <th class="text-center" width="2">Nota</th>
                                <th class="text-center" width="2">Editar</th>


                            </tr>
                            </thead>


                            <tbody id="notas">



                            <?php



                            if (isset($notas)){

                            foreach ($notas as $nota) {


                                echo '<tr>
                                         <td>' . $nota['codigo'] . '</td>
                                         <td>' . $nota['asignatura'] . '</td>
                                          <td class="text-center">' . estilo_nota_definitiva($nota['nota_definitiva'])  . '</td>
                                         <td class="text-center">
                                           <a class="text-center fa fa-edit fa-lg" href="javascript:verModalEditarNotas(' . $nota['codigo'] . ')"></a>
                                         </td>
                                    </tr>';

                            }

                            }

                            ?>



                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>


<div class="modal modal-wide40 fade" id="editar-notas" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">EDITAR NOTAS</b></h4>
            </div>


            <div class="modal-body">


                <div class="container">

                    <div class="row">

                        <div class="col-md-12">


                            <form method="post" action="<?=base_url("superAdmin/editarNotaSabatina")?>" class="form-horizontal form-label-left">

                                <input type="hidden" id="codigo" required="required" name="codigo">
                                <input type="hidden" id="documento-estudiante" required="required" name="documento-estudiante" >
                                <input type="hidden"  value="<?=$jornada?>" name="jornada" >




                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-1 col-xs-12" for="first-name">Definitiva
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-10 col-xs-12">
                                        <input type="number" value="0" id="nota1" step="any" min="0" max="5" name="nota-definitiva" required class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>



                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                        <button type="submit" class="btn btn-primary">Editar</button>
                                        <button type="reset" class="btn btn-success">Cancelar</button>

                                    </div>
                                </div>

                            </form>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

