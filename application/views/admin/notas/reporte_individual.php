<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>REPORTE DE NOTAS INDIVIDUAL</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">



                        <form id="form-matricula" method="post" target="_blank" action="<?=base_url('Reporte/notasIndividualPDF')?>" class="form-horizontal" >


                            <div class="form-group">

                                <label class="col-md-1 control-label" for="name">Documento</label>
                                <div class="col-md-2">

                                    <input readonly required id="documento" name="documento" type="text"
                                           class="form-control">

                                </div>

                                <label class="col-md-2 control-label" for="name">Nombres Estudiante</label>
                                <div class="col-md-6">

                                    <input readonly name="nombres" type="text"  id="nombres-apellidos" class="form-control">

                                </div>


                                <div class="col-md-1">


                                    <button type="button" class="btn btn-primary full-width"
                                            onclick="abrirModalBuscarEstudiante()">
                                        <i class="fa fa-search"></i>
                                    </button>

                                </div>


                            </div>





                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">

                                    <input class="btn btn-success pull-right"
                                           type="reset" value="Cancelar">

                                    <input class="btn btn-primary pull-right" disabled id="btn-matricular" type="submit"
                                           value="Consultar">

                                </div>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
