<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>REPORTE DE NOTAS GRUPAL</h2>


                                <input type="hidden" id="formato" value="<?=$formato?>">


                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <form id="form-matricula" class="form-horizontal">

                            <div class="form-group">


                                <label class="col-md-1 control-label" for="name">Periodo</label>

                                <div class="col-md-2">


                                    <select class="form-control"  id="periodo" onchange="consultarGruposPorPerido(this.value)">
                                        <option value="">Seleccione</option>

                                        <?php

                                            foreach ($periodos as $periodo){
                                        ?>

                                                <option value="<?=$periodo['codigo']?>"><?=$periodo['codigo']?> </option>

                                        <?php

                                            }

                                        ?>






                                    </select>

                                </div>


                                <label class="col-md-2 control-label" for="name">Seleccione el grupo</label>
                                <div class="col-md-6">


                                    <select required id="grupo"  name="grupo" class="form-control">


                                    </select>


                                </div>

                                <div class="col-md-1">


                                    <?php


                                          $icono =   "fa fa-file-pdf-o";

                                        if($formato=="excel"){


                                            $icono =   "fa fa-file-excel-o";

                                        }

                                    ?>


                                    <a id="btn-generar-reporte" href="#" target="_blank" class="btn btn-primary full-width">
                                        <i class="<?=$icono?>" aria-hidden="true"></i>
                                    </a>

                                </div>





                            </div>


                        </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
