<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Estudiantes matriculados <?=$cantidad?> </h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <form method="post"   enctype="multipart/form-data" class="form-horizontal"
                              action="<?= base_url('admin/subirNotasMasivas') ?>">


                            <div class="form-group">

                                <label class="col-md-1 control-label" for="name">Archivo</label>
                                <div class="col-md-3">


                                    <input type="hidden" value="<?=$carga?>" name="carga">
                                    <input type="hidden" value="<?=$grupo?>" name="grupo">
                                    <input type="hidden" value="<?=$corte?>" name="corte">
                                    <input type="hidden" value="<?=$programa?>" name="programa">

                                    <input required name="archivo" accept=".csv" type="file" class="form-control">

                                </div>





                                <label class="col-md-1 control-label" for="name">Asiganatura</label>
                                <div class="col-md-2">

                                    <input type="text"  readonly class="form-control" value="<?=$asignatura?>" >

                                </div>

                                <label class="col-md-1 control-label" for="name">Jornada</label>
                                <div class="col-md-2">

                                    <input type="text"  readonly class="form-control" value="<?=$jornada?>" >

                                </div>


                                <label class="col-md-1 control-label" for="name">Grupo</label>
                                <div class="col-md-1">

                                    <input type="text"  readonly class="form-control" value="<?=$letra_grupo?>" >
                                </div>



                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-9">

                                    <input class="btn btn-success pull-right "
                                           type="button" value="Cancelar">

                                    <input class="btn btn-primary pull-right " type="submit"
                                           value="Aceptar">

                                </div>
                            </div>

                        </form>



                        <?php


                        $mensaje_error = $this->session->flashdata('error_subir_notas');


                        if (isset($mensaje_error) && $mensaje_error['codigo']==1){


                            ?>

                            <div class="alert alert-error alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>!Error!</strong> Las notas deben estar entre 0 y 5. Revisar la línea número <strong><?= $mensaje_error['linea'] ?></strong> en el archivo CSV
                            </div>


                            <?php


                        }


                        else if (isset($mensaje_error) && $mensaje_error['codigo']==2){


                            ?>

                            <div class="alert alert-error alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>!Error!</strong> No se logró cambiar el estado de la carga académica
                            </div>


                            <?php


                        }


                        else if (isset($mensaje_error) && $mensaje_error['codigo']==3){


                            ?>

                            <div class="alert alert-error alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>!Error!</strong> Sea producido un error al momento de publicar las notas, el documento <strong><?=$mensaje_error['documento']?> </strong> del estudiante   <strong> <?=$mensaje_error['nombres']?> </strong> en el archivo CSV
                            </div>


                            <?php


                        }



                        else if (isset($mensaje_error) && $mensaje_error['codigo']==4){


                            ?>

                            <div class="alert alert-error alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>!Error!</strong> El número de estudiantes matriculados(<?= $mensaje_error['matriculados'] ?>) no coinciden el listado de notas que intentar publicar(<?= $mensaje_error['csv'] ?>)
                            </div>


                            <?php


                        }


                        else if (isset($mensaje_error) && $mensaje_error['codigo']==5){


                            ?>

                            <div class="alert alert-error alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>!Error!</strong> El formato del archivo CSV no es valido, las columnas debeb ser las siguientes: #, documento, nombres,nota
                            </div>


                            <?php


                        }


                        else if (isset($mensaje_error) && $mensaje_error['codigo']==6){


                            ?>

                            <div class="alert alert-error alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>!Error!</strong> Todas las notas del archivo csv están en 0 ó vacias
                            </div>


                            <?php


                        }





                        ?>










                        <table id="datatable-grupos"
                               class="table table-striped table-bordered dt-responsive"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>


                                <th width="10">#</th>
                                <th width="50">Código</th>
                                <th width="100">Documento</th>
                                <th>Nombres</th>


                            </tr>
                            </thead>
                            <tbody id="agrega-registros">


                            <?php





                        $i=1;

                        foreach ($estudiantes as $estudiante) {

                            echo '<tr>


                                   <td>' . $i . '</td>
                                   <td>' . $estudiante['codigo_matricula'] . '</td>
                                    <td>' . $estudiante['documento'] . '</td>
                                    <td>' . $estudiante['nombre'] . '</td>


                                  </tr>';


                            $i++;

                        }



                        ?>


                            </tbody>

                        </table>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
