<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Gestionar grupos periodo <?=$periodoActual ?></h2>

                            </div>

                            <div class="col-xs-2">

                                <button class="right btn btn-primary full-width" onclick="abrirModalCrearGrupos()">Nuevo

                                    <i class="fa fa-plus-circle"></i>

                                </button>
                            </div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">




                        <table id="datatable-grupos"
                               class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>


                                <th width="4">Código</th>
                                <th>Programa</th>
                                <th width="10">Sem</th>
                                <th width="10">Jornada</th>
                                <th width="10">Grupo</th>

                            </tr>
                            </thead>
                            <tbody id="agrega-registros">


                            <?php

                            foreach ($grupos as $grupo) {

                                echo '<tr>

                                     
                                        <td>' . $grupo['codigo'] . '</td>
                                        <td>' . $grupo['programa'] . '</td>
                                        <td class="text-center">' . $grupo['semestre'] . '</td>
                                        <td class="text-center">' .$grupo['jornada'] . '</td>
                                        <td class="text-center">' .$grupo['grupo'] . '</td>
                 
                                      </tr>';

                            }


                            ?>


                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<div class="modal modal-wide55 fade" id="modal-crear-grupo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Registro de grupos</b></h4>
            </div>


            <form id="crear-grupo" class="form-horizontal" method="post" action="<?= base_url('admin/registrarGrupo') ?>"
                  onsubmit="return crearGrupo()">
                <div class="modal-body">


                    <div class="form-group">

                        <label class="col-md-1 control-label" for="name">Programa*</label>


                        <div class="col-md-3">


                            <select required class="form-control" name="programa" id="programa"
                                    onchange="generarCodigoDeGrupo()">
                                <option value="">Seleccione</option>


                                <?php

                                foreach ($programas as $programa) {

                                    echo ' <option selected value="' . $programa['codigo'] . '">' . $programa['nombre'] . '</option>';

                                }

                                ?>

                            </select>


                        </div>

                        <label class="col-md-1 control-label" for="name">Semeste*</label>
                        <div class="col-md-2">


                            <select class="form-control text-uppercase" onchange="generarCodigoDeGrupo()" required name="numero-semestre" id="numero-semestre">
                                <option value="">Seleccione</option>


                                <?php


                                foreach ($semestres as $semestre){

                                    echo ' <option value="'.$semestre['numero'].'">'.$semestre['nombre'].'</option>';

                                }



                                ?>


                            </select>




                        </div>

                        <label class="col-md-1 control-label" for="name">Jornada*</label>
                        <div class="col-md-3">


                            <select required id="jornada" name="jornada" class="form-control text-uppercase"
                                    onchange="generarCodigoDeGrupo()">
                                <option value="">Seleccione</option>
                                <option value="M">Lunes a viernes</option>
                                <option value="S">Sábados</option>

                            </select>

                        </div>

                    </div>
                    <div class="form-group">

                        <label class="col-md-1 control-label" for="name">Abreviatura </label>
                        <div class="col-md-3">


                            <input required id="abreviatura" name="abreviatura" type="text" readonly class="form-control ">

                        </div>

                        <label class="col-md-1 control-label" for="name">Periodo </label>
                        <div class="col-md-2">


                            <input class="form-control" readonly value="<?=$periodoActual?>" type="text" id="periodo" name="periodo">

                        </div>





                        <label class="col-md-1 control-label" for="name">Grupo </label>
                        <div class="col-md-3">


                            <input required id="grupo"  name="grupo" type="text" readonly class="form-control ">

                        </div>


                    </div>




                    <div class="clearfix">

                    </div>

                </div>
                <div class="form-group ">

                    <div id="mensaje" class="col-md-offset-2 col-md-9">

                    </div>


                </div>

                <div class="modal-footer">
                    <input type="submit" id="bt-operacion" value="Crear" class="btn btn-primary"/>
                    <input type="reset"  value="Cancelar" class="btn btn-success"/>

                </div>
            </form>
        </div>
    </div>
</div>

