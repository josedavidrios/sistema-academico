<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>MATRICULAS POR PERIODO</h2>
                        <ul class="nav navbar-right panel_toolbox">

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                    </div>


                    <canvas id="matriculas-periodos" width="600" height="400"></canvas>

                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>INSCRIPCIONES WEB</h2>
                        <ul class="nav navbar-right panel_toolbox">

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                    </div>


                    <canvas id="chart-inscripciones" width="600" height="400"></canvas>

                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>EVALUACIONES DOCENTES</h2>
                        <ul class="nav navbar-right panel_toolbox">

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                    </div>


                    <canvas id="chart-evaluaciones-docente" width="600" height="400"></canvas>

                </div>
            </div>



        </div>


        <div class="row">

            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>SESIONES POR PLATAFORMA</h2>
                        <ul class="nav navbar-right panel_toolbox">

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                    </div>


                    <canvas id="chart-visitaa-so" width="600" height="400"></canvas>

                </div>
            </div>





            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>SESIONES AL SISTEMA</h2>
                        <ul class="nav navbar-right panel_toolbox">

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                    </div>


                    <canvas id="sesiones-sistema" width="600" height="400"></canvas>

                </div>
            </div>

        </div>

    </div>
</div>
<!-- /page content -->

