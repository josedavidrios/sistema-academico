<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Matrículas Acádemicas</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!--




                        -->

                        <form id="form-matricula" method="post" action="<?=base_url('admin/matricularAcademicasMasivas')?>" class="form-horizontal">





                            <div class="form-group">




                                <label class="col-md-1 control-label" for="name">Totales </label>
                                <div class="col-md-2">


                                    <input disabled type="text" readonly value="<?=$matriculados?>" id="numero" class="form-control">

                                </div>

                                <label class="col-md-1 control-label" for="name">Sin Legaliar </label>
                                <div class="col-md-2">


                                    <input disabled type="text" readonly value="<?=$sin_matricular?>" id="numero" class="form-control">

                                </div>


                                <label class="col-md-1 control-label"  for="name">Grupos</label>
                                <div class="col-md-2">

                                    <input type="text" readonly name="periodo" class="form-control" value="<?=$grupos?>">
                                </div>


                                <label class="col-md-1 control-label"  for="name">Periodo</label>
                                <div class="col-md-2">

                                    <input type="text" readonly name="periodo" class="form-control" value="<?=$periodo?>">
                                </div>




                            </div>

                            <div class="form-group ">

                                <div id="mensaje" class="col-md-12">

                                </div>


                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">



                                    <input class="btn btn-primary pull-right" type="submit"
                                           value="Matricular">

                                </div>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
