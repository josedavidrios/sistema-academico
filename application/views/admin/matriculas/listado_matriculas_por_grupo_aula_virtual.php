<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Matriculas <?=$periodo?></h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <form id="form-matricula" method="post" action="<?=base_url('admin/exportarEstudiantesParaAulaVirtual')?>" class="form-horizontal">

                            <div class="form-group">






                                <div class="col-md-3">


                                    <select id="grupo"  required id="grupo" onchange="consultarEstudiantesPorGrupo(this.value)" name="grupo"
                                            class="form-control text-uppercase mayus">
                                        <option value="">Seleccione el grupo</option>


                                        <?php foreach ($grupos as $grupo) : ?>





                                            <option value="<?=$grupo['codigo']?>"><?=  $grupo['programa'] . ' ' . $grupo['semestre'] ." ". $grupo['grupo'].' - ' . $grupo['codigo_jornada']   ?></option>

                                        <?php endforeach; ?>

                                    </select>


                                </div>


                                <div class="col-md-8">


                                    <select id="curso" required multiple="multiple"  name="cursos[]"
                                            class="form-control">
                                        <option  value="">Seleccione el grupo</option>



                                        <?php foreach ($cursos as $curso) : ?>

                                            <option value="<?=$curso['shortname']?>"><?=  $curso['fullname']." - ".$curso['parent']?></option>

                                         <?php endforeach; ?>


                                    </select>


                                </div>

                                <div class="col-md-1">


                                    <button type="submit" class="btn btn-primary full-width">

                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    </button>



                                </div>





                            </div>


                        </form>


                        <table class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <th width="20">#</th>
                                <th width="100">Documento</th>
                                <th>Apellidos y nombres</th>
                                <th>Correo</th>
                                <th width="100">Celular</th>


                            </tr>
                            </thead>
                            <tbody id="listado">


                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
