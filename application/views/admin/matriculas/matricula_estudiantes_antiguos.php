<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Matrícula estudiantes antiguos</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!--




                        -->

                        <form id="form-matricula" action="<?=base_url('admin/matriculaEstudiantesAntiguos')?>" class="form-horizontal" method="post" onsubmit="matricular()" >


                            <div class="form-group">

                                <label class="col-md-1 control-label" for="name">Documento</label>
                                <div class="col-md-2">

                                    <input readonly required id="documento" name="documento" type="text"
                                           class="form-control">

                                </div>

                                <label class="col-md-2 control-label" for="name">Nombres Estudiante</label>
                                <div class="col-md-6">

                                    <input disabled type="text" id="nombres-apellidos" class="form-control">

                                </div>


                                <div class="col-md-1">


                                    <button type="button" class="btn btn-primary full-width"
                                            onclick="abrirModalBuscarEstudiante()">
                                        <i class="fa fa-search"></i>
                                    </button>

                                </div>


                            </div>


                            <div class="form-group">


                                <label class="col-md-1 control-label" for="name">Grupo *</label>
                                <div class="col-md-4">


                                    <select required id="grupo" onchange="buscarGrupo()" name="grupo"
                                            class="form-control select2-reset">
                                        <option value="">Seleccione</option>


                                        <?php

                                        foreach ($grupos as $grupo) {

                                            ?>

                                            <option value="<?=$grupo['codigo']?>"> <?=$grupo['programa'].' '.$grupo['semestre'].' - '.$grupo['jornada'].' - '.$grupo['grupo']?></option>

                                            <?php
                                        }

                                        ?>


                                    </select>


                                </div>




                                <label class="col-md-1 control-label" for="name">Programa </label>
                                <div class="col-md-3">


                                    <input disabled type="text" id="programa" class="form-control">
                                    <input type="hidden" name="codigo-programa" id="codigo-programa"
                                           class="form-control">

                                </div>

                                <label class="col-md-2 control-label"  for="name">Auxilio Académico</label>
                                <div class="col-md-1">

                                    <select required class="form-control"  name="auxilio-academico">
                                        <option value="">Seleccione</option>
                                        <option value="0">0%</option>
                                        <option value="50">50%</option>
                                        <option value="75">70%</option>
                                        <option value="100">100%</option>
                                    </select>


                                </div>


                            </div>




                            <div class="form-group">

                                <label class="col-md-1 control-label" for="name">Semestre</label>
                                <div class="col-md-3">

                                    <input readonly type="text" name="semestre" id="semestre" class="form-control">

                                </div>

                                <label class="col-md-1 control-label" for="name">Jornada</label>
                                <div class="col-md-2">

                                    <input readonly type="text" name="jornada"  id="jornada" class="form-control">

                                </div>


                                <label class="col-md-1 control-label" for="name">Grupo </label>
                                <div class="col-md-2">


                                    <input disabled type="text" id="numero" class="form-control">

                                </div>


                                <label class="col-md-1 control-label"  for="name">Periodo</label>
                                <div class="col-md-1">

                                    <input type="text" readonly name="periodo" class="form-control" value="<?=$periodo?>">
                                </div>


                            </div>

                            <div class="form-group ">

                                <div id="mensaje" class="col-md-12">

                                </div>


                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">

                                    <input class="btn btn-success pull-right" onclick="restablecerFormularioDeMatricular()"
                                           type="button" value="Cancelar">

                                    <input class="btn btn-primary pull-right" disabled id="btn-matricular" type="submit"
                                           value="Matricular">

                                </div>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
