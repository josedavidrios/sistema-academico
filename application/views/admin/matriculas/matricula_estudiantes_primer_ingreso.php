<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Matrícula primer ingreso</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!--

                        onsubmit="matriculasPrimerIngreso()"
                        -->

                        <form id="form-matricula-primer-ingreso" class="form-horizontal" method="post"   onsubmit="matriculasPrimerIngreso()" action="<?=base_url('admin/matriculasPrimerIngreso')?>">

                            <div class="form-group">


                                <label class="col-md-1 control-label" for="name">Documento</label>
                                <div class="col-md-3">

                                    <input  required id="documento" name="documento" id="documento" type="number" onfocusout = "consultarEstudiante(this.value)"
                                            class="form-control">

                                </div>


                                <label class="col-md-1 control-label" for="name">Expedida en </label>
                                <div class="col-md-4">

                                    <select required  class="form-control select2-reset lugares" name="lugar-expedicion-documento"  >


                                    </select>

                                </div>

                                <label class="col-md-1 control-label" for="name">Tipo</label>
                                <div class="col-md-2">

                                    <select class="form-control form-control-input" required="" name="tipo-documento" id="tipo-documento">
                                        <option value="">SELECCIONE</option>

                                        <option value="CC">CEDULA DE CIUDADANÍA</option>
                                        <option value="TI">TARJETA DE IDENTIDAD</option>
                                        <option value="RC">REGISTRO CIVIL</option>
                                        <option value="CE">Cédula de Extranjería</option>

                                    </select>

                                </div>







                            </div>

                            <div class="form-group">



                                <label class="col-md-1 control-label" for="name">Nombres</label>
                                <div class="col-md-3">

                                    <input  type="text" id="nombres" name="nombres" class="form-control mayus">

                                </div>


                                <label class="col-md-1 control-label" for="name">Apellidos</label>
                                <div class="col-md-4">

                                    <input  type="text" id="apellidos" name="apellidos" class="form-control mayus">

                                </div>

                                <label class="col-md-1 control-label" for="name">Sexo</label>
                                <div class="col-md-2">

                                    <select required class="form-control mayus" id="sexo" name="sexo">
                                        <option value="">Seleccione</option>
                                        <option value="M">Masculino</option>
                                        <option value="F">Femenino</option>
                                        <option value="NE">No especifica</option>

                                    </select>

                                </div>
                            </div>

                            <div class="form-group">

                                <label class="col-md-1 control-label" for="name">Nació en </label>
                                <div class="col-md-3">

                                    <select required id="lugar-nacimiento" class="form-control select2-reset lugares" name="lugar-nacimiento"  >


                                    </select>

                                </div>


                                <label class="col-md-1 control-label" for="name">Vives en </label>
                                <div class="col-md-4">

                                    <select required class="form-control select2-reset lugares" id="lugar-residencia" name="lugar-residencia"  >


                                    </select>

                                </div>



                                <label class="col-md-1 control-label" title="Fecha de Nacimiento" for="name">Nació el</label>
                                <div class="col-md-2">

                                    <input  type="date" id="fecha-nacimiento" required name="fecha-nacimiento" class="form-control mayus">
                                </div>

                            </div>



                            <div class="form-group">



                                <label class="col-md-1 control-label" for="name">Celular</label>
                                <div class="col-md-3">

                                    <input  type="number" name="celular" id="celular" class="form-control">

                                </div>


                                <label class="col-md-1 control-label" for="name">Correo</label>
                                <div class="col-md-4">

                                    <input  type="email" name="correo" autocomplete="off" id="correo" class="form-control mayus">

                                </div>

                                <label class="col-md-1 control-label"  for="name">Estrato</label>
                                <div class="col-md-2">

                                    <select required class="form-control" id="estrato" name="estrato">
                                        <option value="">Seleccione</option>
                                        <option value="1">UNO</option>
                                        <option value="2">DOS</option>
                                        <option value="3">TRES</option>
                                        <option value="4">Cuatro</option>
                                        <option value="5">CINCO</option>
                                        <option value="6">SEIS</option>

                                    </select>

                                </div>
                            </div>




                            <div class="form-group">



                                <label class="col-md-1 control-label" for="name">Barrio</label>
                                <div class="col-md-3">

                                    <input  type="text" id="barrio" name="barrio" class="form-control mayus">

                                </div>


                                <label class="col-md-1 control-label" for="name">Dirección</label>
                                <div class="col-md-4">

                                    <input  type="text" name="direccion"  id="direccion" class="form-control mayus">

                                </div>


                                <label class="col-md-1 control-label" for="name">Zona</label>

                                <div class="col-md-2">

                                    <select name="zona" required class="form-control" id="zona">
                                        <option value="">SELECCIONE</option>
                                        <option value="URBANA">URBANA</option>
                                        <option value="RURAL">RURAL</option>


                                    </select>
                                </div>


                            </div>


                            <div class="form-group">


                                <label class="col-md-1 col-xs-12 control-label" for="name">Est. Civil</label>

                                <div class="col-md-3 col-xs-12">
                                    <select name="estado-civil" id="estado-civil" required
                                            class="form-control mi-select2 form-control-input">
                                        <option value="">SELECCIONE</option>

                                        <?php


                                        foreach ($estados_civiles as $estado) {



                                            ?>
                                            <option value="<?= $estado['codigo'] ?>" ><?= $estado['nombre'] ?></option>
                                            <?php


                                        }

                                        ?>

                                    </select>


                                </div>

                                <label class="col-md-1 control-label" for="name">EPS </label>
                                <div class="col-md-4">

                                    <select required class="form-control select2-reset"  id="eps" name="eps"  >


                                    </select>

                                </div>



                                <label class="col-md-1 control-label" title="Nivele de Educación" for="name">T. Sangre</label>

                                <div class="col-md-2">

                                    <select class="form-control mayus" required type="text" name="tipo-sangre" id="tipo-sangre">
                                        <option value="">SELECCIONE</option>
                                        <option value="0-">0-</option>
                                        <option value="0+">0+</option>
                                        <option value="A−">A−</option>
                                        <option value="A+">A+</option>
                                        <option value="B−">B−</option>
                                        <option value="B+">B+</option>
                                        <option value="AB−">AB−</option>
                                        <option value="AB+">AB+</option>

                                    </select>


                                </div>



                            </div>


                            <div class="form-group">

                                <label class="col-md-1 control-label" title="Nivele de Educación" for="name">N. Estud</label>
                                <div class="col-md-4">
                                    <select name="nivel-educacion" required class="form-control mi-select2 form-control-input"  id="nivel-educacion">
                                        <option value="">SELECCIONAR</option>

                                        <?php


                                        foreach ($niveles as $nivel){

                                            echo ' <option value="'.$nivel['codigo'].'">'.$nivel['nombre'].'</option>';

                                        }

                                        ?>

                                    </select>


                                </div>
                                <label class="col-md-1 control-label" title="Nivele de Educación" for="name">Título</label>
                                <div class="col-md-6">
                                    <input  type="text" required  name="titulo" class="form-control mayus">
                                </div>




                            </div>

                            <div class="form-group">

                                <label class="col-md-1 control-label"  for="name">Grupo</label>
                                <div class="col-md-4">


                                    <select required class="form-control select2-reset" id="grupo"  name="grupo">
                                        <option value="">Seleccione</option>


                                        <?php

                                        foreach ($grupos as $grupo) {

                                            ?>

                                            <option value="<?=$grupo['codigo']?>"> <?=$grupo['programa'].' '.$grupo['semestre'].' - '.$grupo['jornada'].' - '.$grupo['grupo']?></option>

                                            <?php
                                        }

                                        ?>


                                    </select>
                                </div>

                                <label class="col-md-2 control-label"  for="name">Auxilio Académico</label>
                                <div class="col-md-2">

                                    <select required class="form-control"  name="auxilio-academico">
                                        <option value="">Seleccione</option>
                                        <option value="0">0%</option>
                                        <option value="50">50%</option>
                                        <option value="75">70%</option>
                                        <option value="100">100%</option>
                                    </select>


                                </div>


                                <label class="col-md-1 control-label"  for="name">Periodo</label>
                                <div class="col-md-2">

                                    <input type="text" readonly name="periodo" class="form-control" value="<?=$periodo?>">
                                </div>

                            </div>


                            <div class="form-group">

                                <label class="col-md-1 control-label"  for="name">Observaciones</label>

                                <div class="col-md-11">

                                    <textarea name="observaciones" class="form-control mayus" id="" cols="30" rows="6"></textarea>

                                </div>

                            </div>



                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">

                                    <input class="btn btn-success pull-right"
                                           type="reset" value="Cancelar">

                                    <input class="btn btn-primary pull-right"  id="btn-matricular" type="submit"
                                           value="Matricular">

                                </div>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
