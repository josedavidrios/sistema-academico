<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Matrículas estudiantes</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">



                        <form id="form-matricula" class="form-horizontal">


                            <div class="form-group">

                                <label class="col-md-1 control-label" for="name">Documento</label>
                                <div class="col-md-2">

                                    <input readonly required id="documento" name="documento" value="<?=$estudiante['documento']?>" type="text"
                                           class="form-control">

                                </div>

                                <label class="col-md-1 control-label" for="name">Nombres</label>
                                <div class="col-md-7">

                                    <input disabled type="text"  value="<?=$estudiante['apellidos']." ".$estudiante['nombres']?>" id="nombres-apellidos" class="form-control">

                                </div>


                                <div class="col-md-1">


                                    <button type="button" class="btn btn-primary full-width"
                                            onclick="abrirModalBuscarEstudiante()">
                                        <i class="fa fa-search"></i>
                                    </button>

                                </div>


                            </div>



                        </form>


                        <table class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>


                                <th>Programa</th>


                                <th width="150">Jornada</th>

                                <th class="text-center" width="50">Periodo</th>
                                <th  width="90">Fecha</th>
                                <th class="text-center" width="80">ACTIVA</th>
                                <th class="text-center" width="50">Operación</th>

                            </tr>
                            </thead>
                            <tbody id="listado">



                            <?php


                            if (isset($matriculas)){

                                foreach ($matriculas as $matricula) {

                                    $codigo = $matricula['codigo_matricula'];

                                    echo '<tr>
    
                                            <td>' . $matricula['nombre'] ." ".$matricula['semestre']." - ". $matricula['grupo'].'</td>
                
                                            <td >' . $matricula['nombre_jornada'] . '</td>
                               
                                            <td class="text-center">' . $matricula['periodo'] . '</td>
                                            <td id="' . $codigo . '" class="text-center">' . formato_estado_matricula($matricula['activa'])  . '</td>
                                            <td class="text-center"> 
                                                <a href="javascript:cancelarMatricula(' . $codigo . ')" class="fa fa-trash"></a>
                                                <a href="javascript:editarMatricula(' . $codigo . ')" class="fa fa-pencil"></a>
                                            </td>
                                        </tr>';


                                }

                            }
                            ?>


                            </tbody>

                        </table>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->



<div class="modal modal-wide35 fade" id="editar-matricula" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Editar Matrícula</b></h4>
            </div>


            <div class="modal-body">


                <form id="crear-docente" class="form-horizontal" method="post" action="<?= base_url('admin/editarMatricula') ?>"  >
                    <div class="modal-body">


                        <div class="form-group">


                            <input  required class="col-md-1" id="operacion" name="operacion" value="registrar" type="hidden"
                                    class="form-control">


                        </div>

                        <div class="form-group">



                            <label class="col-md-1 control-label" for="name">Grupo</label>

                            <input type="hidden" name="codigo" id="codigo">
                            <input type="hidden" name="documento-estudiante" id="documento-estudiante">

                            <div class="col-md-11">



                                <select required id="nuevo-grupo" style="width:100%" name="grupo"
                                        class="form-control select2-reset">
                                    <option value="">Seleccione</option>


                                    <?php

                                    foreach ($grupos as $grupo) {

                                        ?>

                                        <option value="<?=$grupo['codigo']?>"> <?=$grupo['programa'].' '.$grupo['semestre'].' - '.$grupo['jornada'].' - '.$grupo['grupo'].' -  '.$grupo['periodo'] ?></option>

                                        <?php
                                    }

                                    ?>


                                </select>




                            </div>





                        </div>




                        <div class="clearfix"></div>

                    </div>

                    <div class="form-group">

                        <div id="mensaje" class="col-md-12">

                        </div>


                    </div>


                    <div class="modal-footer">
                        <input type="submit" id="bt-operacion" value="Editar" class="btn btn-primary"/>
                        <input type="button" data-dismiss="modal" value="Cancelar" class="btn btn-success"/>

                    </div>
                </form>




            </div>
        </div>
    </div>
</div>


<?php


$this->load->view('admin/estudiantes/modal_consultar_matricula_estudiante');

?>
