<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Matrícular asignatura <?=$periodo?></h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!--

                        onsubmit="matriculasPrimerIngreso()"
                        -->

                        <form id="form-matricula-primer-ingreso" class="form-horizontal" method="post" action="<?=base_url('admin/matricularAsignatura')?>">








                            <div class="form-group">

                                <label class="col-md-1 control-label"  for="name">Grupo</label>
                                <div class="col-md-5">


                                    <select required class="form-control select2-reset" id="grupo"  name="grupo">
                                        <option value="">Seleccione</option>


                                        <?php

                                        foreach ($grupos as $grupo) {

                                            ?>

                                            <option title="<?=$grupo['codigo_programa']?>" label="<?=$grupo['semestre']?>"  value="<?=$grupo['codigo']?>"> <?=$grupo['programa'].' '.$grupo['semestre'].' - '.$grupo['codigo_jornada'].' - '.$grupo['grupo']?></option>

                                            <?php
                                        }

                                        ?>


                                    </select>
                                </div>



                                <label class="col-md-1 control-label"  for="nombres">Asiganaturas: </label>

                                <div class="col-md-5">



                                    <select name="asignatura" required id="asignaturas" class="form-control select2">

                                        <option value=""></option>


                                    </select>
                                </div>



                            </div>



                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">

                                    <input class="btn btn-success pull-right"
                                           type="reset" value="Cancelar">

                                    <input class="btn btn-primary pull-right"  id="btn-matricular" type="submit"
                                           value="Matricular">

                                </div>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
