<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Matricula</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <form id="form-matricula" class="form-horizontal">

                            <div class="form-group">


                                <label class="col-md-1 control-label" for="name">Periodo</label>

                                <div class="col-md-2">


                                    <select class="form-control"  id="periodo" onchange="consultarGruposPorPerido(this.value)">
                                        <option value="">Seleccione</option>

                                        <?php

                                            foreach ($periodos as $periodo){
                                        ?>

                                                <option value="<?=$periodo['codigo']?>"><?=$periodo['codigo']?> </option>

                                        <?php

                                            }

                                        ?>






                                    </select>

                                </div>

<!--
                                <label class="col-md-1 control-label" for="name">Estado</label>
-->
                                <div class="col-md-2">

                                    <select class="form-control"  onchange="consultarGruposPorPerido2()" name="estado" id="estado">

                                        <option value="1">ACTIVOS</option>
                                        <option value="0">CANCELADOS</option>
                                        <option value="todos">TODOS</option>
                                    </select>


                                </div>


                                <label class="col-md-1 control-label" for="name"> Grupo</label>
                                <div class="col-md-5">


                                    <select id="grupo" required id="grupo" onchange="consultarEstudiantesPorGrupo(this.value)" name="grupo"
                                            class="form-control">
                                        <option value="">Seleccione el grupo</option>



                                    </select>


                                </div>



                                <div class="col-md-1">

                                    <a id="btn-exportar-excel" href="#" target="_blank" class="btn btn-primary full-width">
                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    </a>

                                </div>





                            </div>


                        </form>


                        <table class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <th width="20">#</th>
                                <th width="100">Documento</th>
                                <th>Apellidos y nombres</th>
                                <th>Correo</th>
                                <th width="100">Celular</th>


                            </tr>
                            </thead>
                            <tbody id="listado">


                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php


$this->load->view('admin/estudiantes/modal_buscar_estudiante');

?>
