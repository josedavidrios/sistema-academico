<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>asignaturas semestrales</h2>

                            </div>

                            <div class="col-xs-2">

                                <button class="pull-right full-width btn btn-primary" onclick="abrirModalAignarAsignatura()">Nueva

                                    <i class="fa fa-plus-circle"></i>

                                </button>
                            </div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <form  class="form-horizontal" id="asignaturas-semestrales">




                            <div class="form-group">


                                <label class="col-md-1 control-label" for="name">Prograna</label>

                                <div class="col-md-4">


                                    <select  class="form-control"  name="programa" onchange="consultarAsignaturasSemestrales()" >
                                        <option value="">Seleccione</option>

                                        <?php

                                        foreach ($programas as $programa){
                                            ?>

                                            <option value="<?=$programa['codigo']?>"><?=$programa['nombre']?> </option>

                                            <?php

                                        }

                                        ?>








                                    </select>

                                </div>




                                    <label class="col-md-1 control-label" for="name">Semestre</label>
                                    <div class="col-md-2">


                                        <select id="grupo" required id="grupo" onchange="consultarAsignaturasSemestrales()" name="semestre"
                                                class="form-control">
                                            <option value="">Seleccione</option>
                                            <option value="I">I</option>
                                            <option value="II">II</option>
                                            <option value="III">III</option>
                                            <option value="IV">IV</option>




                                        </select>

                                </div>

                                <label class="col-md-1 control-label" for="name">Jornada</label>
                                <div class="col-md-3">


                                    <select id="grupo" required id="jornada" onchange="consultarAsignaturasSemestrales()" name="jornada"
                                            class="form-control">
                                        <option value="">Seleccione</option>
                                        <option value="LV">LUNES A VIERNES</option>
                                        <option value="S">SÁBADO</option>



                                    </select>
                                </div>






                            </div>

                        </form>


                        <table id="datatable-asignar-directores"
                               class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <th width="10">#</th>
                                <th width="50">Cód</th>


                                <th >NOMBRE</th>
                                <th width="50" >REMOVER</th>





                            </tr>
                            </thead>
                            <tbody id="agrega-registros">


                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<div class="modal modal-wide2 fade" id="modal-asignatura-semestral" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Asignatura Semestral</b></h4>
            </div>


            <form id="asignatura-semestral" class="form-horizontal" method="post" action="<?= base_url('admin/registrarAsignatura') ?>"
                  onsubmit="registrarAsiganaturaSemestral();">
                <div class="modal-body">




                    <div class="form-group">

                        <label class="col-md-2 control-label" for="name">Asignatura*</label>
                        <div class="col-md-10">




                            <select class="form-control" name="asignatura" id="asignatura">


                                <option value="">SELECCIONAR</option>

                                    <?php foreach ($asignaturas as $asignatura) : ?>

                                        <option value="<?=$asignatura['codigo']?>"> <?=$asignatura['nombre']?></option>

                                    <?php endforeach; ?>

                            </select>
                         </div>


                    </div>





                    <div class="form-group">


                        <label class="col-md-2 control-label" for="name">Prograna</label>

                        <div class="col-md-3">


                            <select class="form-control"  name="programa">
                                <option value="">Seleccione</option>


                                <?php

                                foreach ($programas as $programa){
                                    ?>

                                    <option value="<?=$programa['plan_de_estudio']?>"><?=$programa['nombre']?> </option>

                                    <?php

                                }

                                ?>








                            </select>

                        </div>


                        <label class="col-md-1 control-label" for="name">Semestre</label>
                        <div class="col-md-2">


                            <select required  name="semestre"
                                    class="form-control">
                                <option value="">Seleccione</option>
                                <option value="I">I</option>
                                <option value="II">II</option>
                                <option value="III">III</option>
                                <option value="IV">IV</option>




                            </select>

                        </div>

                        <label class="col-md-1 control-label" for="name">Jornada</label>
                        <div class="col-md-3">


                            <select required name="jornada"
                                    class="form-control">
                                <option value="">Seleccione</option>
                                <option value="LV">LUNES A VIERNES</option>
                                <option value="S">SÁBADO</option>



                            </select>
                        </div>

                    </div>


                    <div class="clearfix">

                    </div>

                </div>
                <div class="form-group ">

                    <div id="mensaje" class="col-md-12">

                    </div>


                </div>

                <div class="modal-footer">

                    <input type="submit" id="bt-operacion" value="Registrar" class="btn btn-primary"/>

                </div>
            </form>
        </div>
    </div>
</div>

