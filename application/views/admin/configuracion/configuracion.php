<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>CONFIGURACIÓN</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <form method="post" class="form-horizontal form-label-left"  action="<?=base_url("superAdmin/registrarConfiguracion")?>">

                        <table class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>


                                <th>CONFIGURACIÓN</th>


                                <th class="text-center" width="50">OPCIONES</th>

                            </tr>
                            </thead>
                            <tbody>

                            <tr>

                                <td>Corte actual para la publicación de notas para la jornada matinal</td>
                                <td>
                                    <input type="text" name="corte-actual" required value="<?=$configuracion['corte_actual']?>" class="form-control">



                                </td>

                            </tr>


                            <tr>


                                <td>Matrículas académicas automaticas</td>

                                <td class="text-center">


                                    <label>
                                        <input type="checkbox"
                                               class="js-switch" name="matriculas-academicas-atutomaticas" <?= $configuracion['matriculas_academicas'] == "1" ? "checked" : "" ?> />
                                    </label>

                                </td>

                            </tr>

                            <tr>


                                <td>Solicitar Actualización de Datos Personales</td>

                                <td class="text-center">


                                    <label>
                                        <input type="checkbox"
                                               class="js-switch" name="solicitar-actualizacion-datos-personales" <?= $configuracion['actualizacion_datos_personales'] == "1" ? "checked" : "" ?> />
                                    </label>

                                </td>

                            </tr>


                            <tr>


                                <td>Número de la fila donde inician las notas en la planilla</td>


                                <td>
                                    <input type="text" name="fila-notas" required value="<?=$configuracion['fila_notas']?>" class="form-control">



                                </td>


                            </tr>

                            <tr>


                                <td>Número de la columna donde inician las notas en la planilla</td>


                                <td>
                                    <input type="text" name="columna-notas" required value="<?=$configuracion['columna_notas']?>" class="form-control">



                                </td>


                            </tr>


                            <tr>


                                <td>Inscripciones web</td>

                                <td class="text-center">


                                    <label>
                                        <input type="checkbox"
                                               class="js-switch" name="inscripciones-web" <?= $configuracion['inscripciones_web'] == "1" ? "checked" : "" ?> />
                                    </label>

                                </td>

                            </tr>



                            </tbody>

                        </table>



                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">


                                    <input class="btn btn-primary pull-right"  id="btn-matricular" type="submit"
                                           value="Guardar">

                                </div>
                            </div>


                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->



