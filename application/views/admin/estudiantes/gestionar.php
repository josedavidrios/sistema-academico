<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Gestonar estudiantes</h2>

                            </div>

                            <div class="col-xs-2"></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <form class="form-horizontal">


                            <div class="form-group">

                                <label class="col-md-2 control-label" for="name">Buscar estudiante</label>
                                <div class="col-md-10">

                                    <input placeholder="Nombres o apellidos" id="filtro-estudiante" type="text"
                                           class="form-control mayus" onkeyup="return filtrarEstudiante()">

                                </div>


                            </div>

                        </form>


                        <table class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <th width="100">Documento</th>
                                <th>Nombres y apellidos</th>
                                <th width="100">Celular</th>
                                <th width="200">Correo</th>
                                <th width="10">ACCIONES</th>


                            </tr>
                            </thead>
                            <tbody id="agrega-registros">


                            <?php


                            if (isset($estudiante)) {


                                echo '<tr>

                                                <td width="50">' . $estudiante['documento'] . '</td>
                                                <td>' . $estudiante['apellidos'] . ' ' . $estudiante['nombres'] . '</td>
                                                <td>' . $estudiante['celular'] . '</td>
                                                <td>' . $estudiante['correo'] . '</td>
                                                
                                                <td class="text-center">
                                                    <a title="Editar" href="javascript:abrirModalEditarEstudiante(' . $estudiante['documento'] . ')" class="fa fa-edit"></a>
                    
                                                    <a title="Restablecer Clave" href="javascript:abrirModalRestablecerClave(' . $estudiante['documento'] . ')" class="fa fa-key"></a>
                                                  
                                                 </td>
                                  
                                         </tr>';

                            }


                            ?>


                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<div class="modal modal-wide3 fade" id="modal-registrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Actualizar datos de estudiantes </b></h4>
            </div>


            <form id="form-matricula-primer-ingreso" class="form-horizontal" method="post" action="<?=base_url('admin/actualizarDatosUsuarios')?>">
                <div class="modal-body">


                    <div class="form-group">


                        <label class="col-md-1 control-label" for="name">Documento</label>
                        <div class="col-md-3">

                            <input required id="documento" name="documento" id="documento" type="hidden"
                                   class="form-control">

                            <input required  name="nuevo-documento" id="nuevo-documento" type="number"
                                   class="form-control">


                            <input required  name="cambio-clave" id="cambio-clave" type="hidden"
                                   class="form-control">

                        </div>


                        <label class="col-md-1 control-label" for="name">Expedida en </label>
                        <div class="col-md-4">

                            <select required style="width: 100%" class="form-control   lugares"
                                    id="lugar-expedicion-documento" name="lugar-expedicion-documento">


                            </select>

                        </div>

                        <label class="col-md-1 control-label" for="name">Tipo</label>
                        <div class="col-md-2">

                            <select class="form-control form-control-input" required="" name="tipo-documento"
                                    id="tipo-documento">
                                <option value="">SELECCIONE</option>

                                <option value="CC">CEDULA DE CIUDADANÍA</option>
                                <option value="TI">TARJETA DE IDENTIDAD</option>
                                <option value="RC">REGISTRO CIVIL</option>
                                <option value="CE">Cédula de Extranjería</option>

                            </select>

                        </div>


                    </div>

                    <div class="form-group">


                        <label class="col-md-1 control-label" for="name">Nombres</label>
                        <div class="col-md-3">

                            <input type="text" id="nombres" name="nombres" class="form-control mayus">

                        </div>


                        <label class="col-md-1 control-label" for="name">Apellidos</label>
                        <div class="col-md-4">

                            <input type="text" id="apellidos" name="apellidos" class="form-control mayus">

                        </div>

                        <label class="col-md-1 control-label" for="name">Sexo</label>
                        <div class="col-md-2">

                            <select required class="form-control mayus" id="sexo" name="sexo">
                                <option value="">Seleccione</option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                                <option value="NE">No especifica</option>

                            </select>

                        </div>
                    </div>

                    <div class="form-group">

                        <label class="col-md-1 control-label" for="name">Nació en </label>
                        <div class="col-md-3">

                            <select required style="width: 100%" id="lugar-nacimiento"
                                    class="form-control select2-reset lugares" name="lugar-nacimiento">


                            </select>

                        </div>


                        <label class="col-md-1 control-label" for="name">Vives en </label>
                        <div class="col-md-4">

                            <select style="width: 100%" required class="form-control select2-reset lugares"
                                    id="lugar-residencia" name="lugar-residencia">


                            </select>

                        </div>


                        <label class="col-md-1 control-label" title="Fecha de Nacimiento" for="name">Nació el</label>
                        <div class="col-md-2">

                            <input type="date" id="fecha-nacimiento" required name="fecha-nacimiento"
                                   class="form-control mayus">
                        </div>

                    </div>


                    <div class="form-group">


                        <label class="col-md-1 control-label" for="name">Celular</label>
                        <div class="col-md-3">

                            <input type="number" name="celular" id="celular" class="form-control">

                        </div>


                        <label class="col-md-1 control-label" for="name">Correo</label>
                        <div class="col-md-4">

                            <input type="email" name="correo" autocomplete="off" id="correo" class="form-control mayus">

                        </div>

                        <label class="col-md-1 control-label" for="name">Estrato</label>
                        <div class="col-md-2">

                            <select required class="form-control" id="estrato" name="estrato">
                                <option value="">Seleccione</option>
                                <option value="1">UNO</option>
                                <option value="2">DOS</option>
                                <option value="3">TRES</option>
                                <option value="4">Cuatro</option>
                                <option value="5">CINCO</option>
                                <option value="6">SEIS</option>

                            </select>

                        </div>
                    </div>


                    <div class="form-group">


                        <label class="col-md-1 control-label" for="name">Barrio</label>
                        <div class="col-md-3">

                            <input type="text" id="barrio" name="barrio" class="form-control mayus">

                        </div>


                        <label class="col-md-1 control-label" for="name">Dirección</label>
                        <div class="col-md-4">

                            <input type="text" name="direccion" id="direccion" class="form-control mayus">

                        </div>


                        <label class="col-md-1 control-label" for="name">Zona</label>

                        <div class="col-md-2">

                            <select name="zona" required class="form-control" id="zona">
                                <option value="">SELECCIONE</option>
                                <option value="URBANA">URBANA</option>
                                <option value="RURAL">RURAL</option>


                            </select>
                        </div>


                    </div>


                    <div class="form-group">


                        <label class="col-md-1 col-xs-12 control-label" for="name">Est. Civil</label>

                        <div class="col-md-3 col-xs-12">
                            <select name="estado-civil" style="width: 100%" id="estado-civil" required
                                    class="form-control form-control-input">
                                <option value="">SELECCIONE</option>

                                <?php


                                foreach ($estados_civiles as $estado) {


                                    ?>
                                    <option value="<?= $estado['codigo'] ?>"><?= $estado['nombre'] ?></option>
                                    <?php


                                }

                                ?>

                            </select>


                        </div>

                        <label class="col-md-1 control-label" for="name">EPS </label>
                        <div class="col-md-4">

                            <select required class="form-control select2-reset" id="eps" name="eps">


                            </select>

                        </div>


                        <label class="col-md-1 control-label" title="Nivele de Educación" for="name">T. Sangre</label>

                        <div class="col-md-2">

                            <select class="form-control mayus" required type="text" name="tipo-sangre" id="tipo-sangre">
                                <option value="">SELECCIONE</option>
                                <option value="0-">0-</option>
                                <option value="0+">0+</option>
                                <option value="A−">A−</option>
                                <option value="A+">A+</option>
                                <option value="B−">B−</option>
                                <option value="B+">B+</option>
                                <option value="AB−">AB−</option>
                                <option value="AB+">AB+</option>

                            </select>


                        </div>


                    </div>


                    <div class="form-group">

                        <label class="col-md-1 control-label" title="Nivele de Educación" for="name">N. Estud</label>
                        <div class="col-md-4">
                            <select name="nivel-educacion" style="width: 100%" required
                                    class="form-control  form-control-input w-100sss" id="nivel-educacion">
                                <option value="">SELECCIONAR</option>

                                <?php


                                foreach ($niveles as $nivel) {

                                    echo ' <option value="' . $nivel['codigo'] . '">' . $nivel['nombre'] . '</option>';

                                }

                                ?>

                            </select>


                        </div>
                        <label class="col-md-1 control-label" title="Nivele de Educación" for="name">Título</label>
                        <div class="col-md-6">
                            <input type="text"  id="titulo" name="titulo" class="form-control mayus">
                        </div>


                    </div>


                </div>

                <div class="modal-footer">

                    <input type="submit" value="Actualizar" class="btn btn-primary"/>


                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal modal-wide40 fade" id="modal-restablecer-clave" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Restablecer clave </b></h4>
            </div>


            <form id="form-editar-estudiante" class="form-horizontal" method="post"
                  action="<?= base_url("admin/restablecerClave") ?>">
                <div class="modal-body">


                    <div class="form-group">

                        <input required id="documento-restablecer-clave" name="documento" type="hidden"
                               class="form-control">
                        <label class="col-md-3 control-label" for="name">Nueva Clave</label>

                        <div class="col-md-9">

                            <input required id="nueva-clave" name="nueva-clave" type="text" class="form-control">

                        </div>

                    </div>

                    <div class="form-group ">

                        <div id="mensaje" class="col-md-12">

                        </div>


                    </div>

                </div>

                <div class="modal-footer">

                    <input type="submit" id="bt-operacion" value="Restablecer" class="btn btn-primary"/>


                </div>
            </form>
        </div>
    </div>
</div>