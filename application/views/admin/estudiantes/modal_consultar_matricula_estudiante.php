<div class="modal modal-wide2 fade" id="modal-buscar-estudiante" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">BUSCAR ESTUDIANTES</b></h4>
            </div>


            <div class="modal-body">


                <div class="container">

                    <div class="row">

                        <div class="col-md-12">

                            <input required class="form-control btn-general-facturas mayus" autocomplete="off"
                                   id="nombres-estudiantes" autofocus  name="usuario" type="text"
                                   placeholder="Apellidos y nombres o el código del usuario" >


                            <table id="datatable-responsive"
                                   class="table table-striped table-bordered dt-responsive nowrap table-hover table-fixed" cellspacing="0"
                                   width="100%">

                                <tbody id="agrega-usuarios">




                                </tbody>

                            </table>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
