<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-md-10">

                                <h2>GESTIONAR DOCENTES</h2>

                            </div>

                            <div class="pull-right col-xs-2">

                                <button class="right btn btn-primary full-width" onclick="abrirModalCrearDocente()">
                                    Nuevo

                                    <i class="fa fa-plus-circle"></i>

                                </button>
                            </div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <form class="form-horizontal">


                            <div class="form-group">

                                <label class="col-md-2 control-label" for="name">Buscar docente</label>
                                <div class="col-md-10">

                                    <input placeholder="Nombres o apellidos" id="filtro-docente" type="text"
                                           class="form-control mayus" onkeyup="return filtrarDocente()">

                                </div>


                            </div>

                        </form>


                        <table id="datatable-asignar-directores"
                               class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <th width="150">Documento</th>
                                <th>nombres</th>
                                <th width="250">Correo Instutucional</th>
                                <th width="100">celular</th>
                                <th width="10">Activo</th>
                                <th width="10">ACCIONES</th>






                            </tr>
                            </thead>
                            <tbody id="agrega-registros">


                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<div class="modal modal-wide3 fade" id="modal-registrar"  role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Registro de docentes</b></h4>
            </div>

            <!--
           onsubmit="return registrarDocente();"

            -->

            <form id="crear-docente" class="form-horizontal" method="post" action="<?= base_url('admin/registrarDocente') ?>"  onsubmit="registrarDocente()">
                <div class="modal-body">


                    <div class="form-group">


                        <input  required class="col-md-1" id="operacion" name="operacion" value="registrar" type="hidden"
                               class="form-control">


                    </div>

                    <div class="form-group">

                        <label class="col-md-2 control-label" for="name">Tipo Documento</label>
                        <div class="col-md-2">

                            <select class="form-control form-control-input" required="" name="tipo-documento" id="tipo-documento">
                                <option value="">SELECCIONE</option>

                                <option value="CC">CEDULA DE CIUDADANÍA</option>

                                <option value="CE">Cédula de Extranjería</option>

                            </select>

                        </div>

                        <label class="col-md-1 control-label" for="name">Documento*</label>
                        <div class="col-md-4">

                            <input required id="documento" name="documento" type="number" class="form-control">

                        </div>


                        <label class="col-md-1 control-label" for="name">Fecha nac*</label>
                        <div class="col-md-2">
                            <input  id="fecha-nacimiento" name="fecha-nacimiento" type="date"
                                    class="form-control">

                        </div>



                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Nombres*</label>
                        <div class="col-md-4">
                            <input required id="nombres" name="nombres" type="text" class="form-control mayus">

                        </div>

                        <label class="col-md-1 control-label" for="name">Apellidos*</label>
                        <div class="col-md-5">
                            <input required id="apellidos" name="apellidos" type="text" class="form-control mayus">

                        </div>


                    </div>


                    <div class="form-group">


                        <label class="col-md-2 control-label" for="name">Correo Personal</label>
                        <div class="col-md-3">
                            <input id="correo" name="correo" type="email" class="form-control mayus">

                        </div>







                        <label class="col-md-1 control-label" for="name">Celular</label>
                        <div class="col-md-3">

                            <input  type="number" name="celular" id="celular" class="form-control">

                        </div>

                        <label class="col-md-1 control-label" for="name">Sexo*</label>
                        <div class="col-md-2">

                            <select required class="form-control" id="sexo" name="sexo">
                                <option value="">Seleccione</option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>

                            </select>


                        </div>


                    </div>


                    <div class="form-group">





                        <label class="col-md-2 control-label" for="name">Residencia </label>
                        <div class="col-md-3">

                            <select required class="form-control select2-reset" name="lugar-residencia"  id="lugar-residencia" style="width:100%" >


                            </select>

                        </div>

                        <label class="col-md-1 control-label" for="name">Dirección</label>
                        <div class="col-md-2">

                            <input  type="text" name="direccion"  id="direccion" class="form-control mayus">

                        </div>

                        <label class="col-md-1 control-label" for="name">Barrio</label>
                        <div class="col-md-3">

                            <input  type="text" id="barrio" name="barrio" class="form-control mayus">

                        </div>


                    </div>


                    <div class="form-group">




                    </div>

                        <div class="form-group">

                            <label class="col-md-2 control-label" title="Nivele de Educación" for="name">Nivel de Educación</label>
                            <div class="col-md-3">
                                <select name="nivel-educacion" required class="form-control mi-select2 form-control-input"  id="nivel-educacion">
                                    <option value="">SELECCIONAR</option>

                                    <?php


                                    foreach ($niveles as $nivel){

                                        echo ' <option value="'.$nivel['codigo'].'">'.$nivel['nombre'].'</option>';

                                    }

                                    ?>

                                </select>
                            </div>

                            <label class="col-md-1 control-label" title="Titulo" for="name">Título</label>

                            <div class="col-md-6">

                                <input  type="text" name="titulo" id="titulo" class="form-control">

                            </div>


                        </div>








                    <div class="clearfix"></div>

                </div>

                <div class="form-group">

                    <div id="mensaje" class="col-md-12">

                    </div>


                </div>


                <div class="modal-footer">
                    <input type="submit" id="bt-operacion" value="Crear" class="btn btn-primary"/>
                    <input type="button" data-dismiss="modal" value="Cancelar" class="btn btn-success"/>

                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal modal-wide40 fade" id="modal-restablecar-clave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Restablecer clave </b></h4>
            </div>


            <form id="form-editar-estudiante" class="form-horizontal" method="post" action="<?=base_url("admin/restablecerClave")?>">
                <div class="modal-body">


                    <div class="form-group">

                        <input required id="documento-restablecer-clave" name="documento" type="hidden" class="form-control">
                        <label class="col-md-3 control-label" for="name">Nueva Clave</label>

                        <div class="col-md-9">

                            <input required id="nueva-clave" name="nueva-clave" type="text" class="form-control">

                        </div>

                    </div>

                    <div class="form-group ">

                        <div id="mensaje" class="col-md-12">

                        </div>


                    </div>

                </div>

                <div class="modal-footer">

                    <input type="submit" id="bt-operacion" value="Restablecer" class="btn btn-primary"/>


                </div>
            </form>
        </div>
    </div>
</div>