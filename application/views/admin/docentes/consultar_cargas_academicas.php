<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row" id="datatable-asignar-carga-academica">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-6">


                                <h2> CARGAS ACADÉMICA</h2>

                            </div>



                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <!-- onsubmit="guardarCargarAcademica()" -->

                        <form class="form-horizontal" action="<?= base_url('admin/guardarCargaAcademica') ?>"
                              method="post">


                            <div class="form-group">


                                <label for="" class="control-label col-md-1">Docente</label>
                                <div class="col-xs-9">




                                    <select class="form-control" name="" id="docente" onchange="selecionarDocente(this.value)">

                                        <option value=""></option>


                                    </select>

                                </div>


                                <div class="col-xs-2">

                                    <a class="pull-right full-width btn btn-primary"
                                       href="<?= base_url('admin/docentes/cargas-academicas/crear') ?>">Nueva

                                        <i class="fa fa-plus-circle"></i>
                                    </a>

                                </div>

                            </div>


                        </form>


                        <table id="datatable-asignar-carga-academica"
                               class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>


                                <th class="text-center" width="30">#</th>
                                <th width="30">CÓD</th>

                                <th width="30">AS</th>

                                <th>ASIGNATURA</th>
                                <th>PROGRAMA</th>
                                <th width="14" class="text-center">SEM</th>
                                <th width="130">JORNADA</th>
                                <th width="20">GRUPO</th>
                                <th width="20">SUBIÓ NOTAS</th>
                                <th width="20">ACCIONES</th>

                            </tr>
                            </thead>
                            <tbody id="agrega-registros">


                            </tbody>

                        </table>


                    </div>


                </div>
            </div>
        </div>





    </div>
</div>



<div class="modal modal-wide55 fade" id="modal-cambiar-docente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">CAMBIAR DOCENTE CARGA ACADÉMICA</b></h4>
            </div>


            <form id="form-cambio-docente" class="form-horizontal" method="post" action="<?= base_url('admin/registrarAsignatura') ?>"
                  onsubmit="return cambiarDocenteCargaAcademica();">
                <div class="modal-body">


                    <div class="form-group">

                        <label class="col-md-2 control-label" for="name">Nuevo Docente</label>
                        <div class="col-md-10">

                            <input type="hidden" name="codigo-carga" id="codigo-carga">

                            <select style="width: 100%" class="form-control select2" name="nuevo-docente" id="nuevo-docente">


                                    <?php foreach ($docentes as $docente) : ?>


                                            <option value="<?=$docente['documento']?>"><?=$docente['nombres']?></option>

                                    <?php endforeach; ?>



                            </select>

                        </div>

                    </div>




                    <div class="clearfix">

                    </div>

                </div>
                <div class="form-group ">

                    <div id="mensaje" class="col-md-12">

                    </div>


                </div>

                <div class="modal-footer">

                    <input type="submit" id="bt-operacion" value="Cambiar" class="btn btn-primary"/>


                </div>
            </form>
        </div>
    </div>
</div>






