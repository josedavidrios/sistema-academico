<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">


                        <div class="row">

                            <div class="col-xs-10">

                                <h2>Evaluación Docente </h2>

                            </div>

                            <div class="col-xs-2">


                                <?php


                                    if (isset($nuevo[0]['codigo'])){

                                        ?>

                                        <a class="right btn btn-primary full-width" href="<?=base_url('superAdmin/registrarEvacuacionesDocentes')?>">Crear <?=$nuevo[0]['codigo']?>

                                        <i class="fa fa-plus-circle"></i>

                                        </a>





                                <?php
                                    }



                                ?>



                            </div>

                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">




                        <table id="datatable-grupos"
                               class="table table-striped table-bordered dt-responsive table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>


                                <th width="4">Código</th>
                                <th>Periodo</th>
                                <th width="5" title="Matrículas Abiertas">Activa</th>


                            </tr>
                            </thead>
                            <tbody id="agrega-registros">



                            <?php

                                foreach ($evaluaciones as $evaluacion){

                                    $checkedActiva="";

                                    if ($evaluacion['estado']==1){

                                        $checkedActiva="checked";

                                    }




                             ?>

                            <tr>

                                <td><?=$evaluacion['codigo']?></td>
                                <td><?=$evaluacion['periodo']?></td>


                                <td class="text-center">

                                    <label>
                                        <input type="checkbox" id="<?=$evaluacion['codigo']?>" onclick="activarEvaluacion(this.id, this.value)" class="js-switch"    <?= $checkedActiva ?> />
                                    </label>

                                </td>



                            </tr>



                            <?php

                                }

                            ?>


                            </tbody>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<div class="modal modal-wide55 fade" id="modal-crear-evaluacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">

                    <i class="fa fa-bars"></i>
                    <b id="titulo-modal">Registro de grupos</b></h4>
            </div>



            <form id="crear-periodo" method="post" class="form-horizontal form-label-left"  action="<?=base_url('superadmin/registrarPeriodo')?>" onsubmit="crearPeriodo()">


                <br>

                <div class="item form-group">
                    <label class="control-label col-md-1 for="name">Año <span class="required">*</span>
                    </label>


                    <div class="col-md-2">


                        <?php

                        $anio_actual =date('Y');

                        $anio_siguiente = strtotime ( '+1 year' , strtotime ( $anio_actual ) ) ;
                        $anio_siguiente = date ( 'Y' , $anio_siguiente );


                        ?>

                        <select class="form-control" name="anio" id="">
                            <option value="">SELECCIONE</option>
                            <option value="<?= $anio_actual ?>"><?= $anio_actual ?></option>
                            <option value="<?= $anio_siguiente ?>"><?= $anio_siguiente ?></option>

                        </select>

                    </div>

                    <label class="control-label col-md-1" for="name">Período <span class="required">*</span>
                    </label>
                    <div class="col-md-2">

                        <select name="periodo" required class="form-control" id="">
                            <option value="">SELECCIONE</option>
                            <option value="1">1</option>
                            <option value="2">2</option>

                        </select>

                    </div>





                    <label class="control-label col-md-1" for="name">Fecha de incio <span class="required">*</span>
                    </label>
                    <div class="col-md-2">


                        <input  class="form-control col-md-7 col-xs-12" name="fecha-incio" id="fecha-incio" required="required" type="date">


                    </div>


                    <label class="control-label col-md-1" for="name">Fecha de fin <span class="required">*</span>
                    </label>
                    <div class="col-md-2">


                        <input  class="form-control"  name="fecha-fin" id="fecha-fin" required="required" type="date">


                    </div>




                </div>



                <div class="form-group ">

                    <div id="mensaje" class="col-md-offset-3 col-md-6">


                    </div>


                </div>





                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-5">
                        <button id="send" type="submit" class="btn btn-primary">Crear</button>
                        <button type="reset" class="btn btn-success">Cancelar</button>

                    </div>
                </div>
            </form>


        </div>
    </div>
</div>

