<!-- page content -->
<div class="right_col" role="main">
    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <h2 class="mayus">Estudiantes para evaluar en el  <b> corte #<?=$corte?></b> </h2>
                        <input type="hidden" name="" value="<?=$codigoPrograma?>" id="codigo-programa">

                        <input type="hidden" name="" value="<?=$corte ?>" id="corte">
                        <input type="hidden" name="" value="<?=$carga ?>" id="carga">

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-hover"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <th width="30">#</th>
                                <th width="60">Código</th>
                                <th>Estudiante</th>
                                <th width="150">Nota</th>

                            </tr>
                            </thead>
                            <tbody>


                            <form action=""  onsubmit="terminarRegistroDeNotas()">

                            <?php

                            $i=1;


                            foreach ($estudiantes as $estudiantes) {


                                echo '<tr>
                                        
                                         <td>' . $i. '</td>
                                                <td>' . $estudiantes['codigo_matricula'] . '</td>
                                                <td>' . $estudiantes['nombre'] . '</a></td>
                                               <td>
                                               
                                               <input required step="any" class="form-control nota decimal-2-places-positive numberBox"  min="1" max="5" onchange="registrarNota(' . $estudiantes['codigo_matricula'] . ',this.value)"  value="'.$estudiantes['nota'].'" type="number" name="" id="">
                                               
                                               
                                                </td>
                                                 

                                            </tr>';


                                $i++;
                            }

                            ?>




                            </tbody>


                            
                            

                        </table>

                        <div class="checkbox">
                            <label><input id="confirmacion" required type="checkbox" value="">Confirmo que he revisado correctamente las notas antes de publicarlas </label>
                        </div>

                        <input type="reset" class="btn btn-success pull-right" value="Cancelar">

                        <input type="submit" class="btn btn-primary pull-right" value="Registrar notas">

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

