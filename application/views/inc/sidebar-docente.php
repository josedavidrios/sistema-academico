<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Docente</h3>
        <ul class="nav side-menu">

            <li><a href="<?= base_url('docente') ?>"><i class="fa fa-home"></i> Inicio </a></li>


            <?php

            if ($matricula_aula_virtual>0) {

                    ?>

                    <li><a target="_blank" href="<?= base_url('sesion/iniciarMoodle') ?>"><i
                                    class="fa fa-graduation-cap"></i> Aula Virtual </a></li>


                    <?php
                }




            ?>


            <li><a><i class="fa fa-edit"></i>Publicación de notas <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">


                    <li><a href="<?= base_url('docente/notas/lv') ?>">Lunes a Viernes</a></li>
                    <li><a href="<?= base_url('docente/notas/s') ?>">Sábados</a></li>


                </ul>
            </li>

            <li><a><i class="fa fa-file-excel-o"></i>Planillas <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">


                    <li><a href="<?= base_url('docente/matriculas/listado/lv') ?>">Lunes a Viernes</a></li>
                    <li><a href="<?= base_url('docente/matriculas/listado/s') ?>">Sábados</a></li>


                </ul>
            </li>










        </ul>


    </div>


</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Cerrar sesión" href="<?= base_url('login/cerrar') ?>">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
</div>
<!-- /menu footer buttons -->
</div>
</div>
