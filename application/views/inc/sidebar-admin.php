<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Administrador</h3>
        <ul class="nav side-menu">


            <?php


            $tipo = $this->session->userdata('tipo');

            ?>

            <li><a href="<?= base_url('admin') ?>"><i class="fa fa-home"></i> Inicio </a></li>

            <?php

            if ($matricula_aula_virtual>0) {
            ?>


                <li><a><i class="fa fa-graduation-cap"></i>Aula Virtual <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        <li>
                            <a target="_blank" href="<?=base_url('sesion/iniciarMoodle')?>">Iniciar sesión</a>
                        </li>
                        <li>
                            <a href="<?= base_url('admin/matriculas/aulavirtual/listado') ?>">Descargar listado</a>
                        </li>

                        <li>
                            <a href="<?= base_url('admin/matriculas/aulavirtual/cursos') ?>">Consultar Matrículas</a>
                        </li>

                    </ul>
                </li>




                <?php
                }

       ?>




            <li><a><i class="fa fa-pencil"></i>Matriculas <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">


                    <li>
                        <a href="<?= base_url('admin/inscripciones') ?>">Inscripciones web</a>
                    </li>


                    <li>
                        <a href="<?= base_url('admin/matriculas/primer-ingreso') ?>">Estudiantes nuevos</a>
                    </li>


                    <li>
                        <a href="<?= base_url('admin/matriculas') ?>">Estudiantes antiguos</a>
                    </li>


                    <li>
                        <a href="<?= base_url('admin/matriculas/listado') ?>">Consultar listado</a>
                    </li>
                    <li>
                        <a href="<?= base_url('admin/matriculas/consultar') ?>">Consultar por estudiante</a>
                    </li>


                    <li>
                        <a href="<?= base_url('admin/matriculas/asignatura/individual') ?>">Matrícular Asignatura Individual</a>
                    </li>

                    <li><a href="<?= base_url('admin/matriculas/asignatura/grupal') ?>">Matrícular Asignatura Grupal</a></li>



                    <?php
                    if ($tipo == "SA") {

                        ?>

                        <li><a href="<?= base_url('admin/matriculas/academicas/matricular') ?>">Matrículas Académicas</a></li>


                        <?php

                    }
                    ?>




                </ul>
            </li>


            <li><a href="<?= base_url('admin/estudiante') ?>"><i class="fa fa-user"></i> Estudiantes </a></li>


            <li><a><i class="fa fa-user-o"></i>Docentes <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">


                    <li><a href="<?= base_url('admin/docentes') ?>">Gestionar</a></li>
                    <li><a href="<?= base_url('admin/docentes/cargas-academicas') ?>">Cargas académica</a></li>
                    <li><a href="<?= base_url('admin/docentes/planillas') ?>">Planillas</a></li>


                    <?php
                    if ($tipo == "SA") {

                        ?>
                        <li><a href="<?= base_url('admin/docentes/evaluacion') ?>">Evaluación Docente</a></li>

                        <?php
                    }


                    ?>


                </ul>
            </li>


            <li><a><i class="fa fa-edit"></i>Notas <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">


                    <li><a href="<?= base_url('admin/notas/lv') ?>">Digitar lunes a Viernes</a></li>
                    <li><a href="<?= base_url('admin/notas/s') ?>">Digitar notas sábados</a></li>

                    <li><a href="<?= base_url('admin/notas/consultar/') ?>">Consultar</a></li>
                    <li><a href="<?= base_url('admin/notas/habilitar/') ?>">Habilitaciones</a></li>

                    <?php
                    if ($tipo == "SA") {

                        ?>
                        <li><a href="<?= base_url('admin/notas/editar/m') ?>">Editar matinal</a></li>
                        <li><a href="<?= base_url('admin/notas/editar/s') ?>">Editar sabatina</a></li>

                        <?php

                    }
                    ?>


                    <li><a href="<?= base_url('admin/notas/reporte/grupal/pdf') ?>">Reporte grupal PDF</a></li>
                    <li><a href="<?= base_url('admin/notas/reporte/grupal/excel') ?>">Reporte grupal EXCEL</a></li>
                    <li><a href="<?= base_url('admin/notas/reporte/individual/pdf') ?>">Reporte individual PDF</a></li>


                </ul>
            </li>




            <li><a><i class="fa fa-book"></i>Asignaturas <span class="fa fa-down"></span></a>
                <ul class="nav child_menu">


                    <li>
                        <a href="<?= base_url('admin/asignaturas') ?>">Gestionar</a>
                    </li>
                    <li>
                        <a href="<?= base_url('admin/asignaturas/semestrales') ?>">Semestrales</a>
                    </li>

                    </ul>

            </li>




            <li><a><i class="fa fa-users"></i>Grupos <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">

                    <?php foreach ($programas as $programa) : ?>

                        <li><a href="<?= base_url('admin/grupos/'.strtolower($programa['codigo']).'') ?>"><?=$programa['nombre']?></a></li>

                    <?php endforeach; ?>

                </ul>
            </li>



            <?php
            if ($tipo == "SA") {

                ?>

                <li><a href="<?= base_url('admin/periodos') ?>"><i class="fa fa-clock-o"></i> Periodos </a></li>
                <li><a href="<?= base_url('admin/configuracion') ?>"><i class="fa fa-gears"></i> Configuración </a></li>

                <?php
            }


            ?>


    </div>


</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">


    <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Cerrar sesión" href="<?= base_url('sesion/cerrar') ?>">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
</div>
<!-- /menu footer buttons -->
</div>
</div>
