<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 *  ======================================= 
 *  Author     : Team Tech Arise 
 *  License    : Protected 
 *  Email      : info@techarise.com 
 * 
 *  ======================================= 
 */
require_once APPPATH . "/third_party/fpdf/fpdf.php";

class Pdf extends FPDF {


    public $drawfooter;
    public $drawHeader;


    public function __construct($drawfooter=1,$drawHeader =1) {


        $this->drawfooter = $drawfooter;
        $this->drawHeader = $drawfooter;

        parent::__construct();
    }



    function Header()
    {


        if ($this->drawHeader==1) {




            $this->Image(base_url('assets/img/reporte_notas/header.jpg'), 140, 15, 960 / 18, 510 / 18, 'JPG', '');


        }
    }


    function Footer()
    {



        if ($this->drawfooter==1){


        $xFirmas = 130;
        $yFirmas = 225;



        /*
            $this->SetXY(25, 10);
            $this->SetFont('Arial', '', 10);
            $this->Cell(2, 14, utf8_decode("Fecha de generación:"));

            */

            $this->SetXY(25, 14);
            $this->SetFont('Arial', '', 10);
            $this->SetTextColor(155,155,155);
            $this->Cell(2, 14, utf8_decode(date('Y-m-d H:i:s')));



            $this->SetTextColor(0,0,0);
            $this->SetFont('Arial', 'b', 12);

        $this->SetXY(25, $yFirmas);
        $this->Cell(2, 14, utf8_decode("Anatael Garay Álvarez"));


        $this->SetXY($xFirmas, $yFirmas);
        $this->Cell(2, 14, utf8_decode("Tania Blanco Taboada"));

        $this->SetFont('Arial', '', 12);
        $this->SetXY(25, $yFirmas + 5);
        $this->Cell(2, 14, utf8_decode("Director"));
        $this->SetXY($xFirmas, $yFirmas + 5);
        $this->Cell(2, 14, utf8_decode("Coordinadora Académica"));



        $this->Image(base_url('assets/img/reporte_notas/pie_pagina.jpg'), 6, 248, 2376 / 12, 407 / 12, 'JPG', '');



        }

    }


}
