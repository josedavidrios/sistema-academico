<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model
{


    public $tipo;

    function __construct()
    {
        parent::__construct();


        $this->tipo = $this->session->userdata('tipo');
    }



    function  consultarMatriculaAulaVirtual($documento){


        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual->select("*")->from("mdl_user")->where("username",$documento);


        return  count($builder->get()->result_array()) ;

    }



    function filtrarMunicipios($nombre)
    {


        $this->db->select("m.codigo AS id,  CONCAT(m.nombre,',',d.nombre) as text, d.nombre AS dpto", false)
           ->from("municipios m")
           ->join("departamentos d", "d.codigo=m.codigo_departamento")
            ->like("m.nombre", $nombre);

        return $this->db->get()->result_array();

    }



    function filtrarEPS($nombre){


        $this->db->select("codigo AS id, nombre AS text")
            ->from("eps")
            ->like("nombre", $nombre);

        return $this->db->get()->result_array();


    }



    function cambiarClaveDeAcceso($documento, $claveActual, $datos)
    {


        $aux = -1;

        if (strcmp($claveActual, $this->obtenerClave($documento)) == 0) {

            $this->db->where('documento',$documento)
                     ->update("usuarios", $datos);


            return  $this->db->affected_rows();


        }

        return $aux;

    }


    function registrar($tabla, $datos)
    {


        $this->db->insert($tabla, $datos);


        return $this->db->affected_rows();


    }


    function generarClaveMoodle($clave)
    {


        $fasthash = true;

        $options = ($fasthash) ? array('cost' => 4) : array();

        $generatedhash = password_hash($clave, PASSWORD_DEFAULT, $options);


        return $generatedhash;

    }


    function  actualizarDatosMoodle($documento,$datos){


        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual->where("username",$documento)->update("mdl_user",$datos);




    }


    function cambiarClaveDeAccesoMoodle($documento, $clave)
    {

        $clave = $this->generarClaveMoodle($clave);

        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);

        $builder= $aulaVirtual->where("username",$documento)->set("password",$clave)->update("mdl_user");


    }












    function obtenerClave($documento)
    {


        $this->load->helper("encrypt");


        $this->db->select("clave")
            ->from("usuarios")
            ->where('documento',$documento);

        return decrypt($this->db->get()->result_array()[0]['clave']);

    }


    function editar($documento, $datos)
    {


        $this->db->where('documento', $documento)
            ->update("usuarios", $datos);


        return $this->db->affected_rows();


    }






    function consultar($documento = null)
    {


        $this->db->select("*")
            ->from("usuarios u");
        //   ->where("u.documento = u.clave");


        if (!is_null($documento)) {


            $this->db->where('documento', $documento);


        }


        return $this->db->get()->result_array();

    }


    function actualizarUsuario($documento, $datos)
    {


        $this->db->where('documento', $documento)
            ->update('usuarios', $datos);

        return $this->db->affected_rows();

    }






    function consultarPeriodoMatriculasAbiertas()
    {


        $this->db->select("*")
            ->from("periodos")
            ->where("matriculas_abiertas", 1);

        return $this->db->get()->result_array();

    }







}
