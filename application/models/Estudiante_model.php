<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiante_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();

        //  $this->semestreVigente = $this->consultarSemestreVigente();
    }



    function registrar($tabla,$datos){


        $this->db->insert($tabla,$datos);


        $this->db->affected_rows();

    }


    function actualizarEstadoEvaluacionDocente($evalucion,$datos){


        $this->db->where("codigo",$evalucion)
            ->update("evaluaciones_docentes_por_matriculas",$datos);

        return $this->db->affected_rows();

    }





    function actualizarEstadoEvaluacionDocentePorAsignatura($evalucion,$carga,$datos){


        $this->db->where("evaluacion_docente_por_matricula",$evalucion)
            ->where("carga_academica",$carga)
            ->update("evaluaciones_docentes_por_asignaturas",$datos);

        return $this->db->affected_rows();

    }



    function actualizar($documento, $datos)
    {


        $this->db->where('documento', $documento)
            ->update('usuarios', $datos);

        return $this->db->affected_rows();

    }




    function consultarEvaluacionAsignatura($evalucion,$carga){



        $this->db->select("codigo")
            ->from("evaluaciones_docentes_por_asignaturas")
            ->where("evaluacion_docente_por_matricula",$evalucion)
            ->where("carga_academica",$carga);



        return  $this->db->get()->result_array()[0]['codigo'];

    }




    function consultarEvaluacionesDocentesPendientes($documento){


        $this->db->select("COUNT(ed.codigo) AS cantidad")
            ->from("matriculas m")
            ->join("usuarios e","e.documento = m.estudiante","INNER")
            ->join("evaluaciones_docentes_por_matriculas ed","ed.matricula=m.codigo","INNER")
            ->join("evaluaciones_docentes evd","evd.codigo=ed.evaluacion_docente","INNER")

            ->join("grupos g","g.codigo = m.grupo","INNER")
            ->join("jornadas j","j.codigo = g.jornada","INNER")
            ->join("programas p","g.programa = p.codigo","INNER")
            ->where("evd.estado",1)
            ->where(" e.documento",$documento);




        return  $this->db->get()->result_array()[0]['cantidad'];


    }


    function consultarEvaluacionesDocentes($documento){


        $this->db->select("ed.codigo AS evaluacion, ee.periodo, m.codigo AS matricula, p.nombre AS programa, j.nombre AS jornada,g.semestre ,COUNT(ed.codigo) AS cant ")
             ->from("matriculas m")
             ->join("usuarios e","e.documento = m.estudiante","INNER")
             ->join("evaluaciones_docentes_por_matriculas ed","ed.matricula=m.codigo","INNER")
            ->join("evaluaciones_docentes_por_asignaturas eda","eda.evaluacion_docente_por_matricula=ed.codigo","INNER")
            ->join("evaluaciones_docentes ee","ee.codigo=ed.evaluacion_docente","INNER")


             ->join("grupos g","g.codigo = m.grupo","INNER")
             ->join("jornadas j","j.codigo = g.jornada","INNER")
             ->join("programas p","g.programa = p.codigo","INNER")
             ->where(" e.documento",$documento)
            ->where("eda.estado","0")
            ->where("ee.estado",1)

            ->group_by("ed.codigo");




        return  $this->db->get()->result_array();


    }




    function consultarProgramasMatriculados($documento, $periodo=null){


        $this->db->select('m.codigo AS matricula,p.nombre, g.grupo, p.codigo, pe.descripcion AS descripcion_periodo , g.semestre, j.nombre AS nombre_jornada, g.periodo, g.jornada, j.nombre as nombre_jornada, s.nombre AS nombre_semestre')
            ->from("matriculas m")
            ->join("usuarios e","e.documento = m.estudiante")
            ->join("grupos g","g.codigo = m.grupo")
            ->join("periodos pe","pe.codigo = g.periodo")
            ->join("programas p","g.programa = p.codigo")
            ->join("jornadas j","j.codigo = g.jornada")
            ->join("semestres s","s.numero = g.semestre")
            ->where("e.documento",$documento)
            ->where("m.activa",1)
            ->order_by("g.periodo","DESC");


        if(!is_null($periodo)){

            $this->db->where("pe.actual",1);

        }



        return  $this->db->get()->result_array();


    }



    function consultarPreguntasEvaluacionDocente($criterio=null){


        $this->db->select('p.codigo,p.nombre, c.nombre AS criterio')
            ->from("preguntas_evaluacion_docentes p")
            ->join("criterios_de_evaluacion c","c.codigo = p.criterio_de_evaluacion","INNER")
            ->where("c.activo",1);



        if (!is_null($criterio)){

            $this->db->where("c.codigo",$criterio);

        }





        return  $this->db->get()->result_array();


    }



    function registrarRespuestasEvaluacionDocente($respuestas){


        $this->db->insert_batch("respuestas_evaluaciones_docentes_por_asignaturas",$respuestas);
    }





    function consultarDocentesPorEvaluar($codigoEvaluacion){




        $this->db->select("ca.codigo, ed.codigo AS evaluacion ,CONCAT(u.apellidos,' ',u.nombres) AS docente, a.nombre AS asignatura")
            ->from("evaluaciones_docentes_por_asignaturas eda")
            ->join("evaluaciones_docentes_por_matriculas ed","ed.codigo = eda.evaluacion_docente_por_matricula","INNER")
            ->join("cargas_academicas ca","ca.codigo = eda.carga_academica","INNER")
            ->join("usuarios u","u.documento = ca.docente","INNER")
            ->join("asignaturas_semestrales asem","asem.codigo = ca.asignatura_semestral","INNER")
            ->join("asignaturas a","a.codigo = asem.asignatura","INNER")
            ->where("eda.estado",0)
            ->where("ca.activa",1)
            ->where("asem.activa",1)

        //    ->where("ca.jornada","LV")

            ->where("eda.evaluacion_docente_por_matricula",$codigoEvaluacion);


        return  $this->db->get()->result_array();

    }


    function consultarNotas($documento,$programa=null,$periodo=null){


        $this->db->select("a. nombre,n.nota1, n.nota2, n.nota3, n.nota_definitiva")
            ->from("notas2018 n")
            ->join("asignaturas_semestrales asem", "asem.codigo = n.asignatura_semestral","INNER")
            ->join("asignaturas a", "asem.asignatura = a.codigo","INNER")
            ->where(" n.estudiante",$documento)
            ->order_by("a.nombre");


        return  $this->db->get()->result_array();

    }

    function consultarMateriasMatriculadas($documento,$programa,$periodo, $perdidas=false){


        $this->db->select("ams.codigo AS codigo_asignatura_matriculada, a.nombre, FORMAT(n.nota1,1) AS nota1, FORMAT(n.nota2,1) AS nota2 , FORMAT(n.nota3,1) AS nota3 ,FORMAT(n.nota_definitiva, 1) AS nota_definitiva, n.observacion")
             ->from("asignaturas_matriculadas ams")
             ->join("asignaturas_semestrales asig_sem", "asig_sem.codigo = ams.asignatura_semestral","INNER")
             ->join("matriculas m", "m.codigo = ams.matricula","INNER")
            ->join("grupos g", "g.codigo =m.grupo","INNER")

            ->join("usuarios e", "e.documento = m.estudiante","INNER")
             ->join("asignaturas a","a.codigo=asig_sem.asignatura","INNER")

            ->join("notas n","n.asignatura_matriculada=ams.codigo","INNER")

            ->join("planes_de_estudios ps","ps.codigo=asig_sem.plan_de_estudio","INNER")

            ->where(" ps.programa",$programa)
            ->where(" g.periodo",$periodo)
            ->where(" e.documento",$documento)
            ->where("m.activa",1)
            ->where("asig_sem.activa",1)
        ->order_by("a.nombre");



        if ($perdidas){


            $this->db->where("n.nota_definitiva<3");

        }





        return $this->db->get()->result_array();

    }


    function consultarDocentesAsignados($documento,$programa,$periodo){


        $this->db->select("ca.codigo,a.nombre AS asignatura, doce.documento, CONCAT(doce.apellidos,' ',doce.nombres) AS nombres")
            ->from("asignaturas_matriculadas ams")
            ->join("asignaturas_semestrales asig_sem", "asig_sem.codigo = ams.asignatura_semestral","INNER")
            ->join("asignaturas a","a.codigo=asig_sem.asignatura","INNER")
            ->join("matriculas m", "m.codigo = ams.matricula","INNER")
            ->join("planes_de_estudios ps","ps.codigo=asig_sem.plan_de_estudio","INNER")
            ->join("cargas_academicas ca","ca.asignatura_semestral=asig_sem.codigo","INNER")
            ->join("usuarios doce","doce.documento = ca.docente","INNER")
            ->where(" ps.programa",$programa)
            ->where("m.estudiante",$documento)
            ->where("ca.activa",1)
            ->group_by("a.codigo");


        return   $this->db->get()->result_array();

    }



    function consultar($documento)
    {

        $this->db->select("u.*, m.nombre AS municipio_residencia, ms.nombre AS municipio_nacimiento, u.lugar_expedicion_documento, med.nombre AS municipio_expedicion_documento, e.nombre AS nombre_eps")
            ->from("usuarios u")
            ->where("u.tipo","E")
            ->where("u.documento", $documento)
            ->join("municipios m ","m.codigo = u.lugar_residencia","INNER")
            ->join("municipios ms ","ms.codigo = u.lugar_nacimiento","INNER")
            ->join("municipios med ","med.codigo = u.lugar_expedicion_documento","INNER")
            ->join("estados_civiles ec ","ec.codigo = u.estado_civil","INNER")
            ->join("eps e ","e.codigo = u.eps","INNER");


        return $this->db->get()->result_array();


    }





}
