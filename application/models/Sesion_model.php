<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sesion_model extends CI_Model
{


    function iniciar($usuario, $clave)
    {


        $this->db->select("nombres, apellidos, tipo, documento")
            ->from("usuarios")
            ->where("clave", $clave)
            ->where("activo",1);

        if (strpos($usuario, '@')) {

            $this->db->where("correo_institucional", $usuario);

        } else {

            $this->db->where("documento", $usuario);

        }

        return $this->db->get()->result_array();
    }

    function consultarDatosUsuario($correo = null)
    {


        $this->db->select("*")
            ->from("usuarios");

        if (!is_null($correo)) {

            $this->db->where("correo_institucional", $correo);

        }

        return $this->db->get()->result_array();


    }


    function consultarPeriodoActual()
    {

        $this->db->select("codigo AS periodo")
            ->from("periodos")
            ->where("actual", 1);

        return $result = $this->db->get()->result_array()[0]['periodo'];
    }


    function registrarLog($datos){


        $this->db->insert("log_sesiones",$datos);

        return $this->db->insert_id();


    }


    function cerrarLog($codigo,$fechaFin){


        $this->db->where("codigo",$codigo)
            ->set("fecha_fin",$fechaFin)
            ->update("log_sesiones");

        return $this->db->affected_rows();

    }


}
