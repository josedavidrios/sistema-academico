<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{


    /*

    function consultarEstudiante($documento)
    {

        $this->db->select("u.*, mr.nombre AS municipio_residencia,  mn.nombre AS municipio_nacimiento")
            ->from("usuarios u")
            ->join("municipios mr", "mr.codigo=u.lugar_residencia", "INNER")
            ->join("municipios mn", "mn.codigo=u.lugar_nacimiento", "INNER")
            ->where_in("u.tipo", ["E", "ASP"])
            ->where("u.documento", $documento);

        return $this->db->get()->result_array();


    }

    */


    function consultarEstudiante($documento)
    {

        $this->db->select("u.*,
                                 m.nombre AS municipio_residencia,
                                 ms.nombre AS municipio_nacimiento,
                                 u.lugar_expedicion_documento,
                                 med.nombre AS municipio_expedicion_documento,
                                 e.nombre AS nombre_eps,
                                 e.codigo AS eps")


            ->from("usuarios u")
            ->where("u.tipo", "E")
            ->where("u.documento", $documento)
            ->join("municipios m ", "m.codigo = u.lugar_residencia", "INNER")
            ->join("municipios ms ", "ms.codigo = u.lugar_nacimiento", "INNER")
            ->join("municipios med ", "med.codigo = u.lugar_expedicion_documento", "INNER")
            ->join("estados_civiles ec ", "ec.codigo = u.estado_civil", "INNER")
            ->join("eps e ", "e.codigo = u.eps", "INNER");


        return $this->db->get()->result_array();


    }


    function  actualizarDatosAulaVirtual($documento,$datos){


        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual->where("username",$documento)->update("mdl_user",$datos);




    }

    function  registearDatosAulaVirtual($datos){


        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual->insert("mdl_user",$datos);




    }

    function consultarPorCodigo($tabla, $codigo)
    {

        $this->db->select("*")
            ->from($tabla)
            ->where("codigo", $codigo);

        return $this->db->get()->result_array();
    }


    function consultarGrados()
    {


        $this->db->select("numero,nombre");
        $this->db->from("grados");

        return $this->db->get()->result_array();


    }

    function consultarNivelesEducacion()
    {


        $this->db->select("codigo,nombre");
        $this->db->from("niveles_educacion");

        return $this->db->get()->result_array();

    }


    function filtrarEstudiante($nombres)
    {

        $this->db->select("u.apellidos, u.nombres, u.documento, u.correo, u.celular");
        $this->db->like('u.apellidos', $nombres);
        $this->db->from('usuarios u');
        $this->db->where("u.tipo", "E");


        return $this->db->get()->result_array();
    }

    function filtrarInstitucion($nombre)
    {

        $this->db->select("i.codigo,  i.nombre, m.nombre AS municipio")
            ->like('i.nombre', $nombre)
            ->from('instituciones i')
            ->join("municipios m", "m.codigo=i.municipio");

        return $this->db->get()->result_array();

    }


    function consultarTodosLosEstudiantes()
    {


        $result = $this->db->get("estudiantes");

        return $result->result_array();


    }


    function filtrarMunicipios($nombre)
    {


        $this->db->select("m.codigo AS id,  CONCAT(m.nombre,',',d.nombre) as text, d.nombre AS dpto", false);
        $this->db->from("municipios m");
        $this->db->join("departamentos d", "d.codigo=m.codigo_departamento");

        $this->db->like("m.nombre", $nombre);

        return $this->db->get()->result_array();

    }


    function actualizar($tabla, $codigo, $datos)
    {


        $this->db->where('codigo', $codigo)
            ->update($tabla, $datos);

        return $this->db->affected_rows();

    }


    function actualizarUsuario($documento, $datos)
    {


        $this->db->where('documento', $documento)
            ->update('usuarios', $datos);

        return $this->db->affected_rows();

    }

    function consultarMunicipios($nombre_departamento)
    {


        $this->db->select("m.codigo AS id, m.nombre AS text, d.nombre AS dpto", false);
        $this->db->from("municipios m");
        $this->db->join("departamentos d", "d.codigo=m.codigo_departamento")
            ->like("m.nombre", $nombre_departamento);

        return $this->db->get()->result_array();

    }


    function validarCorreoInstitucional($correo){


        $this->db->select("*")->from("usuarios")
            ->where("correo_institucional",$correo);

        return $this->db->get()->result_array();
    }

    function registrarDocente($datos)
    {


        $this->db->insert("usuarios", $datos);


        return $this->db->affected_rows();
    }


    function listarDocentes()
    {
        $this->db->select("documento, CONCAT(nombres,' ',apellidos) as nombres")
            ->where("tipo","D")

            ->where("activo",1)

            ->from('usuarios');

        return  $this->db->get()->result_array();


    }

    function consultarDocente($documento)
    {

        $this->db->select("e.*");
        $this->db->from("usuarios e");
        $this->db->where("e.documento", $documento);
        return $this->db->get()->result_array();

    }


    function consultarGruposProximoPeriodo($periodo)
    {


        $this->db->select("DISTINCT '$periodo' AS periodo, ase.semestre, ase.jornada, pe.programa, 'A' AS grupo, CONCAT(pe.programa, '$periodo' ,ase.jornada,ase.semestre) AS abreviatura ");

        $this->db->from('asignaturas_semestrales ase');
        $this->db->join('planes_de_estudios pe', 'pe.codigo = ase.plan_de_estudio');


        return $this->db->get()->result_array();

    }


    function consultarGrupo($codigo)
    {


        $this->db->select("g.codigo,p.nombre AS programa, p.codigo AS codigo_programa, j.codigo as codigo_jornada , j.nombre AS jornada, g.semestre, g.grupo, g.periodo, s.nombre as nombre_semestre")
            ->from("grupos g")
            ->join('programas p', 'p.codigo = g.programa')
            ->join('jornadas j', 'j.codigo = g.jornada')
            ->join('semestres s', 's.numero = g.semestre')
            ->where("g.codigo", $codigo)
            ->where("g.activo", 1);

        return $this->db->get()->result_array();

    }


    function consultarGrupos($periodo, $semestre = null, $programa = null)
    {


        $this->db->select("g.codigo,p.nombre AS programa, p.codigo AS codigo_programa, j.nombre AS jornada, j.codigo AS codigo_jornada ,  g.semestre, g.grupo, g.periodo ")
            ->from('grupos g')
            ->join('programas p', 'p.codigo = g.programa')
            ->join('jornadas j', 'j.codigo = g.jornada')
            ->join('periodos pe', 'pe.codigo = g.periodo')
            ->where("g.periodo", $periodo)
            ->where("g.activo", 1)
            ->order_by("programa,jornada, semestre,grupo", "ASC");


        if (!is_null($semestre)) {

            $this->db->where("g.semestre", $semestre);
        }


        if (!is_null($programa)) {

            $this->db->where("p.codigo", $programa);
        }


        return $this->db->get()->result_array();


    }


    function consultarNotas($codigo)
    {


        $this->db->select("codigo, nota1, nota2,nota3")
            ->from("notas")
            ->where("codigo", $codigo);

        return $this->db->get()->result_array();

    }


    function consultarAsignaturas($codigoPrograma = null)
    {


        $this->db->select("a.codigo, a.nombre, p.nombre AS programa, as.codigo AS asignatura_semestral, as.jornada, as.semestre")
            ->from("planes_de_estudios ps")
            ->join("asignaturas_semestrales as", "as.plan_de_estudio=ps.codigo")
            ->join("programas p", "ps.programa=p.codigo")
            ->join("asignaturas a", "as.asignatura=a.codigo")
            ->where("as.activa",1);


        if (!is_null($codigoPrograma)) {

            $this->db->where("ps.programa", $codigoPrograma);
        }


        return $this->db->get()->result_array();
    }

    function consultarMatricula2($numero)
    {


        $this->db->select("m.*, m.fecha_registro as fecha,
                                
                                  p.nombre AS nombre_programa, g.semestre as semestre, j.nombre as jornada, g.numero as grupo,
                                
e.*, 
        
        i.nombre as nombre_institucion,
        consultar_municipio(e.lugar_residencia) AS nombre_lugar_residencia, 
        consultar_municipio(e.lugar_nacimiento) AS nombre_lugar_nacimiento,
        consultar_municipio(e.lugar_expedicion_documento) AS nombre_lugar_expedicion_documento
                                                          
        
        ")
            ->from("matriculas m")
            ->join("estudiantes e", "e.documento = m.estudiante", "INNER")
            ->join("grupos g", "g.codigo = m.grupo", "INNER")
            ->join("programas p", "p.codigo = g.programa", "INNER")
            ->join("jornadas j", "j.codigo = g.jornada", "INNER")
            ->join("instituciones i", "i.codigo=e.institucion")
            ->where("m.codigo", $numero);

        return $this->db->get()->result_array();
    }

    function consultarMatricula($codigo)
    {


        $this->db->select("m.codigo, m.estudiante, g.periodo, m.auxilio_academico")
            ->from("matriculas m")
            ->join("grupos g", "g.codigo = m.grupo")
            ->where("m.codigo", $codigo);

        return $this->db->get()->result_array();
    }


    function cancelarMatricula($codigo)
    {

        $this->db->where("codigo", $codigo)
            ->set('activa', 0)
            ->set('ultima_fecha_modificacion', fecha_y_hora_actual())
            ->update("matriculas");


        return $this->db->affected_rows();


    }

    function cambiarDocenteCargaAcademica($codigo,$docente)
    {

        $this->db->where("codigo", $codigo)
            ->set('docente', $docente)
            ->set('ultima_fecha_modificacion', fecha_y_hora_actual())
            ->update("cargas_academicas");


        return $this->db->affected_rows();


    }



    function consultarEstadoMatricula($codigo)
    {


        $this->db->select("activa")
            ->from("matriculas")
            ->where("codigo", $codigo);

        return $this->db->get()->result_array();

    }


    function inscribirEstudiante($datos)
    {


        $this->db->insert("inscripciones", $datos);


        return $this->db->affected_rows();
    }


    function calcularNotaDefinitiva($asignaturaMatriculada)
    {


        $this->db->query("CALL calcular_nota_definitiva($asignaturaMatriculada);");

    }


    function recalcularNotaDefinitiva($codigo)
    {


        $this->db->query("CALL recalcular_nota_definitiva($codigo);");

    }


    function consultarVisitasAlsistemaPorPlataforma()
    {


        //  $this->db->select("CONCAT(FORMAT((100*COUNT(sistema_operativo)/contar_sesiones()),2),'%') AS cant, sistema_operativo")


        $cant = $this->db->count_all_results('log_sesiones');


        $this->db->select("FORMAT((100*COUNT(plataforma)/" . $cant . "),1) AS cant, plataforma")
            ->from("log_sesiones")
            ->group_by("plataforma");

        return $this->db->get()->result_array();

    }


    function consultatVisitasAlSistema()
    {


        $result = $this->db->query("SELECT  MONTH(fecha_inicio) AS mes ,COUNT(*) AS cant  
                                        FROM log_sesiones
                                         WHERE tipo_usuario = 'E'
                                          GROUP BY YEAR(fecha_inicio),MONTH(fecha_inicio)
                                        ORDER BY fecha_inicio DESC 
                                      
                                        LIMIT 12
                                        
");

        return $result->result_array();
    }


    function consultarCantidadDeEvaluacionesDocentes()
    {


        $this->db->select("COUNT(*) AS cant, ed.periodo")
            ->from("evaluaciones_docentes ed")
            ->join("evaluaciones_docentes_por_matriculas edm", "edm.evaluacion_docente = ed.codigo", "INNER")
            ->where("edm.estado = 1")
            ->group_by("ed.codigo")
            ->order_by("ed.periodo", "DESC")
            ->limit(3);


        return $this->db->get()->result_array();

    }


    function consultarAsignaturasMatriculadas($documento, $jornada = null, $perdida = null, $periodo = null)
    {


        $this->db->select("n.codigo, n.nota1,n.nota2,n.nota3, FORMAT(n.nota_definitiva, 1) AS nota_definitiva, am.codigo AS codigo_asignatura, a.nombre AS asignatura, ase.jornada")
            ->from("matriculas m")
            ->join("asignaturas_matriculadas am", "am.matricula = m.codigo", "INNER")
            ->join("notas n", "n.asignatura_matriculada = am.codigo", "INNER")
            ->join("asignaturas_semestrales ase", "ase.codigo = am.asignatura_semestral", "INNER")
            ->join("asignaturas a", "a.codigo = ase.asignatura", "INNER")
            ->join("grupos g", "g.codigo = m.grupo", "INNER")
            ->where("m.estudiante", $documento)
            ->where("m.activa", 1)
            ->order_by("a.nombre");


        if (!is_null($periodo)) {

            $this->db->where("g.periodo", $periodo);
        }

        if (!is_null($perdida)) {

            $this->db->where("n.nota_definitiva<3");
        }


        if (!is_null($jornada)) {

            $this->db->where("g.jornada", $jornada);
        }


        return $this->db->get()->result_array();

    }


    function cambiarEstadoEvaluacionPorCorte($carga, $corte)
    {


        $this->db->set("evaluado_corte" . $corte, 1)
            ->where("codigo", $carga)
            ->update("cargas_academicas");


        return $this->db->affected_rows();

    }


    function cambiarEstadoEvaluacionCargaAcademica($carga)
    {


        $this->db->set("evaluacion_docente", 1)
            ->where("codigo", $carga)
            ->update("cargas_academicas");


        return $this->db->affected_rows();

    }


    function cambiarEstadoEvaluacionDefinitiva($carga)
    {


        $this->db->set("evaluado_modulo", 1)
            ->where("codigo", $carga)
            ->update("cargas_academicas");


        return $this->db->affected_rows();

    }


    function consultarGruposPorAsignatura($programa = null, $asignatura = null, $corte = null, $jornada = null, $periodo = null,$docente=null)
    {


        //  ->where("ps.programa",$programa);


        $this->db->select("ased.codigo,  g.codigo AS codigo_grupo, ps.codigo AS nombre_programa, a.nombre, ase.semestre, j.nombre AS jornada, ased.grupo AS grupo, CONCAT(d.apellidos,' ',d.nombres) as docente")
            ->from("asignaturas_semestrales ase")
            ->join('cargas_academicas ased', 'ased.asignatura_semestral =  ase.codigo', "inner")
            ->join('asignaturas a', 'a.codigo = ase.asignatura', "inner")
            ->join("planes_de_estudios ps", "ps.codigo = ase.plan_de_estudio", "inner")
            ->join("jornadas j", "j.codigo = ased.jornada", "inner")
            ->join("usuarios d", "d.documento = ased.docente", "inner")
            ->join('grupos g', 'g.grupo=ased.grupo', "inner")
            ->where("ps.programa=g.programa")
            ->where("g.jornada=ased.jornada")
            ->where("g.grupo=ased.grupo")
            ->where("g.periodo=ased.periodo")
            ->where("g.semestre=ase.semestre")
             ->where("ase.activa", 1)


            ->where_in("ased.jornada", $jornada)
            ->order_by("a.nombre");


        //    ->group_by("g.codigos");


        if (!is_null($asignatura)) {


            $this->db->where("ase.asignatura", $asignatura);

        }

        if (!is_null($docente)) {


            $this->db->where("ased.docente", $docente);

        }


        if (!is_null($corte)) {


            $this->db->where("evaluado_corte" . $corte, 0);

        }

        if (!is_null($asignatura)) {


            $this->db->where("ase.asignatura", $asignatura);

        }


        if (!is_null($periodo)) {


            $this->db->where("g.periodo", $periodo);

        }


        if (!is_null($programa)) {


            $this->db->where("ps.programa", $programa);

        }


        return $this->db->get()->result_array();
    }


    function consultarEstudiantesPorGrupo($grupo, $estado=1 )
    {


        $this->db->select("m.codigo, u.documento,u.correo,u.celular, CONCAT(u.apellidos,' ', u.nombres) AS nombres")
            ->from("matriculas m")
            ->join("usuarios u", "u.documento = m.estudiante", "INNER")
            ->where("m.grupo", $grupo)

            ->order_by("nombres");

        if ($estado!=="todos"){

            $this->db->where("m.activa", $estado);


        }

        return $this->db->get()->result_array();

    }


    function consultarConfiguracion()
    {


        return $this->db->get("configuraciones")->result_array()[0];

    }


    function consultarAsiganaturasPorPrograma($programa, $corte, $jornada, $periodo, $docente=null)
    {


        $this->db->select("ps.programa, a.codigo, a.nombre,COUNT(ased.codigo) AS cant")
            ->from("asignaturas_semestrales ase")
            ->join('cargas_academicas ased', 'ased.asignatura_semestral =  ase.codigo', "inner")
            ->join('asignaturas a', 'a.codigo = ase.asignatura', "inner")
            ->join("planes_de_estudios ps", "ps.codigo = ase.plan_de_estudio", "inner")
            ->group_by("a.codigo")
            ->order_by("a.nombre")
            ->where("ps.programa", $programa)
            ->where("ase.activa", 1)
            ->where("ased.periodo", $periodo)
            ->where("ased.activa", 1)
            ->where_in("ased.jornada", $jornada);


        if (!is_null($docente)) {

            $this->db->where("ased.docente", $docente);


        }


        if ($jornada == "S") {

            $this->db->where("evaluado_modulo", 0);


        } else {

            $this->db->where("evaluado_corte" . $corte, 0);


        }


        return $this->db->get()->result_array();
    }


    function consultarMatriculasPorRealizarEvaluacionDocente($periodo)
    {


        $this->db->select("m.estudiante, m.grupo, m.codigo, g.jornada,g.semestre, g.programa")
            ->from("matriculas m")
            ->join("grupos g", "g.codigo = m.grupo", "INNER")
            ->where("g.periodo", $periodo)
            ->where("m.codigo NOT IN ( SELECT e.matricula FROM evaluaciones_docentes_por_matriculas e )");


        return $this->db->get()->result_array();

    }


    function consultarCatalogoAsiganaturasSemestrales($jornada = null)
    {


        $this->db->select("ase.codigo, j.nombre AS jornada, p.nombre AS programa,  a.nombre  AS asignatura, ase.semestre")
            ->from("asignaturas_semestrales ase")
            ->join("planes_de_estudios ps", "ps.codigo = ase.plan_de_estudio", "INNER")
            ->join("programas p", "p.codigo = ps.programa", "INNER")
            ->join("asignaturas a", "a.codigo = ase.asignatura", "INNER")
            ->join("jornadas j", "j.codigo = ase.jornada", "INNER")
            ->where("ase.activa", 1)
            ->order_by("ase.jornada, programa, ase.semestre, asignatura ");


        if (!is_null($jornada)) {

            $this->db->where("j.codigo", $jornada);

        }


        return $this->db->get()->result_array();
    }


    function consultarAsignaturasSemestrales($programa, $semestre, $jornada)
    {


        $this->db->select("ase.codigo, j.nombre as jornada, ase.semestre, ase.activa, a.nombre as asignatura ")
            ->from("asignaturas_semestrales ase")
            ->join("planes_de_estudios ps", "ps.codigo = ase.plan_de_estudio", "inner")
            ->join("jornadas j", "j.codigo = ase.jornada", "inner")
            ->join("asignaturas a", "a.codigo = ase.asignatura", "inner")
            ->where(" ase.semestre", $semestre)
            ->where("ps.programa", $programa)
            ->where("ase.jornada", $jornada)
            ->where("ase.activa", 1)
            ->where("ps.activo ", 1)
            ->order_by("a.nombre");


        return $this->db->get()->result_array();
    }


    function consultarDocentesAsignados($codigoMatricula, $evaluacionDocente=null,$cargaAcademica=null)
    {


        $this->db->select("ca.codigo, a.nombre AS asignatura, CONCAT(d.nombres,' ',d.apellidos) AS nombres ")
            ->from("matriculas m")
            ->join("asignaturas_matriculadas am", "am.matricula = m.codigo", "INNER")
            ->join("asignaturas_semestrales ase", "ase.codigo = am.asignatura_semestral", "INNER")
            ->join("asignaturas a", "a.codigo = ase.asignatura", "INNER")
            ->join("cargas_academicas ca", "ca.asignatura_semestral = ase.codigo", "INNER")
            ->join("usuarios d", "d.documento = ca.docente", "INNER")
            ->join("grupos g", "g.codigo = m.grupo", "INNER")
            ->where("m.codigo", $codigoMatricula)
            ->where("ca.grupo = g.grupo")
            ->where("ca.jornada = ase.jornada")
            ->where("ca.periodo  = g.periodo");

        if (!is_null($cargaAcademica)){

            $this->db->where("ca.codigo",$cargaAcademica);


        }

        if (!is_null($evaluacionDocente)){

            $this->db->where("ca.evaluacion_docente",0);


        }

        return $this->db->get()->result_array();

    }


    function registrar($tabla, $datos)
    {


        $this->db->insert($tabla, $datos);


        return $this->db->affected_rows();


    }


    function consultarAsignaturaSemestral($programa, $semestre, $jornada,$asignatura){

        $this->db->select("a.codigo, ps.programa")
            ->from("asignaturas_semestrales a")
            ->join("planes_de_estudios ps","ps.codigo=a.plan_de_estudio")
            ->where("a.asignatura", $asignatura)
            ->where("a.semestre", $semestre)
            ->where("a.jornada", $jornada)
            ->where("ps.programa", $programa);


        return $this->db->get()->result_array();


    }


    public function consultarEvaluacionDocentePeriodoActual()
    {

        $this->db->select("*")
            ->from("evaluaciones_docentes")
            ->where("estado", 1);

        return $this->db->get()->result_array();
    }


    function consultarEvaluacionesDocentes($codigoMatricula = null)
    {


        $this->db->select("*")
            ->from("evaluaciones_docentes_por_matriculas");


        if (!is_null($codigoMatricula)) {


            $this->db->where("matricula", $codigoMatricula);

        }


        return $this->db->get()->result_array();
    }


    function registrarNota($asignaura, $datos)
    {


        $this->db->where("asignatura_matriculada", $asignaura)
            ->update("notas", $datos);
        return $this->db->affected_rows();


    }


    function consultarDetallesDeGrupo($codigo)
    {


        $this->db->select("p.nombre AS programa , p.codigo AS codigo_programa,   j.nombre AS jornada, g.semestre AS semestre, g.grupo")
            ->from("grupos g")
            ->join('programas p', 'p.codigo = g.programa')
            ->join('jornadas j', 'j.codigo = g.jornada')
            ->where("g.codigo", $codigo);

        return $this->db->get()->result_array();

    }

    function consultarEstudiantesMatriculadosPorAsignatura($carga, $corte = null, $grupo=null)
    {


        $this->db->select("CONCAT(e.apellidos,' ', e.nombres) AS nombre, e.documento, am.codigo AS codigo_matricula, m.codigo AS matricula, n.nota_definitiva As nota ")
            ->from("cargas_academicas ca")
            ->join("asignaturas_semestrales ase", "ase.codigo = ca.asignatura_semestral", "INNER")
            ->join("asignaturas_matriculadas am", "am.asignatura_semestral = ase.codigo", "INNER")
            ->join("notas n", "n.asignatura_matriculada =am.codigo", "INNER")

            ->join("matriculas m", "m.codigo = am.matricula", "INNER")
            ->join("usuarios e", "e.documento = m.estudiante", "INNER")
            ->where("ca.codigo", $carga)
            ->where("m.activa", 1)
            ->order_by("nombre");


        if (!is_null($grupo)) {


            $this->db->where("m.grupo", $grupo);

        }

        if (!is_null($corte)) {
            $this->db->where("evaluado_corte" . $corte, 0);

        }


        return $this->db->get()->result_array();

    }


    function consultarListadoDeEstudiantesMatriculadosPorAsignatura($carga, $corteActual = null, $grupo=null)
    {


        $this->db->select("ams.codigo as codigo_matricula ,e.documento, CONCAT(e.apellidos,' ', e.nombres) as nombre")
            ->from("asignaturas_matriculadas ams")
            ->where("e.tipo", "e")
            ->join("asignaturas_semestrales asig_sem", "asig_sem.codigo = ams.asignatura_semestral")
            ->join("matriculas m", "m.codigo = ams.matricula")
            ->join("grupos g", "g.codigo = m.grupo")
            ->join("usuarios e", "e.documento = m.estudiante")
            ->join("cargas_academicas ca", "ca.asignatura_semestral = asig_sem.codigo")
            ->where("g.grupo = ca.grupo")
            ->where("g.jornada", "S")
            ->where("m.grupo", $grupo)
            ->where("g.jornada", $grupo)
            ->where("m.activa", 1)
            ->where("ca.codigo", $carga)
            ->order_by("nombre");


        if (!is_null($corteActual)) {
            $this->db->where("evaluado_corte" . $corteActual, 0);

        }


        return $this->db->get()->result_array();
    }


    function consultarEstadosCiviles()
    {


        return $this->db->get("estados_civiles")->result_array();
    }

    function consultarTiposDeDocumentos($codigo = null)
    {


        $this->db->select("*")
            ->from("tipos_documentos");

        if (!is_null($codigo)) {

            $this->db->where("codigo", $codigo);


        }
        return $this->db->get()->result_array();
    }


    function consultarListadoEvaluacionesDocente()
    {


        return $this->db->get('evaluaciones_docentes')->result_array();

    }


    function consultarAsignaturaPorCargaAcademica($codigoCarga)
    {


        /*


        SELECT a.nombre FROM cargas_academicas ca
INNER JOIN asignaturas_semestrales ase ON ase.codigo = ca.asignatura_semestral
INNER JOIN asignaturas a ON a.codigo = ase.asignatura
WHERE ca.codigo = 440


        */


        $this->db->select("a.nombre, ca.grupo, j.nombre as jornada")
            ->from("cargas_academicas ca")
            ->join("asignaturas_semestrales ase", "ase.codigo = ca.asignatura_semestral", "INNER")
            ->join("asignaturas a", "a.codigo = ase.asignatura", "INNER")
            ->join("jornadas j", "ase.jornada = j.codigo", "INNER")
            ->where("ca.codigo", $codigoCarga);


        return $this->db->get()->result_array();

    }


    function desactivarPeriodo($tipo)
    {

        $this->db->set($tipo, 0)
            ->update("periodos");

        return $this->db->affected_rows();
    }

    function activarEvaluacionDocente($codigo, $estado)
    {


        $this->db->set("estado", $estado)
            ->where("codigo", $codigo)
            ->update("evaluaciones_docentes");

        return $this->db->affected_rows();


    }


    function consultarPeriodoSinEvaluar()
    {


        $result = $this->db->query("
        SELECT * FROM periodos WHERE codigo NOT IN(SELECT periodo FROM evaluaciones_docentes)
");

        return $result->result_array();


    }


    function activarPeriodo($codigo, $tipo)
    {


        $this->db->set($tipo, 1)
            ->where("codigo", $codigo)
            ->update("periodos");

        return $this->db->affected_rows();


    }

    function consultarMatriculasEstudiane($estudiante=null,$periodo = null)
    {


        $this->db->select("m.codigo, m.grupo")
            ->from("matriculas m")
            ->join("grupos g", "g.codigo = m.grupo", "INNER");


        if (!is_null($periodo)) {

            $this->db->where("g.periodo", $periodo);
        }

        if (!is_null($estudiante)) {

                    $this->db->where("m.estudiante", $estudiante);
        }

        return $this->db->get()->result_array();

    }

    function consultarMatriculasPorPeriodo($periodo = null)
    {


        $this->db->select("periodo,COUNT(*) AS cant ")
            ->from("matriculas m")
            ->join("grupos g", "g.codigo = m.grupo", "INNER")
            ->group_by("g.periodo")
            ->order_by("g.codigo", "DESC")
            ->where("m.activa",1)
            ->limit(3);

        if (!is_null($periodo)) {

            $this->db->where("g.periodo", $periodo);
        }

        return $this->db->get()->result_array();

    }


    function consultarGruposPorPrograma($programa, $semestre = null, $jornada = null, $periodo = null)
    {


        $this->db->select("g.codigo, g.jornada, g.grupo, CONCAT(g.semestre,' SEMESTRE ', j.nombre,' #', g.grupo) as nombre")
            ->from("grupos g")
            ->join("programas p", "p.codigo = g.programa", "inner")
            ->join("jornadas j", "g.jornada = j.codigo", "inner")
            ->where("g.programa", $programa)
            ->group_by("g.codigo");


        if (isset($semestre) && !empty($semestre)) {

            $this->db->where("semestre", $semestre);

        }

        if (isset($jornada) && !empty($jornada)) {

            $this->db->where("g.jornada", $jornada);


        }

        if (isset($periodo) && !empty($periodo)) {

            $this->db->where("g.periodo", $periodo);

        }


        return $this->db->get()->result_array();

    }

    function consultarProgramasMatriculados($documento, $periodo = null)
    {


        $this->db->select('m.codigo AS codigo_matricula, m.activa, p.nombre, p.codigo, g.semestre, g.periodo, g.jornada,g.grupo, j.nombre as nombre_jornada, m.fecha_registro AS fecha ')
            ->from("matriculas m")
            ->join("usuarios e", "e.documento = m.estudiante")
            ->join("grupos g", "g.codigo = m.grupo")
            ->join("periodos pe", "pe.codigo = g.periodo")
            ->join("programas p", "g.programa = p.codigo")
            ->join("jornadas j", "j.codigo = g.jornada")
            ->where("e.documento", $documento)
            ->order_by("g.periodo", "DESC");


        if (!is_null($periodo)) {

            $this->db->where("m.periodo", $periodo);

        }


        return $this->db->get()->result_array();


    }




    function registrarEstudiante($datos)
    {


        $this->db->insert("estudiantes", $datos);


        return $this->db->affected_rows();
    }


    function consultarCargasAcademicasPorAsignaturaSemestral($asignatura,$periodo)
    {


        $this->db->select("*")
            ->from("cargas_academicas ca")
            ->where("ca.asignatura_semestral", $asignatura)
            ->where("ca.periodo", $periodo);


        return $this->db->get()->result_array();


    }

    function consultarCargasAcademicasOtrosDocentes($asignatura, $periodo, $jornada, $grupo)
    {


        $this->db->select("*")
            ->from("cargas_academicas ca")
            ->where("ca.asignatura_semestral", $asignatura)
            ->where("ca.jornada", $jornada)
            ->where("ca.periodo", $periodo)
            ->where("ca.grupo", $grupo);


        return $this->db->get()->result_array();


    }

    function consultarCargasAcademicasDuplicada($asignatura, $periodo, $jornada, $grupo, $docente)
    {


        $this->db->select("*")
            ->from("cargas_academicas ca")
            ->where("ca.asignatura_semestral", $asignatura)
            ->where("ca.jornada", $jornada)
            ->where("ca.periodo", $periodo)
            ->where("ca.grupo", $grupo)
            ->where("ca.docente", $docente)
            ->where("ca.activa",1);


        return $this->db->get()->result_array();


    }


    function consultarCargasAcademicas($codigo)
    {

        $this->db->select(" p.nombre AS programa,a.nombre AS asignatura, ase.semestre ,ca.codigo,  j.nombre AS jornada, CONCAT(d.nombres,' ',d.apellidos) AS docente ")
            ->from("cargas_academicas ca ")
            ->join("asignaturas_semestrales ase", "ase.codigo = ca.asignatura_semestral", "INNER")
            ->join("jornadas j", "j.codigo = ca.jornada", "INNER")
            ->join("planes_de_estudios ps", "ps.codigo = ase.plan_de_estudio", "INNER")
            ->join("programas p", "p.codigo = ps.programa", "INNER")
            ->join("asignaturas a", "a.codigo = ase.asignatura", "INNER")
            ->join("usuarios d", "d.documento = ca.docente", "INNER")
            ->where("ca.activa",1);


        if (!is_null($codigo)) {

            $this->db->where("ca.codigo", $codigo);

        }

        return $this->db->get()->result_array();

    }

    function consultarCargaAcademicaPorDocente($docente, $periodo = null, $jornada = null)
    {


        $this->db->select("ca.codigo, a.nombre AS asignatura, a.codigo AS codigo_asignatura, a.abreviatura AS abr_asignatura,  p.nombre AS programa, p.codigo AS codigo_programa, ase.semestre, j.nombre AS jornada, j.codigo AS codigo_jornada, ca.grupo, ca.periodo, ase.codigo AS asignatura_semestral, CONCAT(d.apellidos,' ',d.nombres) AS docente, evaluado_corte0,evaluado_corte1,evaluado_corte2,evaluado_corte3")
            ->from("cargas_academicas ca")
            ->join("usuarios d", "d.documento =  ca.docente", "INNER")
            ->join("asignaturas_semestrales ase", "ase.codigo =  ca.asignatura_semestral", "INNER")
            ->join("asignaturas a", "a.codigo =  ase.asignatura", "INNER")
            ->join("jornadas j", "j.codigo =  ca.jornada", "inner")
            ->join("planes_de_estudios ps", "ps.codigo = ase.plan_de_estudio", "INNER")
            ->join("programas p", "p.codigo = ps.programa", "INNER")
            ->where("d.documento", $docente)
            ->where("d.tipo", "D")
            ->where("ca.activa", 1)
            ->where("ase.activa", 1)

            ->order_by("ase.jornada, p.nombre, a.nombre, ase.semestre", "ASC");


        if (!is_null($periodo)) {

            $this->db->where("ca.periodo", $periodo);

        }

        if (!is_null($jornada)) {

            $this->db->where("ca.jornada", $jornada);

        }


        return $this->db->get()->result_array();


    }

    function removerCargaAcademica($codigo){


        $this->db->where("codigo",$codigo)->delete("cargas_academicas");

        return $this->db->affected_rows();


    }


    function consultarJorndasCargasAcademicas2($docente, $periodo = null, $jornada = null)
    {


        $this->db->select("p.nombre AS programa, a.nombre AS asignatura ,ca.docente, ca.jornada as codigo_jornada, ca.codigo AS carga_academica,  j.nombre as jornada")
            ->from("cargas_academicas ca")
            ->join("jornadas j", "j.codigo =  ca.jornada", "inner")
            ->join("asignaturas_semestrales ase", "ase.codigo =  ca.asignatura_semestral", "inner")
            ->join("asignaturas a", "a.codigo =  ase.asignatura", "inner")
            ->join("planes_de_estudios ps", "ps.codigo =  ase.plan_de_estudio", "inner")
            ->join("programas p", "p.codigo =  ps.programa", "inner")

            ->where("ca.docente", $docente)
            ->order_by("j.nombre", "ASC");



        if (!is_null($periodo)) {

            $this->db->where("ca.periodo", $periodo);

        }


        if (!is_null($jornada)) {

            $this->db->where("ca.jornada", $jornada);

        }

        return $this->db->get()->result_array();


    }



    function consultarJorndasCargasAcademicas($docente, $periodo = null, $jornada = null)
    {


        $this->db->select("ca.docente, ca.jornada as codigo_jornada,  j.nombre as jornada, ca.codigo AS carga_academica")
            ->from("cargas_academicas ca")
            ->join("jornadas j", "j.codigo =  ca.jornada", "inner")
            ->where("ca.docente", $docente)
            ->order_by("j.nombre", "ASC")
            ->group_by("j.codigo");

        if (!is_null($jornada)) {

            $this->db->where("ca.jornada", $jornada);

        }

        if (!is_null($periodo)) {

            $this->db->where("ca.periodo", $periodo);

        }

        return $this->db->get()->result_array();


    }


    function editarMatricula($codido, $datos)
    {


        $this->db->where("codigo", $codido);

        $this->db->update("matriculas", $datos);


        return $this->db->affected_rows();
    }

    function editarUsuario($documento, $datos)
    {


        $this->db->where("documento", $documento);

        $this->db->update("usuarios", $datos);


        return $this->db->affected_rows();
    }


    public function consultarProximoNumeroDeGrupo($periodo, $programa, $semestre, $jornada)
    {


        $this->db->select("COUNT(*)+1  AS numero")
            ->from("grupos")
            ->where("periodo", $periodo)
            ->where("jornada", $jornada)
            ->where("programa", $programa)
            ->where("semestre", $semestre);

        return $result = $this->db->get()->result_array()[0]['numero'];


    }


    function registrarAsignatura($datos)
    {


        $this->db->insert("asignaturas", $datos);


        return $this->db->affected_rows();
    }


    function editarNotas($codigo, $datos)
    {


        $this->db->where("codigo", $codigo);

        $this->db->update("notas", $datos);


        return $this->db->affected_rows();
    }

    function editarAsignatura($codigo, $datos)
    {


        $this->db->where("codigo", $codigo);

        $this->db->update("asignaturas", $datos);


        return $this->db->affected_rows();
    }

    function consultarAsignaturaPorNombre($nombre)
    {


        $this->db->select("*");
        $this->db->from("asignaturas");
        $this->db->where("nombre", $nombre);

        return $this->db->get()->result_array();


    }


    function consultarProgramas($codigo = null)
    {


        $this->db->select("*")
            ->from("programas");


        if (!is_null($codigo)) {

            $this->db->where("codigo", $codigo);

        }


        return $this->db->get()->result_array();
    }


    function consultarAsignatura($codigo=null)
    {


        $this->db->select("*");
        $this->db->from("asignaturas");



        if (!is_null($codigo)){

            $this->db->where("codigo", $codigo);

        }

        return $this->db->get()->result_array();


    }


    function consultarJornadas()
    {


        $this->db->select("*")
            ->from("jornadas")
            ->where("activa", 1);

        return $this->db->get()->result_array();


    }

    function consultarPeriodos($codigo = null)
    {


        $this->db->select("*");
        $this->db->from("periodos");
        $this->db->order_by("codigo", "DESC");

        if (!is_null($codigo)) {

            $this->db->where("codigo", $codigo);
        }


        return $this->db->get()->result_array();


    }


    function consultarPeriodoMatriculasAbiertas($estado = 1)
    {


        $this->db->select("*")
            ->from("periodos")
            ->where("matriculas_abiertas", $estado)
            ->order_by("codigo", "DESC");


        return $this->db->get()->result_array();


    }

    function consultarPeriodoActovoYMatriculasAbiertas()
    {


        $this->db->select("*")
            ->from("periodos")
            ->where("matriculas_abiertas", 1)
            ->or_where("actual", 1);

        return $this->db->get()->result_array();

    }


    public function consultarCantidadInscripciones()
    {


        /*
                $result = $this->db->query("SELECT COUNT(*)
                                                FROM inscripciones
                                                WHERE periodo = 20192
                                                GROUP BY matriculado",FALSE);

                return $result->result_array();

                */


        $this->db->select(" COUNT(i.matriculado) AS cant, i.matriculado")
            ->from("inscripciones i ")
            ->join("periodos p", "p.codigo = i.periodo", "INNER")
            ->where("p.matriculas_abiertas", 1)
            ->group_by("i.matriculado");


        return $this->db->get()->result_array();

    }

    public function consultarInscripciones($periodo = null, $programa = null)
    {


        $this->db->select("e.documento,i.periodo,i.programa, CONCAT(e.apellidos,' ',e.nombres) AS estudiante, p.nombre AS programa, i.fecha_registro, e.celular, e.correo, j.nombre AS jornada, i.matriculado", FALSE)
            ->from("inscripciones i")
            ->join("usuarios e", "i.estudiante= e.documento")
            ->join("programas p", "i.programa= p.codigo")
            ->join("jornadas j", "j.codigo= i.jornada")
            ->where_in("e.tipo", ["E", "ASP"])
            ->order_by("i.programa, j.nombre");

        if (!empty($periodo)) {

            $this->db->where("i.periodo", $periodo);
        }

        if (!empty($programa)) {

            $this->db->where("i.programa", $programa);
        }


        return $this->db->get()->result_array();


    }

    public function consultarProgramasOrientados($documento_docente = null, $corte=1, $jornda = null, $periodo=null, $subirNotas=null)
    {

        $this->db->select("p.nombre,p.codigo, COUNT(ca.codigo) as cant")
            ->from("cargas_academicas ca")
            ->join('asignaturas_semestrales aps', 'aps.codigo = ca.asignatura_semestral')
            ->join('planes_de_estudios ps', 'ps.codigo = aps.plan_de_estudio')
            ->join('programas p', 'p.codigo = ps.programa')
            ->where("evaluado_corte" . $corte, 0)
            ->where("aps.activa", 1)
            ->where("ca.activa",1)
            ->where("ca.periodo", $periodo);




        if (!is_null($jornda)) {

            $this->db->where('ca.jornada', $jornda);

        }

        if (!is_null($subirNotas)) {


            $this->db->where('ca.subir_notas', 1);

        }

        if (!is_null($documento_docente)) {

            $this->db->where("ca.docente", $documento_docente);

        }


        $this->db->group_by("p.nombre");

        return $this->db->get()->result_array();
    }


    function consultarFechasDigitacionNotas($periodo)
    {


        $this->db->select("*");
        $this->db->from("fechas_digitaciones_notas f")
            ->where("periodo", $periodo)
            ->where("CURDATE() >= fecha_inicio")
            ->where(" CURDATE()<= fecha_fin");

        return $this->db->get()->result_array();

    }


    public function consultarMatriculas($periodo = null, $programa = null, $semestre = null, $jornada = null)
    {


        $this->db->select("g.periodo,g.semestre AS semestre, j.nombre AS jornada, CONCAT( e.nombres,' ',e.apellidos) AS estudiante, p.nombre AS programa, g.grupo AS grupo", FALSE)
            ->from("matriculas m")
            ->join("usuarios e", "m.estudiante= e.documento")
            ->join("grupos g", "g.codigo= m.grupo")
            ->join("programas p", "g.programa= p.codigo")
            ->join("jornadas j", "j.codigo= g.jornada");

        if (!empty($periodo)) {

            $this->db->where("g.periodo", $periodo);
        }

        if (!empty($programa)) {

            $this->db->where("g.programa", $programa);
        }


        if (!empty($semestre)) {

            $this->db->where("g.semestre", $semestre);
        }

        if (!empty($jornada)) {

            $this->db->where("g.jornada", $jornada);
        }

        $this->db->order_by("g.periodo", "DESC");
        return $this->db->get()->result_array();


    }


    public function consultarListadoMatriculas($periodo = null)
    {


        $this->db->select("m.codigo, g.periodo,g.semestre AS semestre, j.codigo AS jornada, CONCAT( e.nombres,' ',e.apellidos) AS estudiante, p.codigo AS programa, g.grupo AS grupo, g.periodo", FALSE)
            ->from("matriculas m")
            ->join("usuarios e", "m.estudiante= e.documento")
            ->join("grupos g", "g.codigo= m.grupo")
            ->join("programas p", "g.programa= p.codigo")
            ->join("jornadas j", "j.codigo= g.jornada")
            ->where("m.matricula_academica", 0)
            ->where("m.activa",1)
            ->order_by("g.periodo", "DESC");

        if (!is_null($periodo)) {

            $this->db->where("g.periodo", $periodo);
        }

        return $this->db->get()->result_array();


    }


    function filtrarAsignatura($nombre)
    {


        $this->db->select("ase.codigo,a.nombre, p.nombre AS programa, ase.semestre, ase.semestre, j.nombre AS jornada, ase.activa")
            ->like("a.nombre", $nombre)
            ->from("asignaturas a")
            ->join("asignaturas_semestrales ase", "ase.asignatura = a.codigo", "INNER")
            ->join("planes_de_estudios ps", "ps.codigo = ase.plan_de_estudio", "INNER")
            ->join("jornadas j", "j.codigo = ase.jornada", "INNER")
            ->join("programas p", "p.codigo = ps.programa", "INNER")
            ->order_by("a.nombre, ase.semestre, j.codigo");


        return $this->db->get()->result_array();

    }


    function filtrarUsuarios($nombres, $activos = null)
    {


        $this->db->select("documento, CONCAT(apellidos,' ',nombres)  as nombres")
            ->like('apellidos', $nombres)
            ->where_in("tipo",["E","D"])
            ->from("usuarios");




        if (!is_null($activos)) {


            $this->db->where("activo", 1);

        }


        return $this->db->get()->result_array();

    }


    function filtrarDocente($nombres, $activos = null, $tipoUsuario = "A")
    {


        $this->db->select("documento, CONCAT(nombres,' ',apellidos)  as nombres, correo_institucional,  activo, celular")
            ->like('nombres', $nombres)
            ->or_like('apellidos', $nombres);


        if (strcmp($tipoUsuario, "A") == 0) {

            $this->db->from('vista_docentes');

        } else {
            $this->db->from('vista_docentes_y_administrativos');


        }


        if (!is_null($activos)) {


            $this->db->where("activo", 1);

        }


        return $this->db->get()->result_array();

    }


    public function consultarTodosLosProgramas()
    {

       $this->db->select("p.*, ps.codigo AS plan_de_estudio")
           ->from("programas p")
           ->join("planes_de_estudios ps", "ps.programa=p.codigo","INNER")
           ->where("ps.activo",1);


         return $this->db->get()->result_array();

    }

    function consultarTodasLasAsignatura()
    {


        $result = $this->db->get("asignaturas");

        return $result->result_array();


    }


    function crearGrupo($datos)
    {


        $this->db->insert("grupos", $datos);


        return $this->db->affected_rows();
    }


    function registrarOptenerUltimoID($tabla, $datos)
    {


        $this->db->insert($tabla, $datos);


        return $this->db->insert_id();
    }


    function consultarUsuariosSinClave($documento = null)
    {


        $this->db->select("*")
            ->from("usuarios u")
            ->where("u.clave", "")
            ->where("u.activo", 1);


        if (!is_null($documento)) {


            $this->db->where('documento', $documento);


        }

        return $this->db->get()->result_array();

    }


    function cambiarEstadoIncripcion($documentoEstudiante, $periodo)
    {


        $this->db->where("estudiante", $documentoEstudiante)
            ->where("periodo", $periodo)
            ->set("matriculado", "SI")
            ->update("inscripciones");

        return $this->db->affected_rows();


    }


    function registrarConfiguracion($datos)
    {


        $this->db->update("configuraciones", $datos);


        return $this->db->affected_rows();


    }


    function consultarMatriculaPorGrupo($documento_estudiante = null, $codigo_grupo = null)
    {


        $this->db->select("*")
            ->from("matriculas");


        if (!is_null($documento_estudiante)) {

            $this->db->where("estudiante", $documento_estudiante);

        }

        if (!is_null($codigo_grupo)) {

            $this->db->where("grupo", $codigo_grupo);

        }


        return $this->db->get()->result_array();


    }


    function consultarSemestre()
    {

        return $this->db->get("semestres")->result_array();

    }


    function consultarCursosAulaVirtual(){


        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual
            ->select(" UPPER(mdl_course.fullname) as fullname,shortname, 
UPPER(parent_category(mdl_course_categories.parent))  AS parent")
            ->join("mdl_course_categories","mdl_course.category = mdl_course_categories.id","INNER")
             ->from("mdl_course")
            ->order_by("fullname");






        return  $builder->get()->result_array() ;


    }


    function cancelarMatriculaAulaVirtual($id){

        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual->where("id",$id)->delete("mdl_user_enrolments");

    }


    function consultarUsuariosAulaVirtual($documento){


        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual
            ->select("username
           
         
            ")

            ->from("mdl_user")

            ->where("username",$documento);

        return  $builder->get()->result_array() ;


    }



    function consultarMatriculasCursosAulaVirtual($documento){


        $CI =& get_instance();

        $aulaVirtual = $CI->load->database('aulavirtual',TRUE);
        $builder= $aulaVirtual
            ->select("
            
            
            en.id, UPPER(parent_category(cc.parent)) AS programa ,UPPER(c.fullname) AS curso
            ")

            ->from("mdl_user_enrolments en")

            ->join("mdl_enrol er","er.id = en.enrolid","INNER")
            ->join("mdl_user u","u.id = en.userid ","INNER")

            ->join("mdl_course c","c.id = er.courseid","INNER")
            ->join("mdl_course_categories cc","cc.id = c.category","INNER")
            ->where("u.username",$documento)
            ->order_by("programa");




        return  $builder->get()->result_array() ;


    }


    function gruposMooble($grupo)
    {


        $this->db->select("
        
            e.documento AS username,  
            e.nombres AS firstname,
            e.apellidos AS lastname,
            e.correo AS email,
            e.celular as phone2,
        
        ")
            ->from("matriculas m")
            ->join("grupos g", "g.codigo = m.grupo", "INNER")
            ->join("usuarios e", "e.documento = m.estudiante", "INNER")
            ->where("g.codigo", $grupo)
            ->where("m.activa",1)
            ->where("e.correo!=", "");

        return $this->db->get()->result_array();


    }

}
