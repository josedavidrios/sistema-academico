<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Estudiante extends CI_Controller
{


    protected $login;
    protected $peridoActual;
    protected $matriculaAulaVirtual;

    function __construct()
    {

        parent::__construct();


        $this->load->helper("fecha");
        $this->load->helper("mi");

        $this->login = $this->session->userdata('documento');
        $this->load->model("Estudiante_model", "estudianteModel");

        $this->load->model("admin_model", "adminModel");


        $this->peridoActual = $this->session->userdata('periodo');

        $this->load->model("Usuario_model", "usuarioModel");
        $this->matriculaAulaVirtual= $this->usuarioModel->consultarMatriculaAulaVirtual($this->login);

        if ($this->session->userdata('tipo') != "E") {

            redirect(base_url());

        }


    }


    function consultarEvaluacionesDocentesPendientes()
    {

        $evaluacion = $this->estudianteModel->consultarEvaluacionesDocentes($this->login);


        if (count($evaluacion) > 0) {


            $cantidad = $this->estudianteModel->consultarDocentesPorEvaluar($evaluacion[0]['evaluacion']);


            return count($cantidad);


        } else {


            return 0;
        }

    }


    function actualizarDatosPersonales()
    {
        $tipoDocumento = to_uppercase_and_trim($this->input->post("tipo-documento"));
        $lugarExpedicionDocumento = $this->input->post("lugar-expedicion-documento");

        $nombres = to_uppercase_and_trim(remove_accents($this->input->post("nombres")));
        $apellidos = to_uppercase_and_trim(remove_accents($this->input->post("apellidos")));

        $fechaNacimiento = $this->input->post("fecha-nacimiento");
        $correo = to_uppercase_and_trim($this->input->post("correo"));
        $celular = mb_strtoupper($this->input->post("celular"));
        $sexo = mb_strtoupper($this->input->post("sexo"));
        $lugarResidencia = $this->input->post("lugar-residencia");
        $lugarNacimiento = $this->input->post("lugar-nacimiento");
        $direccion = to_uppercase_and_trim($this->input->post("direccion"));
        $barrio = to_uppercase_and_trim($this->input->post("barrio"));
        $nivelEducacion = $this->input->post("nivel-educacion");
        $estadoCivil = $this->input->post("estado-civil");
        $tipoSangre = to_uppercase_and_trim($this->input->post("tipo-sangre"));
        $zona = to_uppercase_and_trim($this->input->post("zona"));
        $titulo = to_uppercase_and_trim($this->input->post("titulo"));
        $estrato = $this->input->post("estrato");

        $eps = $this->input->post("eps");




        if($titulo=="BACHILLER"){

            $titulo = "BACHILLER ACADÉMICO";

        }



        $datos = [


            "nombres" => $nombres,
            "apellidos" => $apellidos,
            "fecha_nacimiento" => $fechaNacimiento,
            "tipo_documento" => $tipoDocumento,
            "correo" => $correo,
            "sexo" => $sexo,
            "direccion" => $direccion,
            "celular" => $celular,
            "lugar_residencia" => $lugarResidencia,
            "lugar_nacimiento" => $lugarNacimiento,
            "barrio" => $barrio,
            "nivel_educacion" => $nivelEducacion,
            "tipo_sangre" => $tipoSangre,
            "zona" => $zona,
            "estrato" => $estrato,
            "ultima_fecha_modificacion"=>fecha_y_hora_actual(),
            "estado_civil"=>$estadoCivil,
            "datos_actualizados"=>1,
            "titulo"=>$titulo,
             "eps"=>$eps,
            "lugar_expedicion_documento"=>$lugarExpedicionDocumento


        ];




          $this->estudianteModel->actualizar($this->login, $datos);


          $datosMoodle =[

              "firstname"=>$nombres,
              "lastname"=>$apellidos,
              "email"=>$correo,
              "phone2"=>$celular

          ];

          $this->usuarioModel->actualizarDatosMoodle($this->login,$datosMoodle);

          redirect(base_url('estudiante'));


    }


    function vistaActuliacionDeDatosPersonales()
    {




        $configuracion = $this->adminModel->consultarConfiguracion();




        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');
        $datos['js'] = ["select2/select2.min.js", "select2/es.js", "sistema-academico/actualizarDatosPersonales.js"];

        $datos['titulo'] = "Sistema Académico - Estudiantes";


        $datos['niveles'] = $this->adminModel->consultarNivelesEducacion();
        $datos['tipos_documentos'] = $this->adminModel->consultarTiposDeDocumentos();
        $datos['estados_civiles'] = $this->adminModel->consultarEstadosCiviles();

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;




        $datos['estudiante'] = $this->estudianteModel->consultar($this->login)[0];
        $datos['contenido'] = '../estudiante/actualizacion_datos_personales/formulario';

        $this->load->view("estudiante/plantilla", $datos);

    }


    function vistaMensajeDeActualizacionDeDatosPersonales(){

        $datos['css'] = ['jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css', 'jquery-confirm/jquery-confirm.css'];
        $datos['js'] = ['jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js'];

        $datos['titulo'] = "Sistema Académico - Estudiantes";

        $datos['contenido'] = '../estudiante/actualizacion_datos_personales/mensaje';
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $this->load->view("estudiante/plantilla", $datos);
    }

    function index()
    {



        $configuracion = $this->adminModel->consultarConfiguracion();


        if($configuracion['actualizacion_datos_personales']==1){


            $usuario = $this->estudianteModel->consultar($this->login)[0];





            if ( $usuario['tipo_documento'] =="0" ||
                 $usuario['lugar_expedicion_documento']==0 ||
                $usuario['correo'] =="" ||
                is_null($usuario['lugar_expedicion_documento']) ||
                $usuario['lugar_expedicion_documento'] ==0||
                empty($usuario['titulo'])  ||
                empty($usuario['tipo_sangre'])  ||
                empty($usuario['zona'])  ||
                $usuario['lugar_residencia']==0 ||
                $usuario['lugar_nacimiento']==0 ||
                $usuario['eps']=="0" ||
                $usuario['fecha_nacimiento']=="0000-00-00" ||
                $usuario['estado_civil']==0






            ){

                $this->vistaActuliacionDeDatosPersonales();

            }else{

                $this->notas();

            }



        }else{

            $this->notas();

        }






    }


    function vistaRealizarEvaluacionDocente()
    {


        //  $this->notas();


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css', 'jquery-confirm/jquery-confirm.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js');

        $datos['titulo'] = "Sistema Académico - Estudiantes";

        $datos['contenido'] = '../estudiante/evaluacion_docente/mensaje';

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $this->load->view("estudiante/plantilla", $datos);


    }


    function notas($opcion = 1, $programa = 1, $periodo = 1)
    {

        if ($this->consultarEvaluacionesDocentesPendientes() == 0) {


            if ($opcion == 1) {

                $this->vistaNotasSemestreActual();

            } else if ($opcion == "de") {


                $this->vistaNotasPorSemestre($programa, $periodo);

            } else if ($opcion == "historico") {

                $this->vistaHistoricoDeNotas();

            }

        } else {


            $this->vistaRealizarEvaluacionDocente();

        }


    }


    function vistaHistoricoDeNotas()
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'filtrarMunicipios.js', 'select2/select2.min.js', 'select2/es.js', 'estudiante.js');

        $datos['titulo'] = "Historico de Notas - Sistema Académico";

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['programas'] = $this->estudianteModel->consultarProgramasMatriculados($this->login);


        $datos['contenido'] = '../estudiante/notas/historico';
        $this->load->view("estudiante/plantilla", $datos);


        //  $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css');
        //  $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js');

        /*
        $datos['titulo'] = "Sistema Académico - Histotico de Notas";

        $datos['contenido'] = '../estudiante/notas/hostorial';

        $this->load->view("estudiante/plantilla", $datos);
*/

    }


    function vistaEvaluacionesDocentes()
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'filtrarMunicipios.js', 'select2/select2.min.js', 'select2/es.js', 'estudiante.js');

        $datos['titulo'] = "Evaluación Docente - Sistema Académico";


        $datos['programas'] = $this->estudianteModel->consultarEvaluacionesDocentes($this->login);

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['contenido'] = '../estudiante/evaluacion_docente/listado_evaluaciones';
        $this->load->view("estudiante/plantilla", $datos);


    }


    function registrarEvaluacionDocente()
    {


        $carga = $this->input->post("carga");
        $evaluacion = $this->input->post("evaluacion");
        $observaciones = to_uppercase_and_trim($this->input->post("observaciones"));


        $datos = [

            "observaciones" => $observaciones,
            "fecha_registro" => fecha_y_hora_actual(),
            "estado" => 1


        ];


        $this->db->trans_start();


        $this->estudianteModel->actualizarEstadoEvaluacionDocentePorAsignatura($evaluacion, $carga, $datos);


        $codigoEvaluacionAsignatura = $this->estudianteModel->consultarEvaluacionAsignatura($evaluacion, $carga);

        $respuestas = [];

        for ($i = 1; $i < count($this->estudianteModel->consultarPreguntasEvaluacionDocente()); $i++) {

            $respuestas[] = [

                "evaluacion_docente_por_asignatura" => $codigoEvaluacionAsignatura,
                "pregunta_evaluacion_docente" => $i,
                "calificacion" => $this->input->post($i . ""),
                "fecha_registro" => fecha_y_hora_actual()


            ];

        }

        $this->estudianteModel->registrarRespuestasEvaluacionDocente($respuestas);


        $this->db->trans_complete();


        $evaluacionesPendientes = $this->estudianteModel->consultarDocentesPorEvaluar($evaluacion);


        if (count($evaluacionesPendientes) == 0) {


            $datosEvaluacion = ["estado" => 1,
                "ultima_fecha_actualizacion" => fecha_y_hora_actual()
            ];

            $this->estudianteModel->actualizarEstadoEvaluacionDocente($evaluacion, $datosEvaluacion);


        }

        // var_dump(count($evaluacionesPendientes));
        redirect(base_url("estudiante/vistaListadoDocentes/" . $evaluacion));


    }

    function vistaNotasSemestreActual($programa = 1, $periodo = 1)
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'filtrarMunicipios.js', 'select2/select2.min.js', 'select2/es.js', 'estudiante.js');

        $datos['titulo'] = "Programas Matriculados - Sistema Académico";

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['programas'] = $this->estudianteModel->consultarProgramasMatriculados($this->login, $this->peridoActual);






        $datos['contenido'] = '../estudiante/notas/semestre_actual';
        $this->load->view("estudiante/plantilla", $datos);


    }


    function vistaNotasPorSemestre($programa = 1, $periodo = 1)
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'filtrarMunicipios.js', 'select2/select2.min.js', 'select2/es.js', 'estudiante.js');

        $datos['titulo'] = "Notas - Sistema Académico";

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        //  $datos['asignaturas'] = $this->estudianteModel->consultarNotas($this->login);


        $matricula = $this->estudianteModel->consultarProgramasMatriculados($this->login);


        if (count($matricula) > 0) {



            $datos['contenido'] = '../estudiante/notas/por_programa_y_periodo_sabados';

            /*
            if ($matricula[0]['jornada'] == "S") {


                $datos['contenido'] = '../estudiante/notas/por_programa_y_periodo_sabados';

            } else {

                $datos['contenido'] = '../estudiante/notas/por_programa_y_periodo';
            }

            */


        } else {

            $datos['contenido'] = '../estudiante/notas/historico';
        }


        $datos['asignaturas'] = $this->estudianteModel->consultarMateriasMatriculadas($this->login, $programa, $periodo);


        $this->load->view("estudiante/plantilla", $datos);


    }


    function vistaVistaEvaluacionDocendePorAsignatura($codigoEvaluacion, $codigoCarga)
    {


        $datos['css'] = array('pnotify/pnotify.css', 'pnotify/pnotify.nonblock.css', 'pnotify.buttons.css');
        $datos['js'] = array('sistema-academico/evaluacionDocente.js', 'pnotify/pnotify.js', 'pnotify/pnotify.buttons.js', 'pnotify/pnotify.nonblock.js');

        $datos['titulo'] = "Realizar Evaluación Docente - Sistema Académico";


        //  $datos['asignaturas'] = $this->estudianteModel->consultarNotas($this->login);


        //Consultar anterior

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['preguntas'][0] = $this->estudianteModel->consultarPreguntasEvaluacionDocente(1);
        $datos['preguntas'][1] = $this->estudianteModel->consultarPreguntasEvaluacionDocente(2);
        $datos['preguntas'][2] = $this->estudianteModel->consultarPreguntasEvaluacionDocente(3);
        $datos['preguntas'][3] = $this->estudianteModel->consultarPreguntasEvaluacionDocente(4);
        $datos['carga'] = $codigoCarga;
        $datos['evaluacion'] = $codigoEvaluacion;

        $datos['contenido'] = '../estudiante/evaluacion_docente/listado_preguntas';
        $this->load->view("estudiante/plantilla", $datos);


    }


    function vistaListadoDocentes($codigoEvaluacion)
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'filtrarMunicipios.js', 'select2/select2.min.js', 'select2/es.js', 'estudiante.js');

        $datos['titulo'] = "Evaluación Docente - Sistema Académico";

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        //  $datos['asignaturas'] = $this->estudianteModel->consultarNotas($this->login);


        //Consultar anterior


        $datos['cargas'] = $this->estudianteModel->consultarDocentesPorEvaluar($codigoEvaluacion);


        $datos['contenido'] = '../estudiante/evaluacion_docente/listado_docentes';
        $this->load->view("estudiante/plantilla", $datos);


    }


}
