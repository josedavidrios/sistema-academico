<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Usuario extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public $login;
    protected $matriculaAulaVirtual;

    function __construct()
    {
        parent::__construct();


        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Admin_model", "adminModel");
        $this->load->helper("fecha");
        $this->load->helper("mi");
        $this->load->helper("encrypt");



        $this->matriculaAulaVirtual= $this->usuario->consultarMatriculaAulaVirtual($this->login);
    }







    function inscribir()
    {


        $exito = 0;

        $documento = $this->input->post("documento");
        $tipoDocumento = to_uppercase_and_trim($this->input->post("tipo-documento"));
        $nombres = to_uppercase_and_trim(remove_accents($this->input->post("nombres")));
        $apellidos = to_uppercase_and_trim(remove_accents($this->input->post("apellidos")));

        $fecha_nacimiento = $this->input->post("fecha-nacimiento");
        $correo = to_uppercase_and_trim($this->input->post("correo"));
        $celular = mb_strtoupper($this->input->post("celular"));

        $sexo = mb_strtoupper($this->input->post("sexo"));


        $lugarResidencia = $this->input->post("lugar-residencia");
         $programa = $this->input->post("programa");
        $jornada = $this->input->post("jornada");



        $config =   $this->adminModel->consultarConfiguracion();

        $datosEstudiante = [

            "documento" => $documento,
            "nombres" => $nombres,
            "apellidos" => $apellidos,
            "fecha_nacimiento" => $fecha_nacimiento,
            "tipo_documento" => $tipoDocumento,
            "correo" => $correo,
            "sexo" => $sexo,
            "celular" => $celular,
            "lugar_residencia" => $lugarResidencia,
            "tipo" => "ASP",
            "fecha_registro"=>fecha_y_hora_actual(),



        ];


        $usuario = $this->usuario->consultar($documento);


        if (count($usuario) == 0) {




            $datosEstudiante["fecha_registro"] = fecha_y_hora_actual();

            $this->usuario->registrar("usuarios", $datosEstudiante);


            $periodo = $this->usuario->consultarPeriodoMatriculasAbiertas()[0]['codigo'];

            $datosInscripcion = [


                "estudiante" => $documento,
                "programa" => $programa,
                "periodo" => $periodo,
                "jornada" => $jornada,
                "fecha_registro" => fecha_y_hora_actual()

            ];


            $registro = $this->usuario->registrar("inscripciones", $datosInscripcion);


            if (!is_null($correo) && strlen($correo) > 1 && !isLocalHost()) {





                if ($config['inscripciones_web']==1){

                    $this->enviarCorreoInscripcion($correo, ucwords(strtolower($nombres." ".$apellidos)),$programa);

                }else{


                    $this->enviarCorreoInscripcionCerrada($correo, ucwords(strtolower($nombres." ".$apellidos)),$programa);


                }







            }


        }else{


            if (!is_null($correo) && strlen($correo) > 1 && !isLocalHost()) {


                $this->enviarCorreoNoInscripcion($correo, ucwords(strtolower($nombres." ".$apellidos)));

            }


        }



            if ($config['inscripciones_web']==1){

                redirect("https://ebah.edu.co/inscripcion-exitosa");

            }else{


                redirect("https://ebah.edu.co");


            }









    }






    function index()
    {


        $datos['titulo'] = "Cambiar contreña - Sistema Académico";
        $datos['contenido'] = '../cambiar_clave/cambiar_clave_de_acceso';
        $datos['js'] = array("sistema-academico/cambiarClave.js");


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;


        $tipo = $this->session->userdata('tipo');


        $ruta = "";

        if ($tipo == "E") {


            $ruta = "estudiante";

        } else if ($tipo == "A" || $tipo == "SA") {


            $ruta = "admin";
        }
        else if ($tipo == "D") {


            $ruta = "docente";
        }




        $this->load->view($ruta . "/plantilla", $datos);


    }


    function inscripciones(){


        $this->load->view('inicio/inscripciones');

    }

    function cambiarClaveDeAcceso()
    {

        $this->load->helper("encrypt");


        $claveActual = $this->input->post('clave-actual');


        $claveNueva = $this->input->post('clave-nueva');
        $claveNuevaConfirmada = $this->input->post('clave-nueva-confirmada');


        if (strcmp($claveNueva, $claveNuevaConfirmada) == 0) {


            $datos = array(

                "clave" => encrypt($claveNuevaConfirmada),
                "cambio_clave" => 1

            );

            $this->db->trans_start();

            $result = $this->usuario->cambiarClaveDeAcceso($this->session->userdata("documento"),$claveActual, $datos);


            if(strpos(base_url(), "http://localhost")=== false){

                 $this->usuario->cambiarClaveDeAccesoMoodle($this->session->userdata("documento"), $claveNuevaConfirmada);

            }




            if ($result>0){


            //



                $usuario = $this->usuario->consultar($this->session->userdata("documento"));



                if (count($usuario)){


                    if (isset($usuario[0]['correo'])){

                        $this->enviarCorreoCambioClave($usuario[0]['correo'],ucwords(strtolower($usuario[0]['nombres'])));
                    }

                    if (isset($usuario[0]['correo_institucional'])){

                        $this->enviarCorreoCambioClave($usuario[0]['correo_institucional'],ucwords(strtolower($usuario[0]['nombres'])));
                    }



                }
                $this->db->trans_complete();

              //  $this->session->set_tempdata('cambio-clave', 'exito');

                $this->session->sess_destroy();
                redirect(base_url());



            }else{
                $this->session->set_tempdata('cambio-clave', 'exito');
                redirect(getenv('HTTP_REFERER'));

            }




        }


    }



    function filtrarMunicipios()
    {


        if (isset($_GET['term'])) {

            // $nombres = $_GET['term'];

            $nombre = strtolower($_GET['term']);
            $valores = $this->usuario->filtrarMunicipios($nombre);


            echo json_encode($valores);


        }


    }


    function filtrarEPS()
    {


        if (isset($_GET['term'])) {

            // $nombres = $_GET['term'];

            $nombre = strtolower($_GET['term']);
            $valores = $this->usuario->filtrarEPS($nombre);


            echo json_encode($valores);


        }


    }




     function enviarCorreoCambioClave($correoDestinatario, $nombreDestinatario)
    {


        $nombreProgrma="";
        $linkPlanEstudio="#";

        $this->load->library('email');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'notificaciones@ebah.edu.co',
            'smtp_pass' => 'notificacionesweb',
            'smtp_timeout' => '7',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'mailtype' => 'html',
            'validation' => false
        );








        $this->email->initialize($config)

            ->from('notificaciones@ebah.edu.co')
            ->to(strtolower($correoDestinatario))
            ->subject("Cambio de Contraseña  - Sistema Académico EBAH")
            ->set_mailtype('html')







            ->message('
                
                
     <div>
    <div>


        <div style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0;padding:0">


            <center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#f2f2f2" style="background-color:#f2f2f2!important;border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td height="7" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;border-spacing:0;border-collapse:collapse;height:7px;font-size:1px;line-height:1px;background:#008b21;padding:0" bgcolor="#8dc23e">
                            </td>

                        </tr>


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                <table width="100%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">


                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                        <br>





                                        <tr height="2" style="height:2px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>


                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                            <td width="130" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <a href="https://www.ebah.edu.co/" style="color:#8dc23e;text-decoration:underline" target="_blank" data-saferedirecturl="">

                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/logo-ebah-sistema-academico.png" width="250" alt="Logo EBAH" style="float:none;display:block;border:0 none" align="none">
                                                                </a>
                                                            </td>


                                                        </tr>



                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr height="25" style="height:25px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>





                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;padding:0">
                                                <table width="96.67%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>



                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="76%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                                <p style="font-size:16px;line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">Hola, ' . $nombreDestinatario . '</p>

                                                                             
                                                                                <p style="font-size:16px;text-align: justify; line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">
                                                                            
                                                                            
                                                                            Tu contraseña se ha cambiado, recuerda que el cambio aplica para el Sistema Académico y El Aula Virtual.    
                                                                                    
                                                                                </p>

                                                                    
                                                 
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>



                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>







                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">
                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td height="45" style="height:45px;word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>



                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">

                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table class="m_-1662020414733504001responsive-table" border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                                        <tr height="35" style="height:35px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                      
                                                                                       <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                 <i>    Esta es una notificación automática, por favor no responda este mensaje. </i>

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                      
                                                                       
                                                                      
                                                                       
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                     Mantente conectado para recibir consejos, actualizaciones y recursos

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.facebook.com/BellasArtesEbah/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/facebook.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Facebook icon" align="none">
                                                                                </a>
                                                                            </td>
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://twitter.com/BellasArteSucre" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/twitter.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Twitter icon" align="none">
                                                                                </a>
                                                                            </td>

                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.instagram.com/bellasartesdesucre/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/instagram.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Linkedin icon" align="none" class="CToWUd">
                                                                                </a>
                                                                            </td>



                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="line-height:22px!important;font-size:12px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0 0 28px">
                                                                                    <a style="color:#aaaaaa;text-decoration:underline" href="h" target="_blank" data-saferedirecturl=""><a style="color:#aaaaaa!important;text-decoration:none">

                                                                                            Calle 19 No. 16A - 09 Barrio Ford, Sincelejo - Sucre <br>
                                                                                            Teléfono: 035 2823160 - 3164933505 <br>

                                                                                            Correo: info@ebah.edu.co



                                                                                        </a>


                                                                                </p>
                                                                            </td>
                                                                        </tr>


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </center>



        </div>
    </div>

</div>
                
                
                
                
                ');


return $this->email->send();



    }

    function enviarCorreoInscripcion($correoDestinatario, $nombreDestinatario, $codigoPrograma)
    {


        $nombreProgrma="";
        $linkPlanEstudio="#";

        $this->load->library('email');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'notificaciones@ebah.edu.co',
            'smtp_pass' => 'notificacionesweb',
            'smtp_timeout' => '7',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'mailtype' => 'html',
            'validation' => false
        );






        if($codigoPrograma=="DG"){

            $nombreProgrma ="Diseño Gráfico";
            $linkPlanEstudio="https://www.ebah.edu.co/documentos/planes-de-estudio/diseno-grafico.pdf";


        }else if ($codigoPrograma=="AP"){

            $nombreProgrma ="Artes Plásticas";
            $linkPlanEstudio="https://www.ebah.edu.co/documentos/planes-de-estudio/artes-plasticas.pdf";

        }else if ($codigoPrograma=="MI"){

            $nombreProgrma ="Música Instrumental";
            $linkPlanEstudio="https://www.ebah.edu.co/documentos/planes-de-estudio/musica-instrumental.pdf";

        }else if ($codigoPrograma=="CD"){

            $nombreProgrma ="Coreografía para la Danza";
            $linkPlanEstudio="https://www.ebah.edu.co/documentos/planes-de-estudio/coreografia-para-la-danza.pdf";


        }



        $this->email->initialize($config)

            ->from('notificaciones@ebah.edu.co')
            ->to(strtolower($correoDestinatario))
            ->bcc('jose.rios@ebah.edu.co')
            ->subject("Inscripción de $nombreDestinatario - Escuela de Bellas Artes y Humanidades de Sucre")
            ->set_mailtype('html')







            ->message('
                
                
     <div>
    <div>


        <div style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0;padding:0">


            <center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#f2f2f2" style="background-color:#f2f2f2!important;border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td height="7" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;border-spacing:0;border-collapse:collapse;height:7px;font-size:1px;line-height:1px;background:#008b21;padding:0" bgcolor="#8dc23e">
                            </td>

                        </tr>


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                <table width="100%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">


                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                        <br>





                                        <tr height="2" style="height:2px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>


                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                            <td width="130" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <a href="https://www.ebah.edu.co/" style="color:#8dc23e;text-decoration:underline" target="_blank" data-saferedirecturl="">

                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/logo-ebah-sistema-academico.png" width="250" alt="Logo EBAH" style="float:none;display:block;border:0 none" align="none">
                                                                </a>
                                                            </td>


                                                        </tr>



                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr height="25" style="height:25px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>





                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;padding:0">
                                                <table width="96.67%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>



                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="76%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                                <p style="font-size:16px;line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">Hola, ' . $nombreDestinatario . '</p>

                                                                             
                                                                                <p style="font-size:16px;text-align: justify; line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">
                                                                                    
                                                                             Te encuentras inscrito(a) al programa <b>'.$nombreProgrma.'</b>. Para continuar con el proceso de matrícula debes consignar $50.000 en la cuenta 463030026667 del Banco Agrario. Acercarte a nuestras instalaciones con los requisitos que aparecen a continuación:
                                                                                    
                                                                                </p>

                                                                    
                                                                                <ul style="margin-top:0;margin-bottom:28px!important">
                                                                                  <li style="font-size:16px;line-height:30px;margin:0">Copia del recibo de consignación.</li>
                                                                                    <li style="font-size:16px;line-height:30px;margin:0">Copia del diploma.</li>
                                                                                    <li style="font-size:16px;line-height:30px;margin:0">Certificado de estudios (9mo, 10mo y 11mo)</li>
                                                                                    <li style="font-size:16px;line-height:30px;margin:0">Copia documento de identidad.</li>
                                                                                    <li style="font-size:16px;line-height:30px;margin:0">Certificado médico.</li>
                                                                                    <li style="font-size:16px;line-height:30px;margin:0">2 foto carnet fondo azul.</li>
                                                                                    <li style="font-size:16px;line-height:30px;margin:0">Certificado médico.</li>
                                                                                     <li style="font-size:16px;line-height:30px;margin:0">Recibo servicio de energía.</li>
                                                                                        <li style="font-size:16px;line-height:30px;margin:0">1 folder café.</li>
                                                                              </ul>

                                                                                <p style="font-size:16px;line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">
                                                                                    
                                                                                    
                                                                                    
                                                                           Si eres víctima de conflicto armado, indígena, Afro, ROM, padre o madre cabeza de hogar, comunidad LGBTI o Sisbenizado, puedes solicitar una BECA del 50% ó 70%.
                                                                                    
                                                                                </p>
                                                                                
                                                                                            <p style="font-size:16px;line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">
                                                                                    
                                                                                    
                                                                                    
                                                                                Tienes plazo hasta el 31 de enero para realizar tu matrícula y solicitar una beca.
                                                                                    
                                                                                </p>
                                                                                
                                                                                
                                                                                
                                                                                	<div style="text-align:center"><span style="font-family:arial,helvetica,sans-serif">
                                                                                	
                                                                                	<a href="'.$linkPlanEstudio.'" style="border-radius:2px;letter-spacing:1px;text-transform:uppercase;font-size:13px;font-weight:400;display:inline-block;background:#76c043 ;padding:15px 25px 14px;color:rgb(255,255,255);line-height:18px!important;text-decoration:none" target="_blank" data-saferedirecturl="">Ver Plan de Estudio</a>
                                                                                	
                                                                                	</span>
                                                                                	
                                                                                	</div>
                                                 
                                                 
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>



                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>







                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">
                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td height="45" style="height:45px;word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>



                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">

                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table class="m_-1662020414733504001responsive-table" border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                                        <tr height="35" style="height:35px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                      
                                                                                       <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                 <i>    Esta es una notificación automática, por favor no responda este mensaje. </i>

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                      
                                                                       
                                                                      
                                                                       
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                     Mantente conectado para recibir consejos, actualizaciones y recursos

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.facebook.com/BellasArtesEbah/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/facebook.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Facebook icon" align="none">
                                                                                </a>
                                                                            </td>
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://twitter.com/BellasArteSucre" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/twitter.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Twitter icon" align="none">
                                                                                </a>
                                                                            </td>

                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.instagram.com/bellasartesdesucre/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/instagram.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Linkedin icon" align="none" class="CToWUd">
                                                                                </a>
                                                                            </td>



                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="line-height:22px!important;font-size:12px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0 0 28px">
                                                                                    <a style="color:#aaaaaa;text-decoration:underline" href="h" target="_blank" data-saferedirecturl=""><a style="color:#aaaaaa!important;text-decoration:none">

                                                                                            Calle 19 No. 16A - 09 Barrio Ford, Sincelejo - Sucre <br>
                                                                                            Teléfono: 035 2823160 - 3164933505 <br>

                                                                                            Correo: info@ebah.edu.co



                                                                                        </a>


                                                                                </p>
                                                                            </td>
                                                                        </tr>


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </center>



        </div>
    </div>

</div>
                
                
                
                
                ');





        return  $this->email->send();

    }


    function enviarCorreoNoInscripcion($correoDestinatario, $nombreDestinatario)
    {


        $nombreProgrma="";
        $linkPlanEstudio="#";

        $this->load->library('email');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'notificaciones@ebah.edu.co',
            'smtp_pass' => 'notificacionesweb',
            'smtp_timeout' => '7',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'mailtype' => 'html',
            'validation' => false
        );



        $this->email->initialize($config)

            ->from('notificaciones@ebah.edu.co')
            ->to(strtolower($correoDestinatario))
            ->bcc('jose.rios@ebah.edu.co')
            ->subject("Inscripción de $nombreDestinatario - Escuela de Bellas Artes y Humanidades del Departamento de Sucre")
            ->set_mailtype('html')







            ->message('
                
                
     <div>
    <div>


        <div style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0;padding:0">


            <center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#f2f2f2" style="background-color:#f2f2f2!important;border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td height="7" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;border-spacing:0;border-collapse:collapse;height:7px;font-size:1px;line-height:1px;background:#008b21;padding:0" bgcolor="#8dc23e">
                            </td>

                        </tr>


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                <table width="100%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">


                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                        <br>





                                        <tr height="2" style="height:2px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>


                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                            <td width="130" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <a href="https://www.ebah.edu.co/" style="color:#8dc23e;text-decoration:underline" target="_blank" data-saferedirecturl="">

                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/logo-ebah-sistema-academico.png" width="250" alt="Logo EBAH" style="float:none;display:block;border:0 none" align="none">
                                                                </a>
                                                            </td>


                                                        </tr>



                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr height="25" style="height:25px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>





                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;padding:0">
                                                <table width="96.67%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>



                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="76%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                                <p style="font-size:16px;line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">Hola, ' . $nombreDestinatario . '</p>

                                                                             
                                                                                <p style="font-size:16px;text-align: justify; line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">
                                                                             
                                                                           
                                                                           La inscripción aplica solo para <strong> estudiantes de primer ingreso</strong>, si ya te has matriculado en algunos de nuestros programas anteriormente, debes acercarte a nuestras instalaciones a realizar de nuevo tu matricula.  
                                                                                    
                                                                                    
                                                                                </p>

                                                                    
                                                                 

                                                                                
                                                                                
                                                                
                                                 
                                                 
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>



                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>







                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">
                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td height="45" style="height:45px;word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>



                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">

                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table class="m_-1662020414733504001responsive-table" border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                                        <tr height="35" style="height:35px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                      
                                                                                       <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                 <i>    Esta es una notificación automática, por favor no responda este mensaje. </i>

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                      
                                                                       
                                                                      
                                                                       
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                     Mantente conectado para recibir consejos, actualizaciones y recursos

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.facebook.com/BellasArtesEbah/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/facebook.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Facebook icon" align="none">
                                                                                </a>
                                                                            </td>
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://twitter.com/BellasArteSucre" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/twitter.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Twitter icon" align="none">
                                                                                </a>
                                                                            </td>

                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.instagram.com/bellasartesdesucre/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/instagram.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Linkedin icon" align="none" class="CToWUd">
                                                                                </a>
                                                                            </td>



                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="line-height:22px!important;font-size:12px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0 0 28px">
                                                                                    <a style="color:#aaaaaa;text-decoration:underline" href="h" target="_blank" data-saferedirecturl=""><a style="color:#aaaaaa!important;text-decoration:none">

                                                                                            Calle 19 No. 16A - 09 Barrio Ford, Sincelejo - Sucre <br>
                                                                                            Teléfono: 035 2823160 - 3164933505 <br>

                                                                                            Correo: info@ebah.edu.co



                                                                                        </a>


                                                                                </p>
                                                                            </td>
                                                                        </tr>


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </center>



        </div>
    </div>

</div>
                
                
                
                
                ');





        return  $this->email->send();

    }


    function enviarCorreoInscripcionCerrada($correoDestinatario, $nombreDestinatario)
    {


        $nombreProgrma="";
        $linkPlanEstudio="#";

        $this->load->library('email');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'notificaciones@ebah.edu.co',
            'smtp_pass' => 'notificacionesweb',
            'smtp_timeout' => '7',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'mailtype' => 'html',
            'validation' => false
        );



        $this->email->initialize($config)

            ->from('notificaciones@ebah.edu.co')
            ->to(strtolower($correoDestinatario))
            ->bcc('jose.rios@ebah.edu.co')
            ->subject("Inscripción de $nombreDestinatario - Escuela de Bellas Artes y Humanidades del Departamento de Sucre")
            ->set_mailtype('html')







            ->message('
                
                
     <div>
    <div>


        <div style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0;padding:0">


            <center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#f2f2f2" style="background-color:#f2f2f2!important;border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td height="7" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;border-spacing:0;border-collapse:collapse;height:7px;font-size:1px;line-height:1px;background:#008b21;padding:0" bgcolor="#8dc23e">
                            </td>

                        </tr>


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                <table width="100%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">


                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                        <br>





                                        <tr height="2" style="height:2px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>


                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                            <td width="130" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <a href="https://www.ebah.edu.co/" style="color:#8dc23e;text-decoration:underline" target="_blank" data-saferedirecturl="">

                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/logo-ebah-sistema-academico.png" width="250" alt="Logo EBAH" style="float:none;display:block;border:0 none" align="none">
                                                                </a>
                                                            </td>


                                                        </tr>



                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr height="25" style="height:25px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>





                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;padding:0">
                                                <table width="96.67%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>



                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="76%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                                <p style="font-size:16px;line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">Hola, ' . $nombreDestinatario . '</p>

                                                                             
                                                                                <p style="font-size:16px;text-align: justify; line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">
                                                                             
                                                                           
                                                                           El proceso de inscripción ha <b>culminado</b>, lo invitamos a estar atento para el próximo periodo académico. 
                                                                                    
                                                                                    
                                                                                </p>

                                                                    
                                                                 

                                                                                
                                                                                
                                                                
                                                 
                                                 
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>



                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>







                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">
                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td height="45" style="height:45px;word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>



                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">

                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table class="m_-1662020414733504001responsive-table" border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                                        <tr height="35" style="height:35px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                      
                                                                                       <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                 <i>    Esta es una notificación automática, por favor no responda este mensaje. </i>

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                      
                                                                       
                                                                      
                                                                       
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                     Mantente conectado para recibir consejos, actualizaciones y recursos

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.facebook.com/BellasArtesEbah/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/facebook.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Facebook icon" align="none">
                                                                                </a>
                                                                            </td>
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://twitter.com/BellasArteSucre" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/twitter.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Twitter icon" align="none">
                                                                                </a>
                                                                            </td>

                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.instagram.com/bellasartesdesucre/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/instagram.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Linkedin icon" align="none" class="CToWUd">
                                                                                </a>
                                                                            </td>



                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="line-height:22px!important;font-size:12px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0 0 28px">
                                                                                    <a style="color:#aaaaaa;text-decoration:underline" href="h" target="_blank" data-saferedirecturl=""><a style="color:#aaaaaa!important;text-decoration:none">

                                                                                            Calle 19 No. 16A - 09 Barrio Ford, Sincelejo - Sucre <br>
                                                                                            Teléfono: 035 2823160 - 3164933505 <br>

                                                                                            Correo: info@ebah.edu.co



                                                                                        </a>


                                                                                </p>
                                                                            </td>
                                                                        </tr>


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </center>



        </div>
    </div>

</div>
                
                
                
                
                ');





        return  $this->email->send();

    }

    function error404(){



        $datos['titulo'] = "Página no encontrada!!!";
        $datos['contenido'] = "../errors/error_404";
        $this->load->view('inicio/plantilla', $datos);

    }


    function errroNoScript(){

        $datos['titulo'] = "No puede desactivar JavaScript";
        $this->load->view('inicio/inc/header', $datos);
        $this->load->view('errors/error_no_script');
        $this->load->view('inicio/inc/footer');


    }



}
