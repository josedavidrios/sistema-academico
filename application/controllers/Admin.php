<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{


    protected $login;
    protected $peridoActual;

    protected $matriculaAulaVirtual;

    function __construct()
    {
        parent::__construct();

        $this->login = $this->session->userdata('documento');
        $this->peridoActual = $this->session->userdata('periodo');

        $this->load->model("Admin_model", "adminModel");

      //  $this->load->model("Usuario_model", "usuarioModel");
      //  $this->matriculaAulaVirtual = $this->usuarioModel->consultarMatriculaAulaVirtual($this->login);

        $this->load->helper(["fecha", "encrypt", "mi"]);


        if (($this->session->userdata('tipo') == "SA") || ($this->session->userdata('tipo') == "A")) {


        } else {


            redirect(base_url());

        }

    }


    function exportarEstudiantesParaAulaVirtual()
    {


        $grupo = $this->input->post("grupo");
        $cursos = $this->input->post("cursos");


        $this->load->helper("mysql_to_excel_helper");
        $data = $this->adminModel->gruposMooble($grupo);


        $header = ["username", "firstname", "lastname", "email", "phone2"];


        for ($i = 0; $i < count($cursos); $i++) {

            $header[] = "course" . ($i + 1);

        }


        $est = [];

        foreach ($data as $dat) {

            for ($i = 0; $i < count($cursos); $i++) {


                $dat["course" . ($i + 1)] = $cursos[$i];

            }

            $est[] = $dat;
        }


        exportCSV($est, $header);


        exit();

    }


    function exportarEstudiantesParaAulaVirtual2()
    {


        $grupo = 178;
        $curso = "1000";


        $this->load->helper("mysql_to_excel_helper");
        $query = $this->adminModel->gruposMooble($grupo, $curso);


        to_excel($query);

    }


    function index()
    {


        $datos['css'] = array('');
        $datos['js'] = ['chartjs/Chart.min.js', 'sistema-academico/graficas.js'];

        $datos['titulo'] = "Inicio";

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['contenido'] = 'inicio/inicio';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
        $this->load->view("admin/plantilla", $datos);

    }


    function consultarMatriculasPorPeriodos()
    {


        $periodos = $this->adminModel->consultarMatriculasPorPeriodo();


        echo json_encode($periodos);


    }


    function vistaRegistrarHabilitacion()
    {


        $documentoEstudiante = $this->session->tempdata('documento-estudiante');


        if (!is_null($documentoEstudiante)) {

            $datos['notas'] = $this->adminModel->consultarAsignaturasMatriculadas($documentoEstudiante, null, true);
            $datos['estudiante'] = $this->adminModel->consultarEstudiante($documentoEstudiante)[0];


        } else {


            $datos['estudiante'] = ["documento" => "",
                "nombres" => "",
                "apellidos" => ""
            ];


        }


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-confirm/jquery-confirm.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/habilitarNotas.js');
        $datos['titulo'] = "Editar notas";
        $datos['contenido'] = 'notas/habilitar';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }


    function registrarHabilitacion()
    {


        $codigo = $this->input->post("codigo");

        $notaDefinitiva = $this->input->post("nota");
        $documentoEstudiante = $this->input->post("documento-estudiante");

        $notas = [
            "nota_definitiva" => $notaDefinitiva,
            "ultima_fecha_modificacion" => fecha_y_hora_actual(),
            "modificada_por" => $this->login,
            "observacion" => "HB"

        ];


        $aux = $this->adminModel->editarNotas($codigo, $notas);


        if ($aux > 0) {


            $this->session->set_tempdata('documento-estudiante', $documentoEstudiante, 300);

            redirect(base_url('admin/notas/habilitar'));

        }


    }


    function subirNotasMasivas()
    {


        $this->load->helper("arrays");


        $numeroDeNotasRegistradas = 0;


        $grupo = $this->input->post("grupo");
        $carga = $this->input->post("carga");
        $corte = $this->input->post("corte");
        $programa = $this->input->post("programa");


        $documentosEstudiantesCSV = [];

        $config = $this->adminModel->consultarConfiguracion();

        $estudiantes = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($carga, $corte, $grupo);


        if (count($estudiantes) > 0) {

            foreach ($estudiantes as $estudiante) {

                $documentosEstudiantesBD[] = [

                    "codigo_matricula" => $estudiante['codigo_matricula'],
                    "nota" => 1.1,
                    "documento" => $estudiante['documento']

                ];


                $nombresEstudiantesCSV[] = [

                    "nombres" => $estudiante['nombre'],
                    "documento" => $estudiante['documento']


                ];


            }

            $nomabreArchivo = $_FILES['archivo']['tmp_name'];
            $archivo = fopen($nomabreArchivo, "r");


            $j = 1;


            $primerFilaNotas = $config['fila_notas'];


            $todasLasNotasEn0 = 1;

            while (($datosNota = fgetcsv($archivo, 1000, ";")) !== FALSE) {


                if (count($datosNota) >= 4) {

                    /*
                     * Conficional para empezar a leer las notas desde la linea estipulada en el formato
                     *
                     * */

                    if ($j > $primerFilaNotas) {

                        $nota = round(str_replace(",", ".", $datosNota[($config['columna_notas'] - 1)]), 1);

                        if ($nota >= 0 && $nota <= 5) {

                            $documentosEstudiantesCSV[] = [

                                "codigo_matricula" => "1",
                                "nota" => $nota,
                                "documento" => $datosNota[1]

                            ];


                        } else {


                            /* Error 1
                             * Se presenta cuando existe una nota fuera del rango en el archivo CSV
                             * */


                            $mensaje = [

                                "codigo" => 1,
                                "linea" => count($documentosEstudiantesCSV) + 1

                            ];

                            $this->session->set_flashdata('error_subir_notas', $mensaje);
                            redirect(getenv('HTTP_REFERER'));


                            break;

                        }


                        if ($nota != 0) {

                            $todasLasNotasEn0 = 0;

                        }


                        /*
                         * Condicional para no leer mas lineas del CSV mayores al listado de estudiantes
                         *
                         * */

                        if (count($documentosEstudiantesBD) == count($documentosEstudiantesCSV)) {

                            break;
                        }


                    }

                } else {


                    $mensaje = [

                        "codigo" => 5

                    ];


                    /* Error 5
                    * Las columnas del documento están erradas
                    * */


                    $this->session->set_flashdata('error_subir_notas', $mensaje);
                    redirect(getenv('HTTP_REFERER'));


                    break;


                }

                $j++;

            }


            if ($todasLasNotasEn0 == 1) {


                /* Error 6
                * Todas las notas en 0 ó en blanco
                * */


                $mensaje = [

                    "codigo" => 6

                ];


                $this->session->set_flashdata('error_subir_notas', $mensaje);
                redirect(getenv('HTTP_REFERER'));

            }


            array_sort_by($documentosEstudiantesBD, 'documento', $order = SORT_ASC);
            array_sort_by($documentosEstudiantesCSV, 'documento', $order = SORT_ASC);
            array_sort_by($nombresEstudiantesCSV, 'documento', $order = SORT_ASC);


            $aux = false;

            if (count($documentosEstudiantesBD) == count($documentosEstudiantesCSV)) {


                $this->db->trans_start();

                $i = 0;

                for ($i; $i < count($documentosEstudiantesCSV); $i++) {

                    if (strcmp(trim($documentosEstudiantesCSV[$i]['documento']), $documentosEstudiantesBD[$i]['documento']) == 0) {

                        $datos = [

                            "registrada_por" => $this->login,
                            "fecha_registro" => fecha_y_hora_actual()

                        ];


                        if ($corte == 0) {

                            $datos["nota_definitiva"] = round($documentosEstudiantesCSV[$i]['nota'], 1);

                        } else {

                            $datos["nota" . $corte] = round($documentosEstudiantesCSV[$i]['nota'], 1);
                        }


                        $numeroDeNotasRegistradas += $this->adminModel->registrarNota($documentosEstudiantesBD[$i]['codigo_matricula'], $datos);


                        if ($corte == 3) {

                            $this->adminModel->calcularNotaDefinitiva($documentosEstudiantesBD[$i]['codigo_matricula']);
                        }


                        $aux = true;

                    } else {


                        $aux = false;
                        break;

                    }

                }

                if ($aux) {


                    $exito = $this->adminModel->cambiarEstadoEvaluacionPorCorte($carga, $corte);

                    $this->db->trans_complete();


                    if ($exito > 0) {


                        if ($corte == 0) {

                            redirect(base_url('admin/notas/sabatina/asignatura/' . $programa));
                        } else {

                            redirect(base_url('admin/notas/matinal/asignatura/' . $programa));
                        }


                    } else {


                        /* Error 2
                         * Se presenta cuando no se cambia el estado de la carga acádemica
                         * */


                        $mensaje = [

                            "codigo" => 2,

                        ];

                        $this->session->set_flashdata('error_subir_notas', $mensaje);
                        redirect(getenv('HTTP_REFERER'));


                    }


                } else {


                    /* Error 3
                     * Se presenta cuando existe un documento erronero en el archivo CSV
                     * */

                    $mensaje = [


                        "codigo" => 3,
                        "documento" => $nombresEstudiantesCSV[$i]['documento'],
                        "nombres" => utf8_encode($nombresEstudiantesCSV[$i]['nombres'])

                    ];

                    $this->session->set_flashdata('error_subir_notas', $mensaje);
                    redirect(getenv('HTTP_REFERER'));

                }

            } else {


                /* Error 4
                 * Se presenta cuando el número de los estudiantes matrículados es diferente al archivo CSV,
                 * la fila del encabezado de las nota esta definido en al base de datos y se almacena en variable primerFilaNotas
                 * */


                $mensaje = [


                    "codigo" => 4,
                    "matriculados" => count($documentosEstudiantesBD),
                    "csv" => count($documentosEstudiantesCSV),
                    "fila" => $primerFilaNotas

                ];

                $this->session->set_flashdata('error_subir_notas', $mensaje);


                /*

                echo count($documentosEstudiantesCSV)."<br>";

                echo count($documentosEstudiantesBD)."<br>";

                foreach ($documentosEstudiantesCSV as $nombres) {

                    echo $nombres['documento']."<br>";

                }


                */

                redirect(getenv('HTTP_REFERER'));

            }

        } else {

            //Error 5

            echo "No se han cargado los estudiantes";

        }


    }


    function matricularAcademicasMasivas()
    {


        $periodo = $this->input->post("periodo");

        $matriculas = $this->adminModel->consultarListadoMatriculas($periodo);
        $i = 0;


        $this->db->trans_start();

        foreach ($matriculas as $matricula) {

            $i++;


            $asignaturas = $this->adminModel->consultarAsignaturasSemestrales($matricula['programa'], $matricula['semestre'], $matricula['jornada']);


            $datosMatriculas = [
                "matricula_academica" => 1,
                "ultima_fecha_modificacion" => fecha_y_hora_actual()

            ];

            $this->adminModel->actualizar("matriculas", $matricula['codigo'], $datosMatriculas);


            foreach ($asignaturas as $asignatura) {

                $datos = [

                    "matricula" => $matricula['codigo'],
                    "asignatura_semestral" => $asignatura['codigo'],
                ];


                $codigoAsignaturaMatriculada = $this->adminModel->registrarOptenerUltimoID("asignaturas_matriculadas", $datos);


                $datosMatriculaAsignatura = [

                    "asignatura_matriculada" => $codigoAsignaturaMatriculada,
                    "fecha_registro" => fecha_y_hora_actual()

                ];


                $codigoNota = $this->adminModel->registrar("notas", $datosMatriculaAsignatura);


            }

        }


        $this->db->trans_complete();


        if ($this->db->trans_status()) {


            $datos = [

                "matriculas_academicas" => 1,

            ];


            $this->adminModel->registrarConfiguracion($datos);


            redirect(base_url('admin/matriculas/academicas/matricular'));

        } else {


            echo "Error";

        }


    }

    function vistaMatriculaAsignatura()
    {


        $periodo = $this->adminModel->consultarPeriodoMatriculasAbiertas();


        if (count($periodo) > 0) {



            $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');

            $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/MatriculaAsignatura.js');
            $datos['titulo'] = "Matrículas Primer Ingreso";

            $datos['contenido'] = 'matriculas/matricula_asignatura';



            $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;


            $datos['grupos'] = $this->adminModel->consultarGrupos($periodo[0]['codigo']);
            $datos['periodo'] = $periodo[0]['codigo'];
            $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
            $this->load->view("admin/plantilla", $datos);


        } else {

            $this->vistaMatriculasCerradas();

        }


    }

    function vistaMatriculaAsignaturaIndividual()
    {


        $periodo = $this->adminModel->consultarPeriodoMatriculasAbiertas();


        if (count($periodo) > 0) {


            $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');
            $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
            $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/matriculas.js');
            $datos['titulo'] = "Matrículas Primer Ingreso";

            $datos['contenido'] = 'matriculas/matricula_asignatura_individual';


            $datos['asignaturas'] = $this->adminModel->consultarAsignaturas();
            $datos['periodo'] = $periodo[0]['codigo'];
            $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
            $this->load->view("admin/plantilla", $datos);


        } else {

            $this->vistaMatriculasCerradas();

        }


    }


    function actualizarClavesDeUsuarios()
    {

        $this->load->helper("encrypt");
        $usuarios = $this->adminModel->consultarUsuariosSinClave();


        $i = 0;

        foreach ($usuarios as $usuario) {


            $datos = [

                "clave" => encrypt($usuario['documento']),
                "ultima_fecha_modificacion" => fecha_y_hora_actual()

            ];


            $i++;

            $this->adminModel->editarUsuario($usuario['documento'], $datos);


        }

        echo $i;

    }


    function vistaMatriculasAcademicas()
    {


        $datos['titulo'] = "Matrículas Académicas - Sistema Académico";


        $datos['contenido'] = '../admin/matriculas/matriculas_academicas';
        $datos['periodo'] = $this->peridoActual;

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['matriculados'] = $this->adminModel->consultarMatriculasPorPeriodo($this->peridoActual)[0]['cant'];

        $datos['sin_matricular'] = count($this->adminModel->consultarListadoMatriculas($this->peridoActual));

        $datos['grupos'] = count($this->adminModel->consultarGrupos($this->peridoActual));
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     

        $this->load->view("admin/plantilla", $datos);

    }


    function vistaSubirNotasMasivas($codigoAargarAcademica, $corteActual, $codigoPrograma, $grupo)
    {


        $estudiantes = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($codigoAargarAcademica, $corteActual, $grupo);


        $datos["carga"] = $codigoAargarAcademica;
        $datos["corte"] = $corteActual;
        $datos["programa"] = $codigoPrograma;
        $datos["grupo"] = $grupo;

        $datos["cantidad"] = count($estudiantes);
        $datos["estudiantes"] = $estudiantes;

        $datos['css'] = [''];
        $datos['js'] = [''];


        $datosCarga = $this->adminModel->consultarAsignaturaPorCargaAcademica($codigoAargarAcademica)[0];


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['asignatura'] = $datosCarga['nombre'];
        $datos['letra_grupo'] = $datosCarga['grupo'];
        $datos['jornada'] = $datosCarga['jornada'];
        $datos['titulo'] = "Digitacíon de Notas - Sistema Académico";


        $datos['contenido'] = '../admin/notas/subir_archivos';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }

    function x5()
    {


        echo var_dump($this->generarCorreoInstutucional("JOSE DAVID", "RIOS PACHECO"));

    }


    function cancelarMatriculaAulaVirtual($documento)
    {

        $cursos = $this->adminModel->consultarMatriculasCursosAulaVirtual($documento);


        foreach ($cursos as $curso) {

            $this->adminModel->cancelarMatriculaAulaVirtual($curso['id']);


        }


    }


    function filtrarMunicipios()
    {


        if (isset($_GET['term'])) {

            // $nombres = $_GET['term'];

            $nombre = strtolower($_GET['term']);
            $valores = $this->adminModel->filtrarMunicipios($nombre);


            echo json_encode($valores);


        }


    }


    function vistaGestionarDocente()
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css', 'jquery-confirm/jquery-confirm.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/docente.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js');
        $datos['titulo'] = "Docentes";

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['niveles'] = $this->adminModel->consultarNivelesEducacion();
        $datos['contenido'] = '../admin/docentes/gestionar';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }

    public function filtrarAsignatura()
    {


        $nombre = $this->input->post("nombre");

        if (!empty($nombre)) {

            $asiganturas = $this->adminModel->filtrarAsignatura(mb_strtoupper($nombre));

            foreach ($asiganturas as $asignatura) {

                echo '<tr>

                    <td>' . $asignatura['codigo'] . '</td>
                    <td>' . $asignatura['nombre'] . '</td>
                    <td>' . $asignatura['programa'] . '</td>
                    <td class="text-center">' . $asignatura['semestre'] . '</td>
                    <td>' . $asignatura['jornada'] . '</td>   
                     
                     <td class="text-center">' . formato_activo($asignatura['activa']) . '</td>
  
                </tr>';

            }
        } else {


            echo '<tr>

                            <td colspan="5">No existen coinicidencias </td>
                      </tr>';
        }

    }

    function vistaGestionarAsignaturas()
    {


        $datos['css'] = [];
        $datos['js'] = ['modalBootstrap.js', 'sistema-academico/asignatura.js'];
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['periodo'] = $this->adminModel->consultarPeriodos($this->peridoActual);
        $datos['programas'] = $this->adminModel->consultarTodosLosProgramas();
        $datos['titulo'] = "Asignaturas - Sistema Académico ";
        $datos['contenido'] = '../admin/asignaturas/gestionar';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }

    function vistaGestionarAsignaturasSemestrales()
    {


        $datos['css'] = ['select2/select2.min.css', 'select2/select2-bootstrap.css','jquery-confirm/jquery-confirm.css'];
        $datos['js'] = ['select2/select2.min.js', 'select2/es.js','modalBootstrap.js', 'sistema-academico/asignatura.js','jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js'];
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['programas'] = $this->adminModel->consultarTodosLosProgramas();
        $datos['asignaturas'] = $this->adminModel->consultarAsignatura();
        $datos['titulo'] = "Asignaturas Semestrales - Sistema Académico ";
        $datos['contenido'] = '../admin/asignaturas/semestrales';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }

    public function registrarAsignaturaSemestral()
    {
        $programa = $this->input->post('programa');
        $asignatura = $this->input->post('asignatura');
        $semestre = $this->input->post('semestre');
        $jornada= $this->input->post('jornada');


       $existe = $this->adminModel->consultarAsignaturaSemestral($programa,$semestre,$jornada, $asignatura);



       if (count($existe)==0){



           $datos=[

               "jornada"=>$jornada,
               "semestre"=>$semestre,
               "asignatura"=>$asignatura,
               "plan_de_estudio"=>$programa


           ];


        echo   $this->adminModel->registrar("asignaturas_semestrales",$datos);

       }else{

           $datos=[

               "activa"=>1
           ];


           echo   $this->adminModel->actualizar("asignaturas_semestrales",$existe[0]['codigo'],$datos);

       }





    }





    public function registrarAsignatura()
    {


        $operacion = $this->input->post('operacion');
        $codigo = $this->input->post('codigo');
        $nombre = mb_strtoupper($this->input->post('nombre'));
        $horas_semanales = $this->input->post('creditos');

        $abreviatura = $this->input->post('abreviatura');


        $datos = array(


            "nombre" => $nombre,
            "creditos" => $horas_semanales,
            "abreviatura" => $abreviatura,


        );

        if (strcmp($operacion, 'registrar') == 0) {


            $existe = $this->adminModel->consultarAsignaturaPorNombre($nombre);

            if (count($existe) == 0) {

                $this->adminModel->registrarAsignatura($datos);

            } else {

                echo -1;

            }


        } else {


            $editado = $this->adminModel->editarAsignatura($codigo, $datos);

            if ($editado > 0) {

                echo 2;

            } else {

                echo -2;
            }


        }

    }

    function vistaEstudiantes()
    {

        $documentoEstudiante = $this->session->tempdata('documento-estudiante');

        if (!is_null($documentoEstudiante)) {

            $datos['estudiante'] = $this->adminModel->consultarEstudiante($documentoEstudiante)[0];

        }


        $datos['niveles'] = $this->adminModel->consultarNivelesEducacion();
        $datos['estados_civiles'] = $this->adminModel->consultarEstadosCiviles();

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;


        $datos['css'] = ['select2/select2.min.css', 'select2/select2-bootstrap.css'];
        $datos['js'] = ['mostrarDatosEstudiante.js', 'modalBootstrap.js', 'filtrarMunicipios.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/estudiante.js'];

        $datos['titulo'] = "Estudiantes";
        $datos['contenido'] = '../admin/estudiantes/gestionar';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }


    public function registrarGrupo()
    {

        $codigo = $this->input->post('codigo');
        $programa = $this->input->post('programa');
        $semestre = $this->input->post('numero-semestre');
        $jornada = $this->input->post('jornada');
        $grupo = $this->input->post('grupo');
        $abreviatura = $this->input->post('abreviatura');

        $periodo = $this->input->post("periodo");


        $datosGrupos = [

            "codigo" => $codigo,
            "programa" => $programa,
            "semestre" => $semestre,
            "jornada" => $jornada,
            "periodo" => $periodo,
            "grupo" => $grupo,
            "abreviatura" => $abreviatura,
            "fecha_registro" => fecha_y_hora_actual()

        ];


        $existe = $this->adminModel->consultarGrupo($codigo);

        $registro = -1;

        if (count($existe) == 0) {


            $registro = $this->adminModel->crearGrupo($datosGrupos);
        }


        echo $registro;

    }


    function letrasGrupos($numero)
    {


        $abecedario = ["", "A", "B", "C", "D", "E", "F", "G", "H"];


        return $abecedario[$numero];
    }

    function consultarProximoNumeroDeGrupo()
    {


        $programa = $this->input->post("programa");
        $semestre = $this->input->post("semestre");
        $jornada = $this->input->post("jornada");


        if (isset($periodo) || isset($programa) || isset($jornada) || isset($semestre)) {


            echo $this->letrasGrupos($this->adminModel->consultarProximoNumeroDeGrupo($this->peridoActual, $programa, $semestre, $jornada));

        } else {

            echo "-1";

        }


    }


    public function vistaGestionarGrupos($programa = "dg")
    {

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['css'] = array('jquery-confirm/jquery-confirm.css');
        $datos['js'] = array('modalBootstrap.js', 'sistema-academico/grupo.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js');

        $datos['grupos'] = $this->adminModel->consultarGrupos($this->peridoActual, null, $programa);

        $datos['programas'] = $this->adminModel->consultarProgramas($programa);
        $datos['semestres'] = $this->adminModel->consultarSemestre();

        $datos['periodoActual'] = $this->peridoActual;
        $datos['titulo'] = "Grupos";
        $datos['contenido'] = '../admin/grupos/gestionar';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);
    }


    function matricularAsignatura()
    {



        $asignatura =    $this->input->post("asignatura");
        $grupo =    $this->input->post("grupo");


        $matriculas = $this->adminModel->consultarEstudiantesPorGrupo($grupo);



        $cont=0;

        foreach ($matriculas as $matricula) {

            $datos = [

                "matricula" => $matricula['codigo'],
                "asignatura_semestral" => $asignatura,
                "fecha_registro" => fecha_y_hora_actual()
            ];


            $codigoAsignaturaMatriculada = $this->adminModel->registrarOptenerUltimoID("asignaturas_matriculadas", $datos);

            $datosMatriculaAsignatura = [

                "asignatura_matriculada" => $codigoAsignaturaMatriculada,
                "fecha_registro" => fecha_y_hora_actual()

            ];


            $cont+=  $this->adminModel->registrar("notas", $datosMatriculaAsignatura);


          //  echo $cont;



        }




        if ($cont>0){

            $carga=  $this->adminModel->consultarCargasAcademicasPorAsignaturaSemestral($asignatura,$this->peridoActual);


            $this->registrarEvaluacionDocentePorGrupo($grupo,$carga[0]['codigo']);


            redirect(base_url('admin/matriculas/asignatura/grupal'));


        }


    }

    public function matricularAsignaturasIndividuales()
    {


        $this->load->helper(["mi_helper", "encrypt"]);

        $exito = 0;

        $documento = $this->input->post("documento");
        $asignatura = $this->input->post("asignatura");


        $existe = $this->adminModel->consultarMatriculasEstudiane($documento, $this->peridoActual);


        if (count($existe) > 0) {


            $datosMatricula = [


                "fecha_registro" => fecha_y_hora_actual(),
                "asignatura_semestral" => $asignatura,
                "matricula" => $existe[0]['codigo']


            ];


            $codigoAsignaturaMatriculada = $this->adminModel->registrarOptenerUltimoID("asignaturas_matriculadas", $datosMatricula);


            $datosMatriculaAsignatura = [

                "asignatura_matriculada" => $codigoAsignaturaMatriculada,
                "fecha_registro" => fecha_y_hora_actual()

            ];


            $exito = $this->adminModel->registrar("notas", $datosMatriculaAsignatura);


            if ($exito>0){




           $ex=     $this->regisstrarEvaluacionDocenteIndividual($asignatura);




            }


        } else {

            $exito = -1;

        }





        echo $exito;


    }


    public function matriculasPrimerIngreso()
    {


        $this->load->helper(["mi_helper", "encrypt"]);

        $exito = 0;

        $documento = $this->input->post("documento");
        $tipoDocumento = to_uppercase_and_trim($this->input->post("tipo-documento"));
        $nombres = to_uppercase_and_trim(remove_accents($this->input->post("nombres")));
        $apellidos = to_uppercase_and_trim(remove_accents($this->input->post("apellidos")));

        $fecha_nacimiento = $this->input->post("fecha-nacimiento");
        $correo = to_uppercase_and_trim($this->input->post("correo"));
        $celular = mb_strtoupper($this->input->post("celular"));

        $sexo = mb_strtoupper($this->input->post("sexo"));

        $grupo = $this->input->post("grupo");
        $lugarResidencia = $this->input->post("lugar-residencia");
        $lugarNacimiento = $this->input->post("lugar-nacimiento");
        $direccion = to_uppercase_and_trim($this->input->post("direccion"));
        $barrio = to_uppercase_and_trim($this->input->post("barrio"));
        $auxilioAcademico = $this->input->post("auxilio-academico");

        $nivelEducacion = $this->input->post("nivel-educacion");
        $estadoCivil = $this->input->post("estado-civil");
        $tipoSangre = to_uppercase_and_trim($this->input->post("tipo-sangre"));

        $zona = to_uppercase_and_trim($this->input->post("zona"));


        $observaciones = to_uppercase_and_trim($this->input->post("observaciones"));


        $periodo = $this->input->post("periodo");
        $estrato = $this->input->post("estrato");

        $eps = $this->input->post("eps");
        $titulo = to_uppercase_and_trim($this->input->post("titulo"));
        $lugarExpedicionDocumento = $this->input->post("lugar-expedicion-documento");

        $datosEstudiante = [

            "documento" => $documento,
            "clave" => encrypt($documento),
            "nombres" => $nombres,
            "apellidos" => $apellidos,
            "fecha_nacimiento" => $fecha_nacimiento,
            "tipo_documento" => $tipoDocumento,
            "correo" => $correo,
            "sexo" => $sexo,
            "direccion" => $direccion,
            "celular" => $celular,
            "lugar_residencia" => $lugarResidencia,
            "lugar_nacimiento" => $lugarNacimiento,
            "estado_civil" => $estadoCivil,
            "tipo" => "E",
            "barrio" => $barrio,
            "nivel_educacion" => $nivelEducacion,
            "tipo_sangre" => $tipoSangre,
            "zona" => $zona,
            "estrato" => $estrato,
            "titulo" => $titulo,
            "eps" => $eps,
            "lugar_expedicion_documento" => $lugarExpedicionDocumento,


        ];

        $datosMatricula = [

            "grupo" => $grupo,
            "estudiante" => $documento,
            "fecha_registro" => fecha_y_hora_actual(),
            "auxilio_academico" => $auxilioAcademico,
            "observaciones" => $observaciones,
            "usuario" => $this->login,

        ];

        $datosEstudianteAulaVirtual = [


            "username" => $documento,
            "firstname" => $nombres,
            "lastname" => $apellidos,
            "email" => $correo,
            "phone2" => $celular,
            "confirmed" => 1,
            "mnethostid" => 1,
            "password" => password_moodle($documento)


        ];

        $existe = $this->adminModel->consultarMatriculaPorGrupo($documento, $grupo);


        if (count($existe) == 0) {


            $estudiante = $this->adminModel->consultarEstudiante($documento);


            $this->db->trans_start();


            if (count($estudiante) == 0) {


                $datosEstudiante["fecha_registro"] = fecha_y_hora_actual();


                $aux = $this->adminModel->registrar("usuarios", $datosEstudiante);


                if (count($this->adminModel->consultarUsuariosAulaVirtual($documento)) == 0) {


                    $aux = $this->adminModel->registearDatosAulaVirtual($datosEstudianteAulaVirtual);

                }


            } else {

                $datosEstudiante["ultima_fecha_modificacion"] = fecha_y_hora_actual();


                unset($datosEstudiante['documento']);

                $this->adminModel->actualizarUsuario($documento, $datosEstudiante);

                $this->adminModel->actualizarDatosAulaVirtual($documento, $datosEstudianteAulaVirtual);


            }


            $configuracion = $this->adminModel->consultarConfiguracion();

            if ($configuracion['matriculas_academicas'] == "1") {


                $datosMatricula["matricula_academica"] = 1;

                $codigoMatricula = $this->adminModel->registrarOptenerUltimoID("matriculas", $datosMatricula);
                $this->adminModel->cambiarEstadoIncripcion($documento, $periodo);
                $datosGrupo = $this->adminModel->consultarGrupo($grupo)[0];


                $asignaturasPorMatricular = $this->adminModel->consultarAsignaturasSemestrales($datosGrupo['codigo_programa'], $datosGrupo['semestre'], $datosGrupo['codigo_jornada']);


                foreach ($asignaturasPorMatricular as $asignatura) {

                    $datosMatriculaAsignatura = [

                        "matricula" => $codigoMatricula,
                        "asignatura_semestral" => $asignatura['codigo'],
                        "fecha_registro" => fecha_y_hora_actual(),


                    ];


                    $codigoAsignaturaMatriculada = $this->adminModel->registrarOptenerUltimoID("asignaturas_matriculadas", $datosMatriculaAsignatura);

                    $datosMatriculaAsignatura = [

                        "asignatura_matriculada" => $codigoAsignaturaMatriculada,
                        "fecha_registro" => fecha_y_hora_actual()

                    ];


                    $codigoNota = $this->adminModel->registrar("notas", $datosMatriculaAsignatura);


                }

            } else {


                $codigoMatricula = $this->adminModel->registrarOptenerUltimoID("matriculas", $datosMatricula);
                $this->adminModel->cambiarEstadoIncripcion($documento, $periodo);


            }


            $this->db->trans_complete();


            $exito = 1;


            if (!is_null($correo) && strlen($correo) > 1 && !isLocalHost()) {

                $programa = $this->adminModel->consultarDetallesDeGrupo($grupo)[0];
                $this->enviarCorreoMatriculaPrimerIngreso($correo, ucwords(strtolower($nombres . " " . $apellidos)), $programa['programa'], $programa['jornada']);

            }


        } else {

            $exito = -1;

        }

        echo $exito;


    }


    public function actualizarDatosUsuarios()
    {


        $documento = $this->input->post("documento");
        $nuevoDocumento = $this->input->post("nuevo-documento");
        $tipoDocumento = to_uppercase_and_trim($this->input->post("tipo-documento"));
        $nombres = to_uppercase_and_trim(remove_accents($this->input->post("nombres")));
        $apellidos = to_uppercase_and_trim(remove_accents($this->input->post("apellidos")));

        $fecha_nacimiento = $this->input->post("fecha-nacimiento");
        $correo = to_uppercase_and_trim($this->input->post("correo"));
        $celular = mb_strtoupper($this->input->post("celular"));

        $sexo = mb_strtoupper($this->input->post("sexo"));


        $lugarResidencia = $this->input->post("lugar-residencia");
        $lugarNacimiento = $this->input->post("lugar-nacimiento");
        $direccion = to_uppercase_and_trim($this->input->post("direccion"));
        $barrio = to_uppercase_and_trim($this->input->post("barrio"));


        $nivelEducacion = $this->input->post("nivel-educacion");
        $estadoCivil = $this->input->post("estado-civil");
        $tipoSangre = to_uppercase_and_trim($this->input->post("tipo-sangre"));

        $zona = to_uppercase_and_trim($this->input->post("zona"));


        $cambio_clave = $this->input->post("cambio-clave");

        $estrato = $this->input->post("estrato");

        $eps = $this->input->post("eps");
        $titulo = to_uppercase_and_trim($this->input->post("titulo"));
        $lugarExpedicionDocumento = $this->input->post("lugar-expedicion-documento");

        $datosEstudiante = [

            "documento" => $nuevoDocumento,
            "nombres" => $nombres,
            "apellidos" => $apellidos,
            "fecha_nacimiento" => $fecha_nacimiento,
            "tipo_documento" => $tipoDocumento,
            "correo" => $correo,
            "sexo" => $sexo,
            "direccion" => $direccion,
            "celular" => $celular,
            "lugar_residencia" => $lugarResidencia,
            "lugar_nacimiento" => $lugarNacimiento,
            "estado_civil" => $estadoCivil,
            "barrio" => $barrio,
            "nivel_educacion" => $nivelEducacion,
            "tipo_sangre" => $tipoSangre,
            "zona" => $zona,
            "estrato" => $estrato,
            "titulo" => $titulo,
            "eps" => $eps,
            "lugar_expedicion_documento" => $lugarExpedicionDocumento,


        ];


        $datosEstudianteAulaVirtual = [


            "username" => $nuevoDocumento,
            "firstname" => $nombres,
            "lastname" => $apellidos,
            "email" => $correo,
            "phone2" => $celular


        ];


        if ($cambio_clave == 0 && ($documento != $nuevoDocumento)) {

            $datosEstudiante['clave'] = encrypt($nuevoDocumento);
            $datosEstudianteAulaVirtual['password'] = password_moodle($nuevoDocumento);


        }


        $this->adminModel->actualizarUsuario($documento, $datosEstudiante);

        $this->usuarioModel->actualizarDatosMoodle($documento, $datosEstudianteAulaVirtual);


        $this->session->set_tempdata('documento-estudiante', $nuevoDocumento, 300);
        redirect(base_url("admin/estudiante"));


    }


    public function matriculaEstudiantesAntiguos()
    {

        $documento_estudiante = $this->input->post("documento");
        $codigo_grupo = $this->input->post("grupo");
        $auxilio = $this->input->post("auxilio-academico");
        $jornada = $this->input->post("jornada");


        $semestre = $this->input->post("semestre");


        $jornada = substr($jornada, 0, 1);

        $codigoPrograma = $this->input->post("codigo-programa");

        $datosMatricula = [

            "estudiante" => $documento_estudiante,
            "auxilio_academico" => $auxilio,
            "grupo" => $codigo_grupo,
            "usuario" => $this->login,
            "fecha_registro" => fecha_y_hora_actual(),


        ];

        $existe = $this->adminModel->consultarMatriculaPorGrupo($documento_estudiante, $codigo_grupo);


        if (count($existe) == 0) {


            $configuracion = $this->adminModel->consultarConfiguracion();

            if ($configuracion['matriculas_academicas'] == "1") {

                $datosMatricula['matricula_academica'] = 1;
            }


            $codigoMatricula = $this->adminModel->registrarOptenerUltimoID("matriculas", $datosMatricula);

            $s = 0;

            if ($configuracion['matriculas_academicas'] == "1") {

                $asignaturasPorMatricular = $this->adminModel->consultarAsignaturasSemestrales($codigoPrograma, $semestre, $jornada);

                foreach ($asignaturasPorMatricular as $asignatura) {

                    $datosMatriculaAsignatura = [

                        "matricula" => $codigoMatricula,
                        "asignatura_semestral" => $asignatura['codigo'],
                        "fecha_registro" => fecha_y_hora_actual(),


                    ];


                    $codigoAsignaturaMatriculada = $this->adminModel->registrarOptenerUltimoID("asignaturas_matriculadas", $datosMatriculaAsignatura);

                    $datosMatriculaAsignatura = [

                        "asignatura_matriculada" => $codigoAsignaturaMatriculada,
                        "fecha_registro" => fecha_y_hora_actual()

                    ];


                    $codigoNota = $this->adminModel->registrar("notas", $datosMatriculaAsignatura);


                    $s++;


                }


            }


            echo $s;


        } else {

            echo -1;

        }
    }


    function consultarAsignaturasPorGrupos()
    {

        $programa = $this->input->post("programa");
        $semestre = $this->input->post("semestre");
        $jornada = $this->input->post("jornada");




        $asignturas = $this->adminModel->consultarAsignaturasSemestrales($programa, $semestre, $jornada);


        echo json_encode($asignturas);


    }


    function consultarAsignaturasSemestrales()
    {


        $programa = $this->input->post('programa');
        $jornada = $this->input->post('jornada');
        $semestre = $this->input->post('semestre');

        $asignaturasSemestrales = $this->adminModel->consultarAsignaturasSemestrales($programa, $semestre, $jornada);


        $i=1;

        foreach ($asignaturasSemestrales as $asignaturas) {



            echo '<tr>


                    <td>'.$i.'</td>
                      
                      <td>'.$asignaturas['codigo'].'</td>
        
                     <td>'.$asignaturas['asignatura'].'</td>
                     <td class="text-center">
                      
                       <a href="javascript:seleccionarAsignaturaSemestral(' . $asignaturas['codigo'] . ')" class="fa fa-trash"></a>
                       
                      </td>
             
           

           
                </tr>';


            $i++;
        }
    }


    function removerAsignatura(){

      $codigo = $this->input->post('codigo');



        $datos=[

            "activa"=>0
        ];


        echo   $this->adminModel->actualizar("asignaturas_semestrales",$codigo,$datos);



    }

    function removerCargaAcademica(){

        $codigo = $this->input->post('codigo');



        $datos=[

            "activa"=>0
        ];


        echo   $this->adminModel->actualizar("cargas_academicas",$codigo,$datos);



    }

    function c3($asignatura)
    {


        $carga=  $this->adminModel->consultarCargasAcademicasPorAsignaturaSemestral($asignatura,$this->peridoActual);



        $matriculas = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($carga[0]['codigo']);




        echo var_dump($matriculas);
    }

    function vistaNuevaMatricula()
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['grados'] = $this->adminModel->consultarGrados();
        $datos['niveles'] = $this->adminModel->consultarNivelesEducacion();
        $datos['programas'] = $this->adminModel->consultarTodosLosProgramas();
        $datos['semestres'] = $this->adminModel->consultarSemestre();
        $datos['jornadas'] = $this->adminModel->consultarJornadas();

        $this->load->view("admin/matriculas/nueva_matricula", $datos);

    }


    public function registrarDocente()
    {

        $operacion = $this->input->post("operacion");


        $this->load->helper(["mi_helper", "encrypt"]);


        $documento = $this->input->post("documento");
        $tipoDocumento = to_uppercase_and_trim($this->input->post("tipo-documento"));
        $nombres = to_uppercase_and_trim($this->input->post("nombres"));
        $apellidos = to_uppercase_and_trim($this->input->post("apellidos"));

        $fechaNacimiento = $this->input->post("fecha-nacimiento");

        $correo = to_uppercase_and_trim($this->input->post("correo"));
        $celular = mb_strtoupper($this->input->post("celular"));

        $sexo = mb_strtoupper($this->input->post("sexo"));


        $lugarResidencia = $this->input->post("lugar-residencia");
        $direccion = to_uppercase_and_trim($this->input->post("direccion"));
        $barrio = to_uppercase_and_trim($this->input->post("barrio"));

        $nivelEducacion = $this->input->post("nivel-educacion");





        $correoInstitucional=$this->generarCorreoInstutucional($nombres,$apellidos);


        $datos = [

            "tipo_documento" => $tipoDocumento,
            "documento" => $documento,
            "nombres" => $nombres,
            "apellidos" => $apellidos,
            "fecha_nacimiento" => $fechaNacimiento,
            "correo" => $correo,
            "sexo" => $sexo,
            "direccion" => $direccion,
            "barrio" => $barrio,
            "lugar_residencia" => $lugarResidencia,
            "tipo" => "D",
            "nivel_educacion" => $nivelEducacion,
            "celular" => $celular,
            "correo_institucional" => $correoInstitucional

        ];



        $datosEstudianteAulaVirtual=[


            "username"=>$documento,
            "firstname"=>$nombres,
            "lastname"=>$apellidos,
            "email"=>$correoInstitucional,
            "phone2"=>$celular,
            "confirmed"=>1,
            "mnethostid"=>1,
            "password"=>password_moodle($documento)


        ];





        if (strcmp($operacion, 'registrar') == 0) {


            $datos['clave'] = encrypt($documento);


            $existe = $this->adminModel->consultarDocente($documento);

            if (count($existe) == 0) {

                $datos['fecha_registro'] = fecha_y_hora_actual();






                $registro = $this->adminModel->registrarDocente($datos);
                $aux = $this->adminModel->registearDatosAulaVirtual($datosEstudianteAulaVirtual);

                $this->enviarCorreoCreacionCorreoInstutucional($documento,$nombres." ".$apellidos,$correoInstitucional);

                echo $registro;

            } else {

                echo -1;

            }


        } else if (strcmp($operacion, 'editar') == 0) {


            $datos['ultima_fecha_modificacion'] = fecha_y_hora_actual();

            $editado = $this->adminModel->editarPorDocumento("usuarios", $documento, $datos);

            if ($editado > 0) {

                echo 2;

            } else {

                echo -2;
            }

        }


    }


    function generarCorreoInstutucional($nombres,$apellidos){


        $_nombres=explode(" ",$nombres);
        $_apellidos=explode(" ",$apellidos);


        $_nombres=  $_nombres[0];

        $_apellido1=$_apellidos[0];
        $_apellido2=$_apellidos[1];

        $correo = $_nombres.".".$_apellido1."@EBAH.EDU.CO";


        $cant=count($this->adminModel->validarCorreoInstitucional($correo)) ;

        if ($cant>0){

            $correo = $_nombres.".".$_apellido1.substr($_apellido2,0,$cant)."@EBAH.EDU.CO";

        }

        return $correo;

    }

    function consultarNotas()
    {

        $codigo = $this->input->post("codigo");

        $notas = $this->adminModel->consultarNotas($codigo);

        echo json_encode($notas);

    }


    function regisstrarEvaluacionDocenteIndividual($asignatura){


        $carga=  $this->adminModel->consultarCargasAcademicasPorAsignaturaSemestral($asignatura,$this->peridoActual);
        $matriculas = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($carga[0]['codigo']);



        $cont=0;



        $this->db->trans_start();




        foreach ( $matriculas as $matricula) {




            $asignaturasMatriculadas = $this->adminModel->consultarDocentesAsignados($matricula['matricula'],null,$carga[0]['codigo']);



            foreach ($asignaturasMatriculadas as $asignaturasMatriculada) {

                $evaluaciones = $this->adminModel->consultarEvaluacionesDocentes($matricula['matricula']);



                foreach ($evaluaciones as $evaluacion) {


                    $datos = [

                        "evaluacion_docente_por_matricula" => $evaluacion['codigo'],
                        "carga_academica" => $asignaturasMatriculada['codigo'],
                        "fecha_registro" => fecha_y_hora_actual()


                    ];



                    $cont+=     $this->adminModel->registrar("evaluaciones_docentes_por_asignaturas", $datos);


                }



            }

        }


        $this->db->trans_complete();

        return $cont;
    }

    function c2($grupo,$cargaAcademica){


        $carga=  $this->adminModel->consultarCargasAcademicasPorAsignaturaSemestral($asignatura,$this->peridoActual);



        $matriculas = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($carga[0]['codigo']);




        $cont=0;
        $matriculas = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($cargaAcademica, 0, $grupo);




        $this->db->trans_start();


        echo "ESTUDIANTES MATRICULADOS:".count($matriculas) ;

        foreach ( $matriculas as $matricula) {




            $asignaturasMatriculadas = $this->adminModel->consultarDocentesAsignados($matricula['matricula'],0,$cargaAcademica);

            echo var_dump($asignaturasMatriculadas)."<br>";

            foreach ($asignaturasMatriculadas as $asignaturasMatriculada) {

                $evaluaciones = $this->adminModel->consultarEvaluacionesDocentes($matricula['matricula']);



                foreach ($evaluaciones as $evaluacion) {


                    $datos = [

                        "evaluacion_docente_por_matricula" => $evaluacion['codigo'],
                        "carga_academica" => $asignaturasMatriculada['codigo'],
                        "fecha_registro" => fecha_y_hora_actual()


                    ];



                    $cont+=     $this->adminModel->registrar("evaluaciones_docentes_por_asignaturas", $datos);


                }



            }

        }

        if ($cont>0){

            $this->adminModel->cambiarEstadoEvaluacionCargaAcademica($cargaAcademica);
        }



        echo $cont;

        $this->db->trans_complete();
    }

    function registrarEvaluacionDocentePorGrupo($grupo, $cargaAcademica)
    {



        $cont=0;


      $matriculas =   $this->adminModel->consultarEstudiantesPorGrupo($grupo);




        $this->db->trans_start();
        foreach ( $matriculas as $matricula) {


            $asignaturasMatriculadas = $this->adminModel->consultarDocentesAsignados($matricula['codigo'],0,$cargaAcademica);


            foreach ($asignaturasMatriculadas as $asignaturasMatriculada) {



            //    echo var_dump($asignaturasMatriculada);

                $evaluaciones = $this->adminModel->consultarEvaluacionesDocentes($matricula['codigo']);


                foreach ($evaluaciones as $evaluacion) {


                    $datos = [

                        "evaluacion_docente_por_matricula" => $evaluacion['codigo'],
                        "carga_academica" => $asignaturasMatriculada['codigo'],
                        "fecha_registro" => fecha_y_hora_actual()


                    ];

               $cont+=     $this->adminModel->registrar("evaluaciones_docentes_por_asignaturas", $datos);


                }





            }

     }

        $this->adminModel->cambiarEstadoEvaluacionCargaAcademica($cargaAcademica);

        $this->db->trans_complete();

        return $cont;

    }


    function filtrarEstudianteParaMatricula()
    {


        $nombres = $this->input->post("nombres");


        if (!empty($nombres)) {

            $estudiantes = $this->adminModel->filtrarEstudiante(mb_strtoupper($nombres));

            foreach ($estudiantes as $estudiante) {


                $nombresEstudiante = "'" . $estudiante['apellidos'] . " " . $estudiante['nombres'] . "'";


                echo '<tr>

                    <td width="100"> <a  href="javascript:seleccionarEstudiante(' . $estudiante['documento'] . ',' . $nombresEstudiante . ')"> ' . $estudiante['documento'] . ' </a></td>
                    
                    <td> <a  href="javascript:seleccionarEstudiante(' . $estudiante['documento'] . ',' . $nombresEstudiante . ')">' . $estudiante['apellidos'] . " " . $estudiante['nombres'] . ' </a></td>
                    
                    
                  
           
                </tr>';

            }
        } else {


            echo '<tr>

                            <td colspan="5">No existen coinicidencias </td>
                      </tr>';
        }


    }


    function filtrarEstudianteMatriculados()
    {


        $nombres = $this->input->post("nombres");


        if (!empty($nombres)) {

            $estudiantes = $this->adminModel->filtrarEstudiante(mb_strtoupper($nombres));

            foreach ($estudiantes as $estudiante) {


                $nombresEstudiante = "'" . $estudiante['apellidos'] . " " . $estudiante['nombres'] . "'";


                echo '<tr>

                    <td width="100"> <a  href="javascript:seleccionarEstudianteMatriculas(' . $estudiante['documento'] . ',' . $nombresEstudiante . ')"> ' . $estudiante['documento'] . ' </a></td>
                    
                    <td> <a  href="javascript:seleccionarEstudianteMatriculas(' . $estudiante['documento'] . ',' . $nombresEstudiante . ')">' . $estudiante['apellidos'] . " " . $estudiante['nombres'] . ' </a></td>
                    
                    
                  
           
                </tr>';

            }
        } else {


            echo '<tr>

                            <td colspan="5">No existen coinicidencias </td>
                      </tr>';
        }


    }


    function filtrarEstudiante()
    {


        $nombres = $this->input->post("nombres");


        if (!empty($nombres)) {

            $estudiantes = $this->adminModel->filtrarEstudiante(mb_strtoupper($nombres));


            foreach ($estudiantes as $estudiante) {

                echo '<tr>

                    <td width="50">' . $estudiante['documento'] . '</td>
                    <td>' . $estudiante['apellidos'] . ' ' . $estudiante['nombres'] . '</td>
                    <td>' . $estudiante['celular'] . '</td>
                    <td>' . $estudiante['correo'] . '</td>
                    
                    <td class="text-center">

                          <a title="Editar" href="javascript:abrirModalEditarEstudiante(' . $estudiante['documento'] . ')" class="fa fa-edit"></a>
                    
                        <a title="Restablecer Clave" href="javascript:abrirModalRestablecerClave(' . $estudiante['documento'] . ')" class="fa fa-key"></a>
                     
                     </td>
      
                </tr>';

            }
        } else {


            echo '<tr>

                            <td colspan="5">No existen coinicidencias </td>
                      </tr>';
        }


    }


    function filtrarUsuarios()
    {





        if (isset($_GET['q'])) {

            $nombres = strtolower($_GET['q']);

            $docentes = $this->adminModel->filtrarUsuarios(mb_strtoupper($nombres), 1);

            echo json_encode($docentes);

        }
    }


    function filtrarDocente2()
    {


        $tipo_usuario = $this->session->userdata('tipo');


        if (isset($_GET['q'])) {

            $nombres = strtolower($_GET['q']);

            $docentes = $this->adminModel->filtrarDocente(mb_strtoupper($nombres), 1, $tipo_usuario);

            echo json_encode($docentes);

        }
    }

    function filtrarInstitucion()
    {


        if (isset($_GET['nombre'])) {

            $nombre = strtolower($_GET['nombre']);

            $institucion = $this->adminModel->filtrarInstitucion(mb_strtoupper($nombre));

            echo json_encode($institucion);

        }
    }

    function filtrarDocente()
    {


        $nombres = $this->input->post("nombres");

        $tipo_usuario = $this->session->userdata('tipo');

        if (!empty($nombres)) {

            $docentes = $this->adminModel->filtrarDocente(mb_strtoupper($nombres), null, $tipo_usuario);

            foreach ($docentes as $docente) {

                echo '<tr>

                    <td>' . $docente['documento'] . '</td>
                    <td>' . $docente['nombres'] . '</td>  
                    <td>' . $docente['correo_institucional'] . '</td>  
                    <td>' . $docente['celular'] . '</td>  
                    <td class="text-center"> ' . formato_activo($docente['activo']) . '</td>
                    
                    <td class="text-center">

                        <a title="Restablecer Clave" href="javascript:abrirModalRestablecerClave(' . $docente['documento'] . ')" class="fa fa-key"></a>
                    
                     </td>
      
                    
               
                    
                    
                </tr>';

            }
        } else {


            echo '<tr>

                            <td colspan="6">No existen coinicidencias </td>
                      </tr>';
        }


    }

    function filtrarDocenteCargasAcademicas()
    {


        $nombres = $this->input->post("nombres");
        $tipo_usuario = $this->session->userdata('tipo');

        if (!empty($nombres)) {

            $docentes = $this->adminModel->filtrarDocente(mb_strtoupper($nombres), 1, $tipo_usuario);

            foreach ($docentes as $docente) {


                $nombres = "'" . $docente['nombres'] . "'";

                echo '<tr>

                    <td>  <a href="javascript:seleccionarDocenteCargarAcademica(' . $docente['documento'] . ',' . $nombres . ')" >' . $docente['documento'] . '</a> </td>
                    <td> <a href="javascript:seleccionarDocenteCargarAcademica(' . $docente['documento'] . ',' . $nombres . ')" >' . $docente['nombres'] . '</a></td>
               
                </tr>';

            }
        } else {


            echo '<tr>

                            <td colspan="5">No existen coinicidencias </td>
                      </tr>';
        }


    }


    public function vistaListadoDeInscripciones()
    {


        $this->load->helper("mi");

        $datos['css'] = array('dataTables.bootstrap.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'estudiante.js', 'datatables/jquery.dataTables.min.js', 'datatables/dataTables.bootstrap.min.js', 'datatables/dataTables.responsive.min.js');

        $periodo = $this->adminModel->consultarPeriodoMatriculasAbiertas()[0]['codigo'];

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['inscripciones'] = $this->adminModel->consultarInscripciones($periodo);


        $datos['titulo'] = "Listado de inscripciones";
        $datos['contenido'] = '../admin/inscripciones/listado';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }

    public function consultarInscripciones()
    {


        $programa = $this->input->post('programa');
        $periodo = $this->input->post('periodo');

        $inscripcionees = $this->adminModel->consultarInscripciones($periodo, $programa);

        echo '<table id="datatable-matriculas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%"  >
                                <thead>
                                <tr>

                                    <th width="20">Período</th>
                                    <th width="250">Programa</th>
                                     <th >Estudiante</th>
                                 



                                </tr>
                                </thead>


                             <tbody>




';

        foreach ($inscripcionees as $inscripcion) {

            echo '  <tr>
                                    
                                                     
                                                <td class="mayus" > ' . $inscripcion['periodo'] . '</td >
                                                <td class="mayus" > ' . $inscripcion['programa'] . '</td >
                                                <td class="mayus" > ' . $inscripcion['estudiante'] . '</td >
                                                
                                       


                </tr >';


        }


        echo '

                                </tbody>

                            </table>


        ';


    }


    function consultarMatricula($numero)
    {


        $datos['matricula'] = $this->adminModel->consultarMatricula($numero)[0];


        $this->load->view("admin/matriculas/imprimir_matricula", $datos);


    }

    function consultarMatricula2($numero)
    {


        $matricula = $this->adminModel->consultarMatricula($numero);


        echo json_encode($matricula);


    }

    public function consultarMatriculas()
    {


        $programa = $this->input->post('programa');
        $periodo = $this->input->post('periodo');
        $semestre = $this->input->post('semestre');
        $jornada = $this->input->post('jornada');

        $matriculas = $this->adminModel->consultarMatriculas($periodo, $programa, $semestre, $jornada);


        foreach ($matriculas as $matricula) {

            echo '  <tr>
                                    
                                                     
                                                <td class="mayus" > ' . $matricula['periodo'] . '</td >
                                                <td class="mayus" > ' . $matricula['programa'] . '</td >
                                                <td class="mayus" > ' . $matricula['estudiante'] . '</td >
                                                  <td class="mayus text-center" > ' . $matricula['semestre'] . '</td >
                                                    <td class="mayus text-center" > ' . $matricula['jornada'] . '</td >
                                                
                                             <td class="mayus text-center" > ' . $matricula['grupo'] . '</td >
                                              


                </tr >';


        }


    }


    function vistaConsultarNotas()
    {

        $datos['css'] = array('jquery-confirm/jquery-confirm.css');

        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/consultarNotas.js');
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['titulo'] = "Sistema Académico - Consultar Notas";
        $datos['contenido'] = 'notas/consultar';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }


    function vistaMatriculas($codigoPeriodo = null)
    {


        if (is_null($codigoPeriodo)) {


            $periodo = $this->adminModel->consultarPeriodoMatriculasAbiertas();


        } else {


            $periodo[0] = ["codigo" => $codigoPeriodo];

        }


        if (count($periodo) > 0) {


            $datos['grupos'] = $this->adminModel->consultarGrupos($periodo[0]['codigo']);

            $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
            $datos['periodo'] = $periodo[0]['codigo'];
            $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');
            $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/matriculas.js');
            $datos['titulo'] = "Sistema Académico - Consultar Matrículas";
            $datos['contenido'] = 'matriculas/matricula_estudiantes_antiguos';
            $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
            $this->load->view("admin/plantilla", $datos);


        } else {


            $this->vistaMatriculasCerradas();


        }


    }

    function vistaMatriculasPrimerIngreso()
    {


        $periodo = $this->adminModel->consultarPeriodoMatriculasAbiertas();


        if (count($periodo) > 0) {

            $datos['programas'] = $this->adminModel->consultarTodosLosProgramas();

            $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');

            $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/matriculas.js');
            $datos['titulo'] = "Matrículas Primer Ingreso";

            $datos['contenido'] = 'matriculas/matricula_estudiantes_primer_ingreso';

            $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
            $datos['niveles'] = $this->adminModel->consultarNivelesEducacion();
            $datos['estados_civiles'] = $this->adminModel->consultarEstadosCiviles();


            $datos['grupos'] = $this->adminModel->consultarGrupos($periodo[0]['codigo']);
            $datos['periodo'] = $periodo[0]['codigo'];
            $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
            $this->load->view("admin/plantilla", $datos);


        } else {

            $this->vistaMatriculasCerradas();

        }


    }


    function vistaConsultarMatricula()
    {

        $documentoEstudiante = $this->session->tempdata('documento-estudiante');


        if (!is_null($documentoEstudiante)) {


            $datos["matriculas"] = $this->adminModel->consultarProgramasMatriculados($documentoEstudiante);
            $datos['estudiante'] = $this->adminModel->consultarEstudiante($documentoEstudiante)[0];

        } else {


            $datos['estudiante'] = ["documento" => "",
                "nombres" => "",
                "apellidos" => ""
            ];


        }


        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/matriculas.js');
        $datos['titulo'] = "Sistema Académico - Consultar Matrículas";
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $periodoMatriculas = $this->adminModel->consultarPeriodoMatriculasAbiertas()[0]['codigo'];


        $datos['grupos'] = $this->adminModel->consultarGrupos($periodoMatriculas);
        $datos['contenido'] = 'matriculas/consultar';
        $this->load->view("admin/plantilla", $datos);


    }


    function visualizarProgramasMatriculados()
    {

        $documento = $this->input->post("documento");


        $matriculas = $this->adminModel->consultarProgramasMatriculados($documento);


        foreach ($matriculas as $matricula) {


            $codigo = $matricula['codigo_matricula'];


            echo '<tr>

                       <td>' . $matricula['nombre'] . " " . $matricula['semestre'] . " - " . $matricula['grupo'] . '</td>
                

                    <td >' . $matricula['nombre_jornada'] . '</td>
          
                    <td class="text-center">' . $matricula['periodo'] . '</td>
                     <td class="text-center">' . substr($matricula['fecha'],0,10)  . '</td>
                    <td id="' . $codigo . '"  title="'.$matricula['activa'].'"  class="text-center">' . formato_estado_matricula($matricula['activa']) . '</td>
                    <td class="text-center"> 
                        <a href="javascript:cancelarMatricula(' . $codigo . ')" class="fa fa-trash"></a>
                        <a href="javascript:editarMatricula(' . $codigo . ')" class="fa fa-pencil"></a>
                    </td>
                </tr>';


        }

    }


    function editarMatricula()
    {


        $codigo = $this->input->post("codigo");
        $grupo = $this->input->post("grupo");

        $documentoEstudiante = $this->input->post("documento-estudiante");


        $configuracion = $this->adminModel->consultarConfiguracion();


        $matricula = $this->adminModel->consultarMatricula($codigo)[0];


        if ($configuracion['matriculas_academicas'] == "1") {

            $this->adminModel->cancelarMatricula($matricula['codigo']);

            $datosMatricula = [

                "estudiante" => $matricula['estudiante'],
                "auxilio_academico" => $matricula['auxilio_academico'],
                "grupo" => $grupo,
                "usuario" => $this->login,
                "fecha_registro" => fecha_y_hora_actual(),
                "matricula_academica" => 1,


            ];


            $codigoMatricula = $this->adminModel->registrarOptenerUltimoID("matriculas", $datosMatricula);


            $datosGrupo = $this->adminModel->consultarGrupo($grupo)[0];


            $asignaturasPorMatricular = $this->adminModel->consultarAsignaturasSemestrales($datosGrupo['codigo_programa'], $datosGrupo['semestre'], $datosGrupo['codigo_jornada']);


            foreach ($asignaturasPorMatricular as $asignatura) {

                $datosMatriculaAsignatura = [

                    "matricula" => $codigoMatricula,
                    "asignatura_semestral" => $asignatura['codigo'],
                    "fecha_registro" => fecha_y_hora_actual(),


                ];


                $codigoAsignaturaMatriculada = $this->adminModel->registrarOptenerUltimoID("asignaturas_matriculadas", $datosMatriculaAsignatura);

                $datosMatriculaAsignatura = [

                    "asignatura_matriculada" => $codigoAsignaturaMatriculada,
                    "fecha_registro" => fecha_y_hora_actual()

                ];


                $codigoNota = $this->adminModel->registrar("notas", $datosMatriculaAsignatura);


            }


        } else {


            $datos = [
                "ultima_fecha_modificacion" => fecha_y_hora_actual(),
                "grupo" => $grupo,
                "editada_por" => $this->login

            ];

            $this->adminModel->editarMatricula($codigo, $datos);

        }


        $this->session->set_tempdata('documento-estudiante', $documentoEstudiante, 300);
        redirect(base_url("admin/matriculas/consultar"));


    }


    function vistaMatriculasAulaVirtual()
    {


        $datos['css'] = array('select2/select2.min.css', 'select2/select2-bootstrap.css');

        $datos['js'] = array('select2/select2.min.js', 'select2/es.js', 'sistema-academico/matriculas.js');
        $datos['titulo'] = "Sistema Académico - Listado de Matrículas";

      $datos['grupos'] = $this->adminModel->consultarGrupos($this->peridoActual);

        $datos['periodo'] = $this->peridoActual;
        $datos['cursos'] = $this->adminModel->consultarCursosAulaVirtual();


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['contenido'] = 'matriculas/listado_matriculas_por_grupo_aula_virtual';
        
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }


    function cancelarMatricula()
    {


        $codigo = $this->input->post("codigo");


        $matricula = $this->adminModel->consultarMatricula($codigo)[0];


        $periodo = $matricula['periodo'];
        $estudiante = $matricula['estudiante'];



        if (strcmp($this->peridoActual, $periodo) == 0) {



            $this->cancelarMatriculaAulaVirtual($estudiante);



            echo $this->adminModel->cancelarMatricula($codigo);

        } else {

            echo "-1";

        }


    }





    function vistaReporteNotasGrupal()
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/consultarNotas.js');
        $datos['titulo'] = "Sistema Académico - Reporte notas";

        $datos['periodos'] = $this->adminModel->consultarPeriodos();


        $datos['formato'] = "pdf";

        $datos['contenido'] = 'notas/reporte_grupal';
        $this->load->view("admin/plantilla", $datos);


    }


    function vistaReporteNotasExcel()
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/consultarNotas.js');
        $datos['titulo'] = "Sistema Académico - Reporte notas reprobadas";

        $datos['periodos'] = $this->adminModel->consultarPeriodoMatriculasAbiertas(0);

        $datos['formato'] = "excel";
        $datos['contenido'] = 'notas/reporte_grupal';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }


    function vistaReporteNotasIndividual()
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/matriculas.js');
        $datos['titulo'] = "Sistema Académico - Consultar Matrículas";
        $datos['contenido'] = 'notas/reporte_individual';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }


    function vistaListadoMatricula()
    {


        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');

        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/matriculas.js');
        $datos['titulo'] = "Sistema Académico - Listado de Matrículas";

        $datos['periodos'] = $this->adminModel->consultarPeriodos();

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['contenido'] = 'matriculas/listado_matriculas_por_grupo';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }

    function vistaPlanillas()
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['css'] = ['select2/select2.min.css', 'select2/select2-bootstrap.css'];

        $datos['js'] = ['sistema-academico/cargaAcademica.js', 'select2/select2.min.js', 'select2/es.js'];


        $datos['titulo'] = "Planillas de notas";
        $datos['contenido'] = '../admin/docentes/planillas';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }


    function vistaMatriculasCursosAulaVirtual()
    {

        $datos['css'] = array('jquery-ui.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css', 'dataTables.bootstrap.css');

        $datos['js'] = array('jquery-ui.js', 'sistema-academico/cursos.js', 'modalBootstrap.js', 'select2/select2.min.js', 'select2/es.js');



        $datos['titulo'] = "Matrículas Cursos Aula Virtual - Sistema Académicas ";
        $datos['contenido'] = '../admin/estudiantes/consultar_cursos_aula_virtual';

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $this->load->view("admin/plantilla", $datos);


    }

    function vistaCargasAcademicas()
    {

        $datos['css'] = array('jquery-ui.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css', 'jquery-confirm/jquery-confirm.css');

        $datos['js'] = array('jquery-ui.js', 'sistema-academico/cargaAcademica.js', 'modalBootstrap.js', 'select2/select2.min.js', 'select2/es.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js' );



        $datos['titulo'] = "Cargas Académicas - Sistema Académicas ";
        $datos['contenido'] = '../admin/docentes/consultar_cargas_academicas';

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;


        $datos['docentes'] = $this->adminModel->listarDocentes();
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }

    function vistaCrearCargaAcademicasDocentes()
    {

        $datos['css'] = array('jquery-ui.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css', 'select2/select2-bootstrap.css', 'jquery-confirm/jquery-confirm.css');

        $datos['js'] = array('jquery-ui.js', 'sistema-academico/cargaAcademica.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'modalBootstrap.js', 'select2/select2.min.js', 'select2/es.js');

        $datos['programas'] = $this->adminModel->consultarTodosLosProgramas();
        $datos['semestres'] = $this->adminModel->consultarSemestre();
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['titulo'] = "Cargas académicas";
        $datos['contenido'] = '../admin/docentes/crear_cargas_academicas';
        $this->load->view("admin/plantilla", $datos);


    }

    public function consultarPeriodoAcademicoActual()
    {

        return $this->adminModel->consultarPeriodoAcademicoActual($this->peridoActual);
    }


    function consultarDetallesDeGrupo()
    {

        $grupo = $this->input->post('grupo');

        $result = $this->adminModel->consultarDetallesDeGrupo($grupo);

        echo json_encode($result);
    }


    function vistaPlanesDeEstudio()
    {

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-ui.css');
        $datos['js'] = array('jquery-ui.js', 'sistema-academico/cargaAcademica.js');

        $datos['titulo'] = "Cargas académicas";
        $datos['contenido'] = '../admistrador/asignaturas/plan_de_estudio';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admistrador/plantilla", $datos);


    }

    function autoCompletarDocentes()
    {


        if (isset($_GET['term'])) {


            $nombres = strtolower($_GET['term']);
            $valores = $this->adminModel->autoCompletarDocentes($nombres);


            echo json_encode($valores);
        }
    }

    function consultarAsignaturasPorPrograma()
    {

        $programa = $this->input->post("programa");


        $programas = $this->adminModel->consultarAsignaturas($programa);


        echo json_encode($programas);

    }


    function consultarGruposPorPrograma()
    {

        $programa = $this->input->post("programa");

        $semestre = $this->input->post("semestre");
        $jornada = $this->input->post("jornada");

        $programas = $this->adminModel->consultarGruposPorPrograma($programa, $semestre, $jornada, $this->peridoActual);


        echo json_encode($programas);

    }


    function consultarAsignatura()
    {

        $codigo = $this->input->post("codigo");

        if (isset($codigo)) {


            $docente = $this->adminModel->consultarAsignatura($codigo);


            echo json_encode($docente);
        }

    }






    function consultarAsignaturas()
    {

        $programa = $this->input->post("programa");
        $semestre = $this->input->post("semestre");
        $jornada = $this->input->post("jornada");
        $activa = $this->input->post("activa");


        $asignturas = $this->adminModel->consultarAsignaturasPorGrupos($programa, $semestre, $jornada, $activa);


        echo json_encode($asignturas);


    }


    function cambiarDocenteCargaAcademica(){


        $codigo = $this->input->post("codigo-carga");
        $docente = $this->input->post("nuevo-docente");





  echo   $this->adminModel->cambiarDocenteCargaAcademica($codigo,$docente);







    }

    function registrarCargaAcademica()
    {


        $docente = $this->input->post("docenteDocumento");
        $jornada = $this->input->post("jornadaCodigo");
        $asignatura = $this->input->post("asignatura");
        $grupo = $this->input->post("numeroGrupo");


        $carga = -1;

        $existe = count($this->adminModel->consultarCargasAcademicasDuplicada($asignatura, $this->peridoActual, $jornada, $grupo, $docente));


        if ($existe == 0) {



            $carga = count($this->adminModel->consultarCargasAcademicasOtrosDocentes($asignatura, $this->peridoActual, $jornada, $grupo, $docente));


            if ($carga == 0) {

                $datosCargaAcademica = [

                    "asignatura_semestral" => $asignatura,
                    "grupo" => $grupo,
                    "jornada" => $jornada,
                    "periodo" => $this->peridoActual,
                    "docente" => $docente,
                    "fecha_registro" => fecha_y_hora_actual(),
                    "usuario" => $this->login


                ];


                if ($jornada == "S") {


                    $datosCargaAcademica["evaluado_corte1"] = 1;
                    $datosCargaAcademica["evaluado_corte2"] = 1;
                    $datosCargaAcademica["evaluado_corte3"] = 1;


                } else {

                    $datosCargaAcademica["evaluado_corte0"] = 1;

                }


                $carga = $this->adminModel->registrar("cargas_academicas", $datosCargaAcademica);

            }else{

                $carga = 0;
            }
        }

        echo $carga;

    }


    public function notas($jornada, $accion = 1, $parametro = null, $parametro2 = null, $parametro3 = null)
    {


        /*
         * @$accion
         * - por-programa
         * - asignatura
         * - digitar
         * - grupos
         *
         * */

        $corteActual = 0;


        if ($jornada == "lv") {


            //   $fechas = $this->adminModel->consultarFechasDigitacionNotas($this->peridoActual);

            $corteActual = $this->adminModel->consultarConfiguracion()['corte_actual'];



        }


        if ($accion == 1) {

            $this->vistaProgramasOrientados($corteActual, $jornada, $this->peridoActual);

        } else if ($accion == "asignatura") {


            /*
             * @$parametro: codigo de programa
             * */
            $this->vistaVerAsignaturasPorPrograma($parametro, $corteActual, $jornada, $this->peridoActual);


        } else if ($accion == "digitar") {


            /*
            * @$parametro: codigo de la carga académica
            * @$parametro: codigo del programa
            * @$parametro3: codigo del grupo
            * */

            $this->vistaVerLitadoDeEstudiantesMatriculadosPorAsignatura($parametro, $corteActual, $parametro2, $parametro3);

        } else if ('grupos') {


            $this->vistaVerAsignaturasPorGrupo($parametro, $parametro2, $corteActual, $jornada,  $this->peridoActual);
        }

    }


    public function vistaVerAsignaturasPorPrograma($programa, $corte, $jornada, $periodo)
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js');
        $datos['carga_academica'] = $this->adminModel->consultarAsiganaturasPorPrograma($programa, $corte, $jornada, $periodo);

        $datos['nombrePrograma'] = $this->adminModel->consultarProgramas($programa)[0]['nombre'];


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['titulo'] = "Digitación de Notas - Sistema Académico";
        $datos['corte'] = $corte;
        $datos['contenido'] = '../docente/notas/selecionar_asignatura';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }


    public function vistaVerAsignaturasPorGrupo($programa, $asignatura, $corte, $jornada, $periodo)
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js');
        $datos['grupos'] = $this->adminModel->consultarGruposPorAsignatura($programa, $asignatura, $corte, $jornada, $periodo);

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['nombrePrograma'] = $this->adminModel->consultarProgramas($programa)[0]['nombre'];

        $datos['codigoPrograma'] = $programa;
        $datos['titulo'] = "Digitación de Notas - Sistema Acádemico";


        $datos['corte'] = $corte;
        $datos['contenido'] = '../docente/notas/selecionar_grupo';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }


    public function vistaVerLitadoDeEstudiantesMatriculadosPorAsignatura($cargarAcademica, $corteActual, $codigoPrograma, $grupo)
    {


        $this->vistaSubirNotasMasivas($cargarAcademica, $corteActual, $codigoPrograma, $grupo);

        //   $datos['estudiantes'] = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($cargarAcademica,$corteActual,$grupo);


        /*






        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'docente.js', 'jquery.numeric.js','sistema-academico/digitacionNotas.js');



        $datos['titulo'] = "Digitación de Notas - Sistema Académico";
        $datos['codigoPrograma'] = $codigoPrograma;
        $datos['corte'] = $corteActual;
        $datos['carga'] = $cargarAcademica;


        $datos['contenido'] = '../docente/notas/listado_estudiantes_matriculados';
        $this->load->view("admin/plantilla", $datos);





        */


    }


    function z()
    {


        $asignturas = $this->adminModel->consultarAsignaturasPorGrupos("MI", "I", "LV",1);


        echo json_encode($asignturas);

    }


    public function vistaProgramasOrientados($corte, $jornada, $periodo)
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js');

        $datos['titulo'] = "Digitación de Notas - Sistema Académico";
        $datos['corte'] = $corte;

        $datos['programas'] = $this->adminModel->consultarProgramasOrientados(null, $corte, $jornada, $periodo);
        $datos['contenido'] = '../docente/notas/selecionar_programa';
        $this->load->view("admin/plantilla", $datos);


    }

    public function vistaPlataformaCerrada()
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'jquery.numeric.js');
        $datos['titulo'] = "Gestionar docente";
        $datos['contenido'] = '../docente/notas/plataforma_cerrada';
        $this->load->view("admin/plantilla", $datos);

    }


    function configuracion()
    {


        $config = $this->adminModel->consultarConfiguracion();


        echo var_dump($config);


    }

    public function vistaMatriculasCerradas()
    {

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'jquery.numeric.js');
        $datos['titulo'] = "Gestionar docente";
        $datos['contenido'] = 'matriculas/matriculas_cerradas';
        $this->load->view("admin/plantilla", $datos);

    }

    function consultarFechasDigitacionNotas()
    {


        $result = $this->adminModel->consultarFechasDigitacionNotas($this->peridoActual);


        return $result;

    }


    function consultarDocente()
    {

        $documento = $this->input->post("documento");

        if (isset($documento)) {


            $docente = $this->adminModel->consultarDocente($documento);


            echo json_encode($docente);
        }

    }


    function consultarCantidadInscripciones()
    {


        $inscripciones = $this->adminModel->consultarCantidadInscripciones();

        echo json_encode($inscripciones);

    }

    function consultarCursosAulaVirtual()
    {


        $documento_docente = $this->input->post("documento");


        $cursos = $this->adminModel->consultarMatriculasCursosAulaVirtual($documento_docente);


        $i = 1;

        foreach ($cursos as $curso) {


            echo "
                    <tr>
                            <td>" . $i . "</td>
                            <td>" . $curso['id'] . "</td>
            
                            <td>" . $curso['programa'] . "</td>
                            <td>" . $curso['curso'] . "</td>
                           
                            
                    </tr>
            
            ";


            $i++;

        }


        if (count($cursos)==0){

            echo "
            
                <tr>
             <td colspan='4'>No hay cursos matrículados</td>
              </tr>
            
            ";

        }

    }

    function consultarCargaAcademicaPorDocente()
    {


        $documento_docente = $this->input->post("documento");


        $cargasAcademicas = $this->adminModel->consultarCargaAcademicaPorDocente($documento_docente, $this->peridoActual);


        $i = 1;

        foreach ($cargasAcademicas as $cargasAcademica) {



            $notas=0;

            if($cargasAcademica['jornada']=="LV"){


                $notas = $cargasAcademica['evaluado_corte1'];

            }else{

                $notas = $cargasAcademica['evaluado_corte0'];

            }



            /*
             *   <td>' . $cargasAcademica['asignatura'] . ' - ' . $cargasAcademica['asignatura_semestral'] . '</td>
             * */

            echo '
                    <tr  id="'.$cargasAcademica['codigo'].'">
                            <td>' . $i . '</td>
                            <td>' . $cargasAcademica['codigo'] . '</td>
                              <td>' . $cargasAcademica['asignatura_semestral'] . '</td>
            
                            <td>' . $cargasAcademica['asignatura'] . '</td>
                            <td>' . $cargasAcademica['programa'] . '</td>
                            
                            <td class="text-center">' . $cargasAcademica['semestre'] . '</td>
                            <td>' . $cargasAcademica['jornada'] . '</td>
                            <td class="text-center">' . $cargasAcademica['grupo'] . '</td>
                              <td class="text-center">' . formato_si_no2( $notas) . '</td>
                            
      
                       <td class="text-center">
                      
                       <a href="javascript:seleccionarCargaAcademica(' . $cargasAcademica['codigo'] . ')" class="fa fa-trash"></a>
                         <a href="javascript:seleccionarCargaAcademica2(' . $cargasAcademica['codigo'] . ')" class="fa fa-edit"></a>
                     
                      </td>
                           
                            
                    </tr>
            
            ';


            $i++;

        }


    }


    function consultarJorndasCargasAcademicas()
    {


        $documento_docente = $this->input->post("documento");


        $cargasAcademicas = $this->adminModel->consultarJorndasCargasAcademicas($documento_docente, $this->peridoActual);


        foreach ($cargasAcademicas as $cargasAcademica) {


            echo '
                    <tr>
                          
                            <td>' . $cargasAcademica['jornada'] . '</td>
                            <td class="text-center"> <a class="fa fa-file-excel-o" href="' . base_url('reporte/planillas/' . $cargasAcademica['docente'] . '/' . $cargasAcademica['codigo_jornada']) . '"> </a> </td>
        
                           
                            
                    </tr>
            
            ';

        }


    }

    function consultarEstudiante()
    {


        $documento = $this->input->post("documento");

        if (isset($documento)) {


            $estudiante = $this->adminModel->consultarEstudiante($documento);


            echo json_encode($estudiante);
        }
    }


    function consultarVisitasAlsistemaPorPlataforma()
    {

        $visitas = $this->adminModel->consultarVisitasAlsistemaPorPlataforma();


        echo json_encode($visitas);

    }

    function consultarVisitasAlSistema()
    {


        $visitas = $this->adminModel->consultatVisitasAlSistema();

        echo json_encode($visitas);


    }

    function consultarCantidadDeEvaluacionesDocentes()
    {


        $evaluacionesDocentes = $this->adminModel->consultarCantidadDeEvaluacionesDocentes();

        echo json_encode($evaluacionesDocentes);


    }


    function consultarAsignaturasParaHabilitar()
    {

        $documento = $this->input->post("documento");
        $notas = $this->adminModel->consultarAsignaturasMatriculadas($documento, null, true);


        foreach ($notas as $nota) {


            echo '<tr>

                   
                    <td>' . $nota['codigo'] . '</td>
                    <td>' . $nota['asignatura'] . '</td>
                    <td  class="text-center">' . $nota['nota_definitiva'] . '</td>
                    <td class="text-center">
                        <a class="text-center fa fa-edit fa-lg" href="javascript:verModalEditarNotas(' . $nota['codigo'] . ')"></a>
                    </td>
        
                
                
                </tr>';

        }


    }


    function restablecerClave()
    {

        $documento = $this->input->post("documento");
        $nuevaClave = $this->input->post("nueva-clave");


        $this->load->model("Usuario_model", "usuarioModel");


        $datos = [

            "clave" => encrypt($nuevaClave),
            "ultima_fecha_modificacion" => fecha_y_hora_actual()

        ];

        $this->adminModel->editarUsuario($documento, $datos);


        $this->usuarioModel->cambiarClaveDeAccesoMoodle($documento, $nuevaClave);

        $this->session->set_tempdata('documento-estudiante', $documento, 300);


        redirect(getenv('HTTP_REFERER'));
        //   redirect(base_url("admin/estudiante"));


    }

    function consultarAsignaturasMatriculadasParaEditar()
    {

        $documento = $this->input->post("documento");
        $jornada = $this->input->post("jornada");


        $this->session->set_tempdata('documento-estudiante', $documento, 300);


        $notas = $this->adminModel->consultarAsignaturasMatriculadas($documento, $jornada, null, $this->peridoActual);


        if ($jornada == "s") {


            foreach ($notas as $nota) {


                echo '<tr>

                   
                    <td>' . $nota['codigo'] . '</td>
                    <td>' . $nota['asignatura'] . '</td>
                    <td class="text-center">' . estilo_nota_definitiva($nota['nota_definitiva']) . '</td>
                    <td class="text-center">
                        <a class="text-center fa fa-edit fa-lg" href="javascript:verModalEditarNotas(' . $nota['codigo'] . ')"></a>
                    </td>
        
                
                
                </tr>';


            }

        } else if ($jornada == "m") {

            foreach ($notas as $nota) {


                echo '<tr>

                   
                    <td>' . $nota['codigo'] . '</td>
                    <td>' . $nota['asignatura'] . '</td>

                    <td class="text-center">' . $nota['nota1'] . '</td>
                    <td class="text-center">' . $nota['nota2'] . '</td>
                    <td class="text-center">' . $nota['nota3'] . '</td>
                    <td class="text-center">' . estilo_nota_definitiva($nota['nota_definitiva']) . '</td>
                    <td class="text-center">
                        <a class="text-center fa fa-edit fa-lg" href="javascript:verModalEditarNotas(' . $nota['codigo'] . ')"></a>
                    </td>
        
                
                
                </tr>';

            }
        }


    }

    function consultarAsignaturasMatriculadas()
    {

        $documento = $this->input->post("documento");


        $notas = $this->adminModel->consultarAsignaturasMatriculadas($documento, null, null, $this->peridoActual);


        foreach ($notas as $nota) {



            /*

         <td class="text-center">' . $nota['nota1'] . '</td>
         <td class="text-center"> ' . $nota['nota2'] . '</td>
         <td class="text-center">' . $nota['nota3'] . '</td>


         */

            echo '<tr>

                   
                    <td>' . $nota['codigo'] . '</td>
                    <td>' . $nota['codigo_asignatura'] . '</td>
                    <td>' . $nota['asignatura'] . '</td>
                    
         
                    <td class="text-center">' . estilo_nota_definitiva($nota['nota_definitiva']) . '</td>
               
                
                </tr>';

        }


    }


    function consultarMatriculasPorEstudiantes()
    {

        $documento = $this->input->post("documento");


        $estudiantes = $this->adminModel->consultarEstudiantesPorGrupo($documento);

        $i = 1;


        foreach ($estudiantes as $estudiante) {

            echo '<tr>

                   
                   <td>' . $i . '</td>
                    <td>' . $estudiante['documento'] . '</td>
                    <td>' . $estudiante['nombres'] . '</td>

                
                
                </tr>';


            $i++;

        }

    }


    function consultarEstudiantesPorGrupo()
    {

        $grupo = $this->input->post("grupo");

        $estado = $this->input->post("estado");

        $estudiantes = $this->adminModel->consultarEstudiantesPorGrupo($grupo,$estado);

        $i = 1;


        foreach ($estudiantes as $estudiante) {

            echo '<tr>

                   
                   <td>' . $i . '</td>
                    <td>' . $estudiante['documento'] . '</td>
                    <td>' . $estudiante['nombres'] . '</td>
                    <td>' . $estudiante['correo'] . '</td>
                    <td>' . $estudiante['celular'] . '</td>
              
                   

                
                
                </tr>';


            $i++;

        }

    }


    function consultarGrupoPorPeriodo()
    {

        $codigo = $this->input->post("codigo");


        $grupos = $this->adminModel->consultarGrupos($codigo);


        echo '<option value="">Seleccione</option>';

        foreach ($grupos as $grupo) {


            echo '<option value="' . $grupo['codigo'] . '">' . $grupo['programa'] . ' ' . $grupo['semestre'] . ' - ' . $grupo['jornada'] . ' - ' . $grupo['grupo'] . '</option>';


        }

    }


    function consultarCargaAcademica()
    {

        $documento = $this->input->post("documento");


        $asignaturas = $this->docente->consultarCargaAcademica($documento);


        foreach ($asignaturas as $asignatura) {

            echo '<tr>

                   
                    <td>' . $asignatura['asignaturas'] . '</td>
                    <td>' . $asignatura['grado'] . '</td>
                     <td class="text-center">' . $asignatura['horas_semanales'] . '</td>
                
                
                </tr>';

        }

    }




    function consultarDocentesAsignados($codigoMatricula)
    {


        $asignaturasMatriculadas = $this->usuario->consultarDocentesAsignados($codigoMatricula);


        foreach ($asignaturasMatriculadas as $asignaturaMatriculada) {


            echo $asignaturaMatriculada['nombres'] . " - " . $asignaturaMatriculada['asignatura'] . "<br>";


        }


    }


    function enviarCorreoMatriculaPrimerIngreso($correoDestinatario, $nombreDestinatario, $programa, $jornada)
    {

        $this->load->library('email');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'notificaciones@ebah.edu.co',
            'smtp_pass' => 'notificacionesweb',
            'smtp_timeout' => '7',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'mailtype' => 'html',
            'validation' => false
        );


        $this->email->initialize($config)
            ->from('notificaciones@ebah.edu.co')
            ->to(strtolower($correoDestinatario))
            ->subject("Matrícula de $nombreDestinatario - Escuela de Bellas Artes y Humanidades de Sucre")
            ->set_mailtype('html')
            ->message('
                
                
     <div>
    <div>


        <div style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0;padding:0">


            <center>
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#f2f2f2" style="background-color:#f2f2f2!important;border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td height="7" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;border-spacing:0;border-collapse:collapse;height:7px;font-size:1px;line-height:1px;background:#008b21;padding:0" bgcolor="#8dc23e">
                            </td>

                        </tr>


                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                <table width="100%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">


                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                        <br>





                                        <tr height="2" style="height:2px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>


                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                            <td width="130" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <a href="https://www.ebah.edu.co/" style="color:#8dc23e;text-decoration:underline" target="_blank" data-saferedirecturl="">

                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/logo-ebah-sistema-academico.png" width="250" alt="Logo EBAH" style="float:none;display:block;border:0 none" align="none">
                                                                </a>
                                                            </td>


                                                        </tr>



                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr height="25" style="height:25px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                        </tr>





                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" valign="top" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;padding:0">
                                                <table width="96.67%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" valign="top" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>



                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="76%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                                <p style="font-size:16px;line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">Hola, ' . $nombreDestinatario . '</p>

                                                                             
                                                                                <p style="font-size:16px;text-align: justify; line-height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;margin:0 0 28px">
                                                                                    
                                                                                    
                                                                                    Te encuentras matriculado al programa <b>' . $programa . '</b>, en la jornda ' . $jornada . '. La escuela cuenta con un Sistema Académico, en el cual podrás consultar tus notas y realizar otras actividades académicas. El usuario y contraseña es tu documento de identidad, con los cuales podrás ingresar. 
                                                                                    
                                                                                </p>
                                                                                

                                                                    
                                                            
          	                                                                        <div style="text-align:center"><span style="font-family:arial,helvetica,sans-serif">
                                                                                	
                                                                                	<a href="https://sistemaacademico.ebah.edu.co/" style="border-radius:2px;letter-spacing:1px;text-transform:uppercase;font-size:13px;font-weight:400;display:inline-block;background:#76c043 ;padding:15px 25px 14px;color:rgb(255,255,255);line-height:18px!important;text-decoration:none" target="_blank" data-saferedirecturl="">INICIAR SESIÓN </a>
                                                                                	
                                                                                	</span>
                                                                                	
                                                                                	</div>
                                                                 
                                                                                
                                                                   
                                                                                
                                                                                
                                                                                                                   
                                                 
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>



                                                        <tr height="70" style="height:70px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>







                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:100%;max-width:600px">
                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td height="45" style="height:45px;word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>



                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                <table width="96.67%" border="0" cellpadding="0" cellspacing="0" valign="top" bgcolor="#f2f2f2" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">

                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table class="m_-1662020414733504001responsive-table" border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">




                                                                        <tr height="35" style="height:35px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                      
                                                                                       <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                 <i>    Esta es una notificación automática, por favor no responda este mensaje. </i>

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                      
                                                                       
                                                                      
                                                                       
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td valign="top" align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="font-size:12px;line-height:24px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0">



                                                                                     Mantente conectado para recibir consejos, actualizaciones y recursos

                                                                                </p>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.facebook.com/BellasArtesEbah/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/facebook.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Facebook icon" align="none">
                                                                                </a>
                                                                            </td>
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://twitter.com/BellasArteSucre" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/twitter.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Twitter icon" align="none">
                                                                                </a>
                                                                            </td>

                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <a href="https://www.instagram.com/bellasartesdesucre/" target="_blank">
                                                                                    <img src="https://sistemaacademico.ebah.edu.co/assets/img/correos/instagram.png" width="28" height="28" style="max-width:28px;width:28px;height:28px;float:none;display:block;margin:3px;border:0 none" alt="Linkedin icon" align="none" class="CToWUd">
                                                                                </a>
                                                                            </td>



                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>


                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                <table border="0" cellpadding="0" cellspacing="0" valign="top" width="96.67%" style="border-spacing:0;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;width:96.67%;max-width:580px">

                                                                    <tbody style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                        <tr style="font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                            <td align="center" style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">
                                                                                <p style="line-height:22px!important;font-size:12px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#aaaaaa;margin:0 0 28px">
                                                                                    <a style="color:#aaaaaa;text-decoration:underline" href="h" target="_blank" data-saferedirecturl=""><a style="color:#aaaaaa!important;text-decoration:none">

                                                                                            Calle 19 No. 16A - 09 Barrio Ford, Sincelejo - Sucre <br>
                                                                                            Teléfono: 035 2823160 - 3164933505 <br>

                                                                                            Correo: info@ebah.edu.co



                                                                                        </a>


                                                                                </p>
                                                                            </td>
                                                                        </tr>


                                                                        <tr height="30" style="height:30px;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777;font-size:0;line-height:0;margin:0;padding:0">
                                                                            <td style="word-break:normal;font-family:Helvetica,Arial,Open Sans,sans-serif;color:#777777">&nbsp;</td>
                                                                        </tr>


                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                    </tbody>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </center>



        </div>
    </div>

</div>
                
                
                
                
                ');


        return $this->email->send();

    }


    function enviarCorreoCreacionCorreoInstutucional($documento,$nombresCompletos, $correo)
    {

        $this->load->library('email');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'notificaciones@ebah.edu.co',
            'smtp_pass' => 'notificacionesweb',
            'smtp_timeout' => '7',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'mailtype' => 'html',
            'validation' => false
        );


        $this->email->initialize($config)
            ->from('notificaciones@ebah.edu.co')
            ->to(strtolower('soporte@ebah.edu.co'))
            ->subject("CREAR CORREO INSTUTUCIONAL")
            ->set_mailtype('html')
            ->message($documento." ".$nombresCompletos." ".$correo);


        return $this->email->send();

    }

}
