<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once APPPATH . 'controllers/Admin.php';

class SuperAdmin extends Admin
{


    function __construct()
    {


        parent::__construct();

        $this->load->model("Estudiante_model", "estudianteModel");

        if ($this->session->userdata('tipo') != "SA") {

            redirect(base_url());

        }
    }


    function vistaEditarNotas($jornada = "m")
    {


        $jornada = substr($jornada, 0, 1);

        $documentoEstudiante = $this->session->tempdata('documento-estudiante');


        if (!is_null($documentoEstudiante)) {

            $datos['notas'] = $this->adminModel->consultarAsignaturasMatriculadas($documentoEstudiante, $jornada, null, $this->peridoActual);
            $datos['estudiante'] = $this->adminModel->consultarEstudiante($documentoEstudiante)[0];


        } else {


            $datos['estudiante'] = ["documento" => "",
                "nombres" => "",
                "apellidos" => ""
            ];


        }

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-confirm/jquery-confirm.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/editarNotas.js');
        $datos['titulo'] = "Editar notas";


        if ($jornada == "m") {

            $datos['contenido'] = 'notas/editar';


        } else {

            $datos['contenido'] = 'notas/editar_sabados';
            $jornada = "s";


        }


        $datos['jornada'] = $jornada;
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);


    }


    function activarPeriodo($codigo)
    {

        $this->adminModel->desactivarPeriodo("actual");
        $this->adminModel->activarPeriodo($codigo, "actual");


        $this->peridoActual = $codigo;
        $this->session->set_userdata('periodo', $codigo);

        redirect("admin/periodos");

    }


    function activarPeriodoMatriculasAbiertas($codigo)
    {

        $this->adminModel->desactivarPeriodo("matriculas_abiertas");
        $this->adminModel->activarPeriodo($codigo, "matriculas_abiertas");


        redirect("admin/periodos");

    }









    function generarClaveUsuarios($documento)
    {

        $this->load->helper("encrypt");




        echo encrypt($documento);

    }

    function optenerClaveUsuarios($documento)
    {

        $this->load->helper("encrypt");
        $this->load->model("Usuario_model", "usuario");

        $usuario = $this->usuario->consultar($documento);


       echo decrypt($usuario[0]['clave']);

    }







    function registrarPeriodo()
    {


        $anio = $this->input->post('anio');
        $semestre = $this->input->post('periodo');

        $fechaIncio = $this->input->post('fecha-incio');
        $fechaFin = $this->input->post('fecha-fin');

        $codigo = $anio . "" . $semestre;


        $auxDescripcion = "PRIMER";

        if ($semestre == 2) {

            $auxDescripcion = "SEGUNDO";

        }


        $descripcion = "$auxDescripcion PERIODO $anio";

        $existe = $this->adminModel->consultarPorCodigo("periodos", $codigo);

        $registro = 0;

        if (count($existe) == 0) {


            $datos = array(

                "codigo" => $codigo,
                "descripcion" => $descripcion,
                "fecha_inicio" => $fechaIncio,
                "fecha_fin" => $fechaFin



            );


            $registro = $this->adminModel->registrar("periodos", $datos);


            if ($registro > 0) {


                $numeroGrupos = $this->registrarGruposProximoPeriodo($codigo);


                echo $numeroGrupos;

            }


        }

        echo $registro;

    }


    function registrarGruposProximoPeriodo($periodo)
    {


        $grupos = $this->adminModel->consultarGruposProximoPeriodo($periodo);


        $cont = 0;

        foreach ($grupos as $grupo) {


            $grupo['fecha_registro']= fecha_y_hora_actual();
            $grupo['usuario']= $this->login;



            $cont += $this->adminModel->registrar("grupos", $grupo);


        }


        return $cont;

    }


    public function vistaEvaluacionDocente()
    {


        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'switchery/switchery.min.css');
        $datos['js'] = array('modalBootstrap.js', 'sistema-academico/evaluacionDocente.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'switchery/switchery.min.js');
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['evaluaciones'] = $this->adminModel->consultarListadoEvaluacionesDocente();

        $datos['nuevo'] = $this->adminModel->consultarPeriodoSinEvaluar();

        $datos['titulo'] = "Evaluacion Docente";
        $datos['contenido'] = '../admin/docentes/evaluacion';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }


    function activarEvaluacionDocente($codigo, $estado)
    {


        $this->adminModel->activarEvaluacionDocente($codigo, $estado);


        redirect("admin/docentes/evaluacion");


    }


    function registrarEvacuacionesDocentes2()
    {

        $this->db->trans_start();

        $periodo = $this->adminModel->consultarPeriodoSinEvaluar()[0];

        $datosEvaluacion = [

            "periodo" => $periodo['codigo']

        ];




        $evaluacionDocente = $this->adminModel->consultarEvaluacionDocentePeriodoActual()[0];


        $matriculas = $this->adminModel->consultarMatriculasPorRealizarEvaluacionDocente($evaluacionDocente['periodo']);


        $i = 0;


        foreach ($matriculas as $matricula) {


            $this->registrarDocentesEvaluacion($matricula['codigo'], $evaluacionDocente['codigo']);

            $i++;
        }


        $this->db->trans_complete();


        redirect(base_url('admin/docentes/evaluacion'));


    }

    function registrarEvacuacionesDocentes()
    {

        $this->db->trans_start();

        $periodo = $this->adminModel->consultarPeriodoSinEvaluar()[0];

        $datosEvaluacion = [

            "periodo" => $periodo['codigo']

        ];


        $this->adminModel->registrar("evaluaciones_docentes", $datosEvaluacion);


        $evaluacionDocente = $this->adminModel->consultarEvaluacionDocentePeriodoActual()[0];


        $matriculas = $this->adminModel->consultarMatriculasPorRealizarEvaluacionDocente($evaluacionDocente['periodo']);


        $i = 0;


        foreach ($matriculas as $matricula) {


            $this->registrarDocentesEvaluacion($matricula['codigo'], $evaluacionDocente['codigo']);

            $i++;
        }


        $this->db->trans_complete();


        redirect(base_url('admin/docentes/evaluacion'));


    }



    function registrarDocentesEvaluacion($codigoMatricula, $codigoEvaluacion)
    {


        $datos = [

            "matricula" => $codigoMatricula,
            "evaluacion_docente" => $codigoEvaluacion,
            "fecha_registro" => fecha_y_hora_actual()


        ];


        $this->adminModel->registrar("evaluaciones_docentes_por_matriculas", $datos);

        $evaluaciones = $this->adminModel->consultarEvaluacionesDocentes($codigoMatricula);


        foreach ($evaluaciones as $evaluacion) {


            $asignaturasMatriculadas = $this->adminModel->consultarDocentesAsignados($evaluacion['matricula']);


            foreach ($asignaturasMatriculadas as $asignaturasMatriculada) {

                //   echo var_dump($evaluacion['codigo']." - ".$asignaturasMatriculada['codigo'])."<br>";


                $datos = [

                    "evaluacion_docente_por_matricula" => $evaluacion['codigo'],
                    "carga_academica" => $asignaturasMatriculada['codigo'],
                    "fecha_registro" => fecha_y_hora_actual()


                ];

                $this->adminModel->registrar("evaluaciones_docentes_por_asignaturas", $datos);


            }


        }


    }


    public function vistaPeriodos()
    {

        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'switchery/switchery.min.css');
        $datos['js'] = array('modalBootstrap.js', 'sistema-academico/periodo.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'switchery/switchery.min.js');

        $datos['periodo'] = $this->adminModel->consultarPeriodos($this->peridoActual);


        $datos['periodos'] = $this->adminModel->consultarPeriodos();

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['titulo'] = "Periodos";
        $datos['contenido'] = '../admin/periodos/gestionar';
        $this->load->view("admin/plantilla", $datos);


    }


    function vistaConfiguracion()
    {


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'switchery/switchery.min.css');
        $datos['js'] = array('modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'switchery/switchery.min.js');


        $datos['configuracion'] = $this->adminModel->consultarConfiguracion();


        $datos['titulo'] = "Configuración";
        $datos['contenido'] = '../admin/configuracion/configuracion';
        $datos['programas'] = $this->adminModel->consultarProgramas(null);
     
        $this->load->view("admin/plantilla", $datos);

    }


    function registrarConfiguracion()
    {


        $corteActual = $this->input->post("corte-actual");
        $solicitarActualizacionDatosPersonales = input_is_checked($this->input->post("solicitar-actualizacion-datos-personales"));
        $matriculaAcademica = input_is_checked($this->input->post("matriculas-academicas-atutomaticas"));
        $filaNotas = $this->input->post("fila-notas");
        $columnaNotas = $this->input->post("columna-notas");
        $inscripcionesWeb = input_is_checked($this->input->post("inscripciones-web"));


        $datos = [

            "corte_actual" => $corteActual,
            "matriculas_academicas" => $matriculaAcademica,
            "actualizacion_datos_personales" => $solicitarActualizacionDatosPersonales,
            "fila_notas" => $filaNotas,
            "columna_notas" => $columnaNotas,
            "inscripciones_web"=>$inscripcionesWeb

        ];


        $this->adminModel->registrarConfiguracion($datos);


        redirect(base_url("/admin/configuracion"));


    }


    function editarNotaSabatina()
    {


        $codigo = $this->input->post("codigo");

        $notaDefinitiva = $this->input->post("nota-definitiva");
        $jornada = $this->input->post("jornada");
        $documentoEstudiante = $this->input->post("documento-estudiante");

        $notas = [
            "nota_definitiva" => $notaDefinitiva,
            "ultima_fecha_modificacion" => fecha_y_hora_actual(),
            "modificada_por" => $this->login

        ];


        $aux = $this->adminModel->editarNotas($codigo, $notas);


        if ($aux > 0) {


            $this->session->set_tempdata('documento-estudiante', $documentoEstudiante, 300);

            redirect(base_url('admin/notas/editar/' . $jornada));

        }


    }


    function editarNota()
    {


        $codigo = $this->input->post("codigo");

        $nota1 = $this->input->post("nota1");
        $nota2 = $this->input->post("nota2");
        $nota3 = $this->input->post("nota3");

        $jornada = $this->input->post("jornada");
        $documentoEstudiante = $this->input->post("documento-estudiante");


        $notasCompletas = false;

        if (!empty($nota1)) {


            $notas["nota1"] = $nota1;
            $notasCompletas = true;

        } else {

            $notasCompletas = false;
        }

        if (!empty($nota2)) {


            $notas["nota2"] = $nota2;
            $notasCompletas = true;

        } else {

            $notasCompletas = false;
        }

        if (!empty($nota3)) {


            $notas["nota3"] = $nota3;
            $notasCompletas = true;

        } else {

            $notasCompletas = false;
        }


        $notas["ultima_fecha_modificacion"] = fecha_y_hora_actual();
        $notas["modificada_por"] = $this->login;

        $aux = $this->adminModel->editarNotas($codigo, $notas);


        if ($notasCompletas) {
            $this->adminModel->recalcularNotaDefinitiva($codigo);

        }


        if ($aux > 0) {


            // $this->session->set_tempdata()
            $this->session->set_tempdata('documento-estudiante', $documentoEstudiante, 300);
            redirect(base_url('admin/notas/editar/' . $jornada));

        }


    }





    function csv2($grupo,$cusrso){




        $filename = 'users_'.date('Ymd').'.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv;");

        // get data


        $query =  $this->adminModel->gruposMooble($grupo,$cusrso)->result_array();


        // file creation
        $file = fopen('php://output', 'w');

        $header = array("Username","Name","Gender","Email");

        fputcsv($file, $header);
        foreach ($query as $key=>$line){
            fputcsv($file,$line);
        }
        fclose($file);
        exit;




    }

}
