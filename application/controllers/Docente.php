<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Docente extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    protected $login;
    protected $peridoActual;

    protected  $matriculaAulaVirtual;

    function __construct()
    {
        parent::__construct();

        $this->login = $this->session->userdata('documento');
        $this->peridoActual = $this->session->userdata('periodo');

        $this->load->model("Docentes_model", "docente");

        $this->load->model("Admin_model", "adminModel");

        $this->load->model("Usuario_model", "usuarioModel");
        $this->matriculaAulaVirtual= $this->usuarioModel->consultarMatriculaAulaVirtual($this->login);


        $this->load->helper(["mi","fecha"]);


        if ($this->session->userdata('tipo') != "D") {

            redirect(base_url());

        }
    }


    function index()
    {


     //  $this->notas();



        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

         $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js', 'docente.js');

        $datos['titulo'] = "Sistema Académico - Docentes";
        $datos['contenido'] = '../docente/inicio/contenido';
        $this->load->view("docente/plantilla", $datos);





    }



    function consultarGrupoPorPeriodo()
    {

        $codigo = $this->input->post("codigo");


        $grupos = $this->adminModel->consultarGrupos($codigo);


        echo '<option value="">Seleccione</option>';

        foreach ($grupos as $grupo) {


            echo '<option value="' . $grupo['codigo'] . '">' . $grupo['programa'] . ' ' . $grupo['semestre'] . ' - ' . $grupo['jornada'] . ' - ' . $grupo['grupo'] . '</option>';


        }

    }


    function consultarEstudiantesPorGrupo()
    {

        $grupo = $this->input->post("grupo");


        $estudiantes = $this->adminModel->consultarEstudiantesPorGrupo($grupo);

        $i = 1;


        foreach ($estudiantes as $estudiante) {

            echo '<tr>

                   
                   <td>' . $i . '</td>
                    <td>' . $estudiante['documento'] . '</td>
                    <td>' . $estudiante['nombres'] . '</td>

                
                
                </tr>';


            $i++;

        }

    }

    function filtrarDocente2()
    {


        if (isset($_GET['q'])) {

            $nombres = strtolower($_GET['q']);

            $docentes = $this->adminModel->filtrarDocente(mb_strtoupper($nombres), 1);

            echo json_encode($docentes);

        }
    }



    function vistaPlanillas2()
    {

        $datos['css'] = ['select2/select2.min.css', 'select2/select2-bootstrap.css'];

        $datos['js'] = ['select2/select2.min.js', 'select2/es.js','sistema-academico/docente.js'];


        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['cargasAcademicas'] = $this->adminModel->consultarJorndasCargasAcademicas($this->login, $this->peridoActual);



        $datos['titulo'] = "Planillas de notas";
        $datos['contenido'] = 'matriculas/planillas';
        $this->load->view("docente/plantilla", $datos);


    }



    function vistaPlanillas($jormada="lv")
    {

        $datos['css'] = ['select2/select2.min.css', 'select2/select2-bootstrap.css'];

        $datos['js'] = ['select2/select2.min.js', 'select2/es.js','sistema-academico/docente.js'];



        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['cargasAcademicas'] = $this->adminModel->consultarJorndasCargasAcademicas($this->login, $this->peridoActual,$jormada);
      //  $datos['cargasAcademicas'] = $this->adminModel->consultarJorndasCargasAcademicas2($this->login, $this->peridoActual,$jormada[0]);



        $datos['titulo'] = "Planillas de notas";
        $datos['contenido'] = 'matriculas/planillas_notas';
        $this->load->view("docente/plantilla", $datos);


    }




    function vistaListadoMatricula()
    {




        $datos['css'] = array('jquery-confirm/jquery-confirm.css', 'select2/select2.min.css', 'select2/select2-bootstrap.css');

        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js', 'select2/select2.min.js', 'select2/es.js', 'sistema-academico/docente.js');


        $datos['titulo'] = "Sistema Académico - Listado de Matrículas";
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['asignaturas']= $this->adminModel->consultarCargaAcademicaPorDocente($this->login,$this->peridoActual);

        $datos['contenido'] = 'matriculas/listado_matriculas_por_grupo';
        $this->load->view("docente/plantilla", $datos);


    }



    function registrarNota()
    {


        $notas = $this->input->post("notas");

        $corte = $this->input->post("corte");
        $carga = $this->input->post("carga");


        $numeroDeNotasRegistradas = 0;


        $this->db->trans_start();

        foreach ($notas as &$nota) {


            $valores = explode("-", $nota);


            $datos = [

                "registrada_por" => $this->login,
                "ultima_fecha_modificacion" => fecha_y_hora_actual()


            ];


            $datos["nota_definitiva"] = $valores[1];



            /*

            if ($corte == 0) {


                $datos["nota_definitiva"] = $valores[1];


            } else {


                $datos["nota" . $corte] = $valores[1];

            }

            */


      $numeroDeNotasRegistradas += $this->adminModel->registrarNota($valores[0], $datos);

            if ($corte == 3) {

               $this->adminModel->calcularNotaDefinitiva($valores[0]);
            }


        }


           $this->adminModel->cambiarEstadoEvaluacionPorCorte($carga, $corte);


        $this->db->trans_complete();

        echo $numeroDeNotasRegistradas;


    }


    public function notas($jornada="lv", $accion = 1, $parametro = null, $parametro2 = null, $parametro3 = null)
    {


        /*
         * - accion
         * - por-programa
         * - asignatura
         * - digitar
         * - grupos
         *
         * */

        $corteActual = 0;


        if ($jornada == "lv") {


            //   $fechas = $this->adminModel->consultarFechasDigitacionNotas($this->peridoActual);

            $corteActual = $this->adminModel->consultarConfiguracion()['corte_actual'];


        }


        if ($accion == 1) {

            $this->vistaProgramasOrientados($corteActual, $jornada,  $this->peridoActual);

        } else if ($accion == "asignatura") {


            /*
             * @$parametro: codigo de programa
             * */


            $this->vistaVerAsignaturasPorPrograma($parametro, $corteActual, $jornada,$this->peridoActual);


        } else if ($accion == "digitar") {


            /*
            * @$parametro: codigo de la carga académica
            * @$parametro: codigo del programa
            * @$parametro3: codigo del grupo
            * */

            $this->vistaVerLitadoDeEstudiantesMatriculadosPorAsignatura($parametro, $corteActual, $parametro2, $parametro3);

        } else if ('grupos') {


            $this->vistaVerAsignaturasPorGrupo($parametro, $parametro2, $corteActual, $jornada, $this->peridoActual);
        }

    }


    public function vistaVerAsignaturasPorGrupo($programa, $asignatura, $corte, $jornada, $periodo)
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js');
        $datos['grupos'] = $this->adminModel->consultarGruposPorAsignatura($programa, $asignatura, $corte, $jornada, $periodo,$this->login);

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['nombrePrograma'] = $this->adminModel->consultarProgramas($programa)[0]['nombre'];

        $datos['codigoPrograma'] = $programa;
        $datos['titulo'] = "Digitación de Notas - Sistema Acádemico";


        $datos['corte'] = $corte;
        $datos['contenido'] = '../docente/notas/selecionar_grupo';
        $this->load->view("docente/plantilla", $datos);

    }


    public function vistaVerAsignaturasPorPrograma($programa, $corte, $jornada, $periodo)
    {


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js');
        $datos['carga_academica'] = $this->adminModel->consultarAsiganaturasPorPrograma($programa, $corte, $jornada, $periodo,$this->login);

        $datos['nombrePrograma'] = $this->adminModel->consultarProgramas($programa)[0]['nombre'];
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;

        $datos['titulo'] = "Digitación de Notas - Sistema Académico";
        $datos['corte'] = $corte;
        $datos['contenido'] = '../docente/notas/selecionar_asignatura';
        $this->load->view("docente/plantilla", $datos);


    }


    public function vistaVerLitadoDeEstudiantesMatriculadosPorAsignatura($cargarAcademica, $corteActual, $codigoPrograma, $grupo)
    {


        //   $this->vistaSubirNotasMasivas($cargarAcademica, $corteActual, $codigoPrograma, $grupo);

        $datos['estudiantes'] = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($cargarAcademica, $corteActual, $grupo);


        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css','jquery-confirm/jquery-confirm.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js',  'jquery.numeric.js','jquery-confirm/jquery-confirm.js', 'jquery-confirm/confirmacion.js' ,'sistema-academico/digitacionNotas.js');


        $datos['titulo'] = "Digitación de Notas - Sistema Académico";
        $datos['codigoPrograma'] = $codigoPrograma;
        $datos['corte'] = $corteActual;
        $datos['carga'] = $cargarAcademica;

        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['contenido'] = '../docente/notas/listado_estudiantes_matriculados';
        $this->load->view("docente/plantilla", $datos);


    }


    function consultarFechasDigitacionNotas()
    {


        $result = $this->docente->consultarFechasDigitacionNotas($this->peridoActual);


        return count($result);

    }


    public function vistaProgramasOrientados($corte, $jornada, $periodo)
    {



        $datos['css'] = array('jquery-ui.css', 'jquery.tagsinput.css');
        $datos['js'] = array('jquery-ui.js', 'modalBootstrap.js', 'jquery.tagsinput.js',);
        $datos['matricula_aula_virtual'] = $this->matriculaAulaVirtual;
        $datos['titulo'] = "Digitación de Notas - Sistema Académico";
        $datos['corte'] = $corte;

        $datos['programas'] = $this->adminModel->consultarProgramasOrientados($this->login, $corte, $jornada, $periodo,true);
        $datos['contenido'] = '../docente/notas/selecionar_programa';
        $this->load->view("docente/plantilla", $datos);


    }


    function p($parametro)
    {

        echo var_dump($this->vistaVerLitadoDeEstudiantesMatriculadosPorAsignatura($parametro));

    }
}
