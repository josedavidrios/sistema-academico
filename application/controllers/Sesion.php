<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sesion extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */


    function __construct()
    {
        parent::__construct();


        $this->load->helper(["encrypt", "fecha"]);
        $this->load->model('Sesion_model', 'sesionModel');


    }


    public function index()
    {

        $login = $this->session->userdata('documento');


        if (isset($login)) {

            $tipo_usuario = $this->session->userdata('tipo');

            if (strcmp($tipo_usuario, "A") == 0) {

                redirect(base_url("admin"));

            } else if (strcmp($tipo_usuario, "D") == 0) {

                redirect(base_url('docente'));


            } else if (strcmp($tipo_usuario, "SA") == 0) {


                redirect(base_url('superAdmin'));
            }else{

                redirect(base_url('estudiante'));


            }


        } else {

            $this->load->view('inicio/inicio_sesion.php');

        }


    }
	
	
	    function optenerClaveUsuarios($documento)
    {

        $this->load->helper("encrypt");
        $this->load->model("usuario");

        $usuario = $this->usuario->consultar($documento);


       echo decrypt($usuario[0]['clave']);

    }


    function iniciar()
    {

        $this->load->library('user_agent');
        $usuario = $this->input->post("usuario");
        $clave = encrypt($this->input->post("clave"));
        $usuario = $this->sesionModel->iniciar($usuario, $clave, "correo");

        if (count($usuario)) {


            $tipo = $usuario[0]['tipo'];
            $periodo = $this->sesionModel->consultarPeriodoActual();


            $datos = [

                "documento" => $usuario[0]['documento'],
                "nombres" =>$usuario[0]['nombres']." ".$usuario[0]['apellidos'],
                "periodo" => $periodo,
                "tipo"=>$tipo

            ];



            $datosLog = [
                "usuario" => $usuario[0]['documento'],
                "tipo_usuario"=>$tipo,
                "fecha_inicio" => fecha_y_hora_actual(),
                "plataforma" => $this->agent->platform(),
                "navegador" => $this->agent->browser(),
                "nombre_equipo" => $_SERVER['REMOTE_ADDR'],
                "referente"=>$this->agent->referrer(),
                "es_dispositivo_movil"=>$this->agent->is_mobile()

            ];




            if ($tipo == "E") {

               // $this->load->helper("user_agent");







                $this->guardarLog($datosLog);

                $this->session->set_userdata($datos);
                redirect(base_url('estudiante'));

            }

            if ($tipo == "D") {

                $this->guardarLog($datosLog);
                $this->session->set_userdata($datos);
                redirect(base_url('docente'));


            }else if ($tipo == "A") {

                $this->guardarLog($datosLog);
                $this->session->set_userdata($datos);
                redirect(base_url('admin'));


            }
            else if ($tipo == "SA") {

                $this->guardarLog($datosLog);
                $this->session->set_userdata($datos);
                redirect(base_url('superAdmin'));


            }


        } else {


            $this->session->set_flashdata('login', 'error');
            redirect(base_url());


        }


    }


    function guardarLog($datosLog){



        $codigoSesion = $this->sesionModel->registrarLog($datosLog);
        $datos["codigo"] = $codigoSesion;


    }

    function iniciarMoodle()
    {


        header("location:  https://aulavirtual.ebah.edu.co/login/login_sistema.php?token=" . url_encrypt($this->session->userdata('documento')));


    }

    function cerrar()
    {


        $this->sesionModel->cerrarLog($this->session->userdata('codigo'), fecha_y_hora_actual());


        $this->session->sess_destroy();
        redirect(base_url());

    }
}
