<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Reporte Controller
 *
 * @author TechArise Team
 *
 * @email  info@techarise.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reporte extends CI_Controller
{

    // construct


    public $textoCentrado;
    public $peridoActual;

    public function __construct()
    {
        parent::__construct();
        // load model

        $this->load->library('excel');
        $this->load->model("Admin_model", "adminModel");
        $this->load->model("Estudiante_model", "estudianteModel");
        $this->load->library('pdf');

        $this->load->helper("fecha");

        $this->textoCentrado = array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        $login = $this->session->userdata('documento');
        $this->peridoActual = $this->session->userdata('periodo');

        if (!isset($login)) {

            redirect(base_url());
        }


    }


    function listatoEstudiantesGrupalExcel($codigoGrupo,$estado)
    {


        $grupo = $this->adminModel->consultarGrupo($codigoGrupo)[0];

        // load excel library

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);


        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $gdImage = imagecreatefrompng(base_url('assets/img/logo.png'));
        $objDrawing->setImageResource($gdImage);
        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWidth(100);
        $objDrawing->setCoordinates('D2');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());


        $bordes = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );


        $objPHPExcel->getActiveSheet()->getStyle('B2:B4')->getAlignment()->applyFromArray($this->textoCentrado);
        $objPHPExcel->getActiveSheet()->getStyle("B2:E4")->getFont()->setBold(true);


        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'ESCUELA DE BELLAS ARTES Y HUMANIDADES DEL DEPARTAMENTO DE SUCRE');
        $objPHPExcel->getActiveSheet()->setCellValue('B3', $grupo['programa'] . ' - ' . $grupo['semestre'] . " SEMESTRE - "."GRUPO ".$grupo['grupo']);
        $objPHPExcel->getActiveSheet()->setCellValue('B4', 'JORNADA ' . $grupo['jornada']);


        $objPHPExcel->getActiveSheet()->mergeCells('B2:C2');
        $objPHPExcel->getActiveSheet()->mergeCells('B3:C3');
        $objPHPExcel->getActiveSheet()->mergeCells('B4:C4');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);


        $fila = 7;
        $filaEncabezadoTabla = $fila - 1;


        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $filaEncabezadoTabla, '#');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $filaEncabezadoTabla, 'DOCUMENTO');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $filaEncabezadoTabla, 'NOMBRES');

        $objPHPExcel->getActiveSheet()->getStyle("A$filaEncabezadoTabla:C$filaEncabezadoTabla")->getFont()->setBold(true);

        $estudiantes = $this->adminModel->consultarEstudiantesPorGrupo($codigoGrupo,$estado);


        $i = 1;

        $objPHPExcel->getActiveSheet()->mergeCells("C" . ($fila - 1) . ":D" . ($fila - 1));

        foreach ($estudiantes as $estudiante) {

            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $fila, $i);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $fila, $estudiante['documento']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $fila, $estudiante['nombres']);


            $objPHPExcel->getActiveSheet()->mergeCells("C" . ($fila) . ":D" . ($fila));

            $fila++;

            $i++;


        }


        $objPHPExcel->getActiveSheet()->getStyle('A' . $filaEncabezadoTabla . ':D' . ($fila - 1))->applyFromArray($bordes);
        $nombreArchivo = 'LISTADO-DE-ESTUDIANTES-' . $grupo['programa'] . '-' . $grupo['semestre'] . '-' . $grupo['jornada'] . '-'.$grupo['grupo'].'-'. time() . '.xlsx';
        $objPHPExcel->getActiveSheet()->setTitle("LISTADO");


        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $nombreArchivo . "");
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


    }


    function asignaturasSemestralesExcel(){


        $jornadas = $this->adminModel->consultarJornadas();


        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        //Start adding next sheets

        for ($i=0; $i<count($jornadas); $i++) {

            // Add new sheet

            $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating
            $objWorkSheet->setTitle($jornadas[$i]['nombre']);

            $asiganaturas = $this->adminModel->consultarCatalogoAsiganaturasSemestrales($jornadas[$i]['codigo']);




            $j=2;


            $objWorkSheet->setCellValue('A1', 'COD')
                         ->setCellValue('B1', 'PROGRAMA')
                         ->setCellValue('C1', 'ASINATURA')
                         ->setCellValue('D1', 'SEM');


            $objWorkSheet->getColumnDimension('B')->setWidth(30);
            $objWorkSheet->getColumnDimension('C')->setWidth(32);



            $objWorkSheet->getStyle("A1:D1")->getFont()->setBold(true);


            $aux="I";

            foreach ($asiganaturas as $asignatura  ){



                /*Separación de asignaturas por semestre*/
                if (strcmp($aux,$asignatura['semestre'])!==0){

                    $j++;
                }


                $objWorkSheet->setCellValue("A$j", $asignatura['codigo'])
                              ->setCellValue("B$j", $asignatura['programa'])
                              ->setCellValue("C$j",  $asignatura['asignatura'])
                              ->setCellValue("D$j",  $asignatura['semestre']);



                $aux= $asignatura['semestre'];

                $j++;

            }

        }



        $objPHPExcel->removeSheetByIndex(count($jornadas));









        $nombreArchivo = time() . '.xlsx';

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $nombreArchivo . "");
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');




    }


    function notasNocentesExcel()
    {

        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        //Start adding next sheets
        $i = 0;
        while ($i < 10) {

            // Add new sheet
            $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating

            //Write cells
            $objWorkSheet->setCellValue('A1', 'Hello' . $i)
                ->setCellValue('B2', 'world!')
                ->setCellValue('C1', 'Hello')
                ->setCellValue('D2', 'world!');

            // Rename sheet
            $objWorkSheet->setTitle("$i");

            $i++;
        }


        $nombreArchivo = time() . '.xlsx';

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $nombreArchivo . "");
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

    }


    function planillasMatriculas($docente, $jornada)
    {




        $cargasAcademicas = $this->adminModel->consultarCargaAcademicaPorDocente($docente, $this->peridoActual, $jornada);

        $objPHPExcel = new PHPExcel();

        $sheet = $objPHPExcel->getActiveSheet();

        foreach ($cargasAcademicas as $cargasAcademica) {



            $nombreDocente = $cargasAcademica['docente'];

            $grupos = $this->adminModel->consultarGruposPorAsignatura($cargasAcademica['codigo_programa'], $cargasAcademica['codigo_asignatura'], $corte = null, $cargasAcademica['codigo_jornada'], $cargasAcademica['periodo']);



            for ($i=0;$i< count($grupos) ;$i) {

                // echo $grupo['codigo_grupo']." - ".$cargasAcademica['codigo']."<br>";


                $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating




                $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                $gdImage = imagecreatefrompng(base_url('assets/img/logo.png'));
                $objDrawing->setImageResource($gdImage);
                $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
                $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
                $objDrawing->setResizeProportional(true);
                $objDrawing->setWidth(100);
                $objDrawing->setCoordinates('D2');
                $objDrawing->setWorksheet($objWorkSheet);


                $bordes = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );


                $objWorkSheet->getStyle('B2:B4')->getAlignment()->applyFromArray($this->textoCentrado);
                $objWorkSheet->getStyle("B2:E4")->getFont()->setBold(true);



                $objWorkSheet->setCellValue('B2', 'ESCUELA DE BELLAS ARTES Y HUMANIDADES DEL DEPARTAMENTO DE SUCRE');
                $objWorkSheet->setCellValue('B3', $cargasAcademica['programa'] . ' - ' . $cargasAcademica['semestre'] . " SEMESTRE"." - GRUPO ".$cargasAcademica['grupo']);
                $objWorkSheet->setCellValue('B4', 'JORNADA ' . $cargasAcademica['jornada']);




                $objWorkSheet->mergeCells('B2:C2');
                $objWorkSheet->mergeCells('B3:C3');
                $objWorkSheet->mergeCells('B4:C4');


                $objWorkSheet->getColumnDimension('A')->setWidth(4);
                $objWorkSheet->getColumnDimension('B')->setWidth(15);
                $objWorkSheet->getColumnDimension('C')->setWidth(60);
                $objWorkSheet->getColumnDimension('D')->setWidth(15);

                $objWorkSheet->getStyle("B6:B7")->getFont()->setBold(true);

                $objWorkSheet->setCellValue('B6', 'DOCENTE:');
                $objWorkSheet->setCellValue('B7', 'ASIGNATURA:');


                $objWorkSheet->setCellValue('C6', $cargasAcademica['docente']);
                $objWorkSheet->setCellValue('C7', $cargasAcademica['asignatura']);


                $fila = 10;
                $filaEncabezadoTabla = $fila - 1;


                $objWorkSheet->SetCellValue('A' . $filaEncabezadoTabla, '#');
                $objWorkSheet->SetCellValue('B' . $filaEncabezadoTabla, 'DOCUMENTO');
                $objWorkSheet->SetCellValue('C' . $filaEncabezadoTabla, 'NOMBRES');
                $objWorkSheet->SetCellValue('D' . $filaEncabezadoTabla, 'NOTA');

                $objWorkSheet->getStyle("D$filaEncabezadoTabla")->getAlignment()->applyFromArray($this->textoCentrado);


                $objWorkSheet->getStyle("B$filaEncabezadoTabla:E$filaEncabezadoTabla")->getFont()->setBold(true);


                $estudiantes = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($cargasAcademica['codigo'], null, $grupos[$i]['codigo_grupo']);

                $i = 1;

                foreach ($estudiantes as $estudiante) {

                    $objWorkSheet->SetCellValue('A' . $fila, $i);
                    $objWorkSheet->SetCellValue('B' . $fila, $estudiante['documento']);
                    $objWorkSheet->SetCellValue('C' . $fila, $estudiante['nombre']);
                    $objWorkSheet->SetCellValue('D' . $fila, "");

                    $fila++;

                    $i++;

                }


                $objWorkSheet->getStyle('A' . $filaEncabezadoTabla . ':D' . ($fila - 1))->applyFromArray($bordes);
                $nombreArchivo = 'ASIGNATURAS-'.$cargasAcademica['jornada'].'-' . $nombreDocente . '-' . time() . '.xlsx';



                $objWorkSheet->setTitle($cargasAcademica['abr_asignatura']."-".$cargasAcademica['grupo']);




            }




        }


        $objPHPExcel->removeSheetByIndex(count($cargasAcademicas));

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $nombreArchivo . "");
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


    }


    function planillas($docente, $jornada = null)
    {


        $cargasAcademicas = $this->adminModel->consultarCargaAcademicaPorDocente($docente, $this->peridoActual, $jornada);

        $objPHPExcel = new PHPExcel();

        $sheet = $objPHPExcel->getActiveSheet();

        foreach ($cargasAcademicas as $cargasAcademica) {



            $nombreDocente = $cargasAcademica['docente'];

            $grupos = $this->adminModel->consultarGruposPorAsignatura($cargasAcademica['codigo_programa'], $cargasAcademica['codigo_asignatura'], $corte = null, $cargasAcademica['codigo_jornada'], $cargasAcademica['periodo']);



            for ($i=0;$i< count($grupos) ;$i) {

                // echo $grupo['codigo_grupo']." - ".$cargasAcademica['codigo']."<br>";


                $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating




                $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                $gdImage = imagecreatefrompng(base_url('assets/img/logo.png'));
                $objDrawing->setImageResource($gdImage);
                $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
                $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
                $objDrawing->setResizeProportional(true);
                $objDrawing->setWidth(100);
                $objDrawing->setCoordinates('D2');
                $objDrawing->setWorksheet($objWorkSheet);


                $bordes = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );


                $objWorkSheet->getStyle('B2:B4')->getAlignment()->applyFromArray($this->textoCentrado);
                $objWorkSheet->getStyle("B2:E4")->getFont()->setBold(true);



                $objWorkSheet->setCellValue('B2', 'ESCUELA DE BELLAS ARTES Y HUMANIDADES DEL DEPARTAMENTO DE SUCRE');
                $objWorkSheet->setCellValue('B3', $cargasAcademica['programa'] . ' - ' . $cargasAcademica['semestre'] . " SEMESTRE"." - GRUPO ".$cargasAcademica['grupo']);
                $objWorkSheet->setCellValue('B4', 'JORNADA ' . $cargasAcademica['jornada']);




                $objWorkSheet->mergeCells('B2:C2');
                $objWorkSheet->mergeCells('B3:C3');
                $objWorkSheet->mergeCells('B4:C4');


                $objWorkSheet->getColumnDimension('A')->setWidth(4);
                $objWorkSheet->getColumnDimension('B')->setWidth(15);
                $objWorkSheet->getColumnDimension('C')->setWidth(60);
                $objWorkSheet->getColumnDimension('D')->setWidth(15);

                $objWorkSheet->getStyle("B6:B7")->getFont()->setBold(true);

                $objWorkSheet->setCellValue('B6', 'DOCENTE:');
                $objWorkSheet->setCellValue('B7', 'ASIGNATURA:');


                $objWorkSheet->setCellValue('C6', $cargasAcademica['docente']);
                $objWorkSheet->setCellValue('C7', $cargasAcademica['asignatura']);


                $fila = 10;
                $filaEncabezadoTabla = $fila - 1;


                $objWorkSheet->SetCellValue('A' . $filaEncabezadoTabla, '#');
                $objWorkSheet->SetCellValue('B' . $filaEncabezadoTabla, 'DOCUMENTO');
                $objWorkSheet->SetCellValue('C' . $filaEncabezadoTabla, 'NOMBRES');
                $objWorkSheet->SetCellValue('D' . $filaEncabezadoTabla, 'NOTA');

                $objWorkSheet->getStyle("D$filaEncabezadoTabla")->getAlignment()->applyFromArray($this->textoCentrado);


                $objWorkSheet->getStyle("B$filaEncabezadoTabla:E$filaEncabezadoTabla")->getFont()->setBold(true);


                $estudiantes = $this->adminModel->consultarEstudiantesMatriculadosPorAsignatura($cargasAcademica['codigo'], null, $grupos[$i]['codigo_grupo']);

                $i = 1;

                foreach ($estudiantes as $estudiante) {

                    $objWorkSheet->SetCellValue('A' . $fila, $i);
                    $objWorkSheet->SetCellValue('B' . $fila, $estudiante['documento']);
                    $objWorkSheet->SetCellValue('C' . $fila, $estudiante['nombre']);
                    $objWorkSheet->SetCellValue('D' . $fila, "");

                    $fila++;

                    $i++;

                }


                $objWorkSheet->getStyle('A' . $filaEncabezadoTabla . ':D' . ($fila - 1))->applyFromArray($bordes);


                $nombreArchivo = 'ASIGNATURAS-'.$cargasAcademica['jornada'].'-' . $nombreDocente . '-' . time() . '.xlsx';
                $objWorkSheet->setTitle($cargasAcademica['abr_asignatura']."-".$cargasAcademica['grupo']);




            }




        }


        $objPHPExcel->removeSheetByIndex(count($cargasAcademicas));

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $nombreArchivo . "");
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


    }



    function notasGrupalExcel($codigoGrupo)
    {


        $styleArray = array(
            'font' => array(
                'color' => array('rgb' => 'FF0000'),

            ));

        $abecedario = ["", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"];

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', "#");
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', "DOCUMENTO");
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', "NOMBRES");


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);

        $estudiantes = $this->adminModel->consultarEstudiantesPorGrupo($codigoGrupo);
        $grupo = $this->adminModel->consultarGrupo($codigoGrupo)[0];


        $i = 2;


        $numColumna = 2;
        $letraFila = 4;

        foreach ($estudiantes as $estudiante) {


            $notas = $this->estudianteModel->consultarMateriasMatriculadas($estudiante['documento'], $grupo['codigo_programa'], $grupo['periodo'], false);


            if (count($notas) > 0) {


                $nombreAsignaturas = [];


                foreach ($notas as $nota) {

                    $nombreAsignaturas[] = $nota['nombre'];

                }

                for ($j = 0; $j < count($nombreAsignaturas); $j++) {


                    $objPHPExcel->getActiveSheet()->SetCellValue($abecedario[$j + $letraFila] . "1", $nombreAsignaturas[$j]);


                }


                for ($j = 0; $j < count($nombreAsignaturas); $j++) {


                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $i);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $estudiante['documento']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $estudiante['nombres']);


                    $objPHPExcel->getActiveSheet()->SetCellValue($abecedario[$letraFila + $j] . "" . ($numColumna), $notas[$j]['nota_definitiva']);


                    if ($notas[$j]['nota_definitiva'] < 3) {


                        $objPHPExcel->getActiveSheet()->getStyle($abecedario[$letraFila + $j] . "" . ($numColumna))->applyFromArray($styleArray);


                    }


                }

                $numColumna++;


                $i++;

            }


        }


        $nombreArchivo = 'LISTADO-DE-NOTAS-POR-GRUPO' . time() . '.xlsx';
        $objPHPExcel->getActiveSheet()->setTitle("LISTADO");

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $nombreArchivo . "");
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


    }

    function notasIndividualPDF()
    {


        $documento = $this->input->post("documento");
        $nombres = $this->input->post("nombres");


        $pdf = new PDF();
        $pdf->SetTitle("REPORTE NOTAS " . $nombres, true);


        $programasMatriculados = $this->estudianteModel->consultarProgramasMatriculados($documento);

        foreach ($programasMatriculados as $programaMatriculado) {


            $pdf->AliasNbPages();
            $pdf->AddPage();


            $pdf->SetFont('Arial', 'B', 12);

            $yTitulo = 50;

            $pdf->SetXY(105, $yTitulo);
            $pdf->Cell(2, 14, "NOTAS");


            $pdf->SetXY(85, $yTitulo + 4);
            $pdf->Cell(2, 14, $programaMatriculado['descripcion_periodo']);


            $yDatosEstudiante = 70;
            $xDatosEstudiante = 30;
            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante);
            $pdf->Cell(2, 14, "ESTUDIANTE: ");


            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante + 5);
            $pdf->Cell(2, 14, "PROGRAMA: ");
            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante + 10);




            $pdf->Cell(2, 14, "SEMESTRE: ");
            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante + 15);
            $pdf->Cell(2, 14, "JORNADA: ");

            $pdf->SetFont('Arial', '', 12);


            $pdf->SetXY($xDatosEstudiante + 30, $yDatosEstudiante);
            $pdf->Cell(2, 14, utf8_decode($nombres));
            $pdf->SetXY($xDatosEstudiante + 28, $yDatosEstudiante + 5);
            $pdf->Cell(2, 14, utf8_decode($programaMatriculado['nombre']));
            $pdf->SetXY($xDatosEstudiante + 26, $yDatosEstudiante + 10);
            $pdf->Cell(2, 14, utf8_decode($programaMatriculado['nombre_semestre'])." - ".$programaMatriculado['grupo']);
            $pdf->SetXY($xDatosEstudiante + 24, $yDatosEstudiante + 15);
            $pdf->Cell(2, 14, utf8_decode($programaMatriculado['nombre_jornada']));


            $pdf->Ln();

            $pdf->SetFont('Arial', 'b', 12);
            // $pdf->SetXY($xDatosEstudiante+100, $yDatosEstudiante+15);
            $header = array('ASIGANTURA', 'NOTA');
            // Data loading
            $data[] = array(utf8_decode('DISEÑO WEB'), '4');
            $data[] = array(utf8_decode('DISEÑO WEB'), '4');

            $pdf->SetXY($xDatosEstudiante + 70, $yDatosEstudiante + 15);


            // $pdf->SetDrawColor(27, 121, 165);

            $pdf->SetFillColor(215, 215, 215); // establece el color del fondo de la celda (en este caso es AZUL


            $tamCeldaAsignatura = 140;


            $yNotas = 114;
            $xNotas = 170;

            $pdf->SetXY(30, 107);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell($tamCeldaAsignatura, 7, "ASIGNATURA", 1, 0, "L", true);
            $pdf->SetXY($xNotas, 107);
            $pdf->Cell(15, 7, "NOTA", 1, 0, "C", true);


            $pdf->Ln();

            $pdf->SetFont('Arial', '', 11);


            $notas = $this->estudianteModel->consultarMateriasMatriculadas($documento, $programaMatriculado['codigo'], $programaMatriculado['periodo']);


            $promedioNotas = 0;


            if (count($notas) > 0) {


                foreach ($notas as $nota) {


                    $pdf->SetXY(30, $yNotas);
                    $pdf->Cell($tamCeldaAsignatura, 7, utf8_decode($nota['nombre']), 1, 0, "L", false);
                    $pdf->SetXY($xNotas, $yNotas);
                    $pdf->Cell(15, 7, $nota['nota_definitiva'], 1, 0, "C", false);
                    $pdf->Ln();


                    $promedioNotas += $nota['nota_definitiva'];


                    $yNotas += 7;

                }


                $pdf->SetXY(30, $yNotas);
                $pdf->SetFont('Arial', 'B', 11);
                $pdf->Cell($tamCeldaAsignatura, 7, "PROMEDIO FINAL", 1, 0, "L", true);
                $pdf->SetXY($xNotas, $yNotas);
                $pdf->Cell(15, 7, round(($promedioNotas / count($notas)), 1), 1, 0, "C", true);
            }

        }


        $pdf->Output();
    }

    function notasGrupalPDF($codigo)
    {


        $grupo = $this->adminModel->consultarGrupo($codigo)[0];
        $periodo = $this->adminModel->consultarPeriodos($grupo['periodo'])[0];

        $estudiantes = $this->adminModel->consultarEstudiantesPorGrupo($codigo);


        $pdf = new PDF();
        $pdf->SetTitle("REPORTE NOTAS " . $grupo['programa'] . "-" . $grupo['semestre'] . " SEM-" . $grupo['jornada'] . "-" . $grupo['periodo'], true);

        foreach ($estudiantes as $estudiante) {


            $pdf->AliasNbPages();
            $pdf->AddPage();


            $pdf->SetFont('Arial', 'B', 12);

            $yTitulo = 50;

            $pdf->SetXY(105, $yTitulo);
            $pdf->Cell(2, 14, "NOTAS");


            $pdf->SetXY(85, $yTitulo + 4);
            $pdf->Cell(2, 14, $periodo['descripcion']);


            $yDatosEstudiante = 60;
            $xDatosEstudiante = 30;
            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante);
            $pdf->Cell(2, 14, "ESTUDIANTE: ");


            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante + 5);
            $pdf->Cell(2, 14, "PROGRAMA: ");
            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante + 10);
            $pdf->Cell(2, 14, "SEMESTRE: ");
            $pdf->SetXY($xDatosEstudiante, $yDatosEstudiante + 15);
            $pdf->Cell(2, 14, "JORNADA: ");

            $pdf->SetFont('Arial', '', 12);

            $pdf->SetXY($xDatosEstudiante + 30, $yDatosEstudiante);
            $pdf->Cell(2, 14, utf8_decode($estudiante['nombres']));
            $pdf->SetXY($xDatosEstudiante + 28, $yDatosEstudiante + 5);
            $pdf->Cell(2, 14, utf8_decode($grupo['programa']));
            $pdf->SetXY($xDatosEstudiante + 26, $yDatosEstudiante + 10);
            $pdf->Cell(2, 14, utf8_decode($grupo['nombre_semestre'])." - ".$grupo['grupo']);
            $pdf->SetXY($xDatosEstudiante + 24, $yDatosEstudiante + 15);
            $pdf->Cell(2, 14, utf8_decode($grupo['jornada']));


            $pdf->Ln();

            $pdf->SetFont('Arial', 'b', 12);


            $pdf->SetXY($xDatosEstudiante + 70, $yDatosEstudiante + 15);


            // $pdf->SetDrawColor(27, 121, 165);

            $pdf->SetFillColor(215, 215, 215); // establece el color del fondo de la celda (en este caso es AZUL


            $tamCeldaAsignatura = 140;


            $yNotas = 100;
            $xNotas = 163;
            $xAsignatura = 23;
            $xObs = $xNotas + 15;

            $pdf->SetXY($xAsignatura, 93);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell($tamCeldaAsignatura, 7, "ASIGNATURA", 1, 0, "L", true);
            $pdf->SetXY($xNotas, 93);
            $pdf->Cell(20, 7, "NOTA", 1, 0, "C", true);

            //  $pdf->SetXY($xObs, 107);
            //  $pdf->Cell(15, 7, "OBS", 1, 0, "C", true);


            //$pdf->Cell($x2, 4, "", 1, 0, 'C', True);


            $pdf->Ln();

            $pdf->SetFont('Arial', '', 11);


            $notas = $this->estudianteModel->consultarMateriasMatriculadas($estudiante['documento'], $grupo['codigo_programa'], $grupo['periodo']);


            $promedioNotas = 0;

            $numAsignaturasHabilitadas = 0;


            if (count($notas) > 0) {


                foreach ($notas as $nota) {


                    $pdf->SetFont('Arial', '', 11);
                    $pdf->SetXY($xAsignatura, $yNotas);
                    $pdf->Cell($tamCeldaAsignatura, 7, utf8_decode($nota['nombre']), 1, 0, "L", false);
                    $pdf->SetXY($xNotas, $yNotas);
                    $pdf->Cell(20, 7, "     " . $nota['nota_definitiva'], 1, 0, "", false);


                    $pdf->SetXY($xNotas + 8, $yNotas);
                    $pdf->SetFont('Arial', '', 7);
                    $pdf->Cell(20, 7, "    " . $nota['observacion'], 0, 0, "", false);


                    // $pdf->SetXY($xObs, $yNotas);

                    // $pdf->Cell(15, 7, "HB", 1, 0, "C", false);

                    $pdf->Ln();


                    $promedioNotas += $nota['nota_definitiva'];


                    $yNotas += 7;


                    if ($nota['observacion'] == "HB") {

                        $numAsignaturasHabilitadas++;
                    }


                }

                $pdf->SetXY($xAsignatura, $yNotas);
                $pdf->SetFont('Arial', 'B', 11);
                $pdf->Cell($tamCeldaAsignatura, 7, "PROMEDIO FINAL", 1, 0, "L", true);
                $pdf->SetXY($xNotas, $yNotas);
                $pdf->Cell(20, 7, round(($promedioNotas / count($notas)), 1), 1, 0, "C", true);


            }


            if ($numAsignaturasHabilitadas > 0) {


                $pdf->SetFont('Arial', 'I', 9);
                $pdf->SetXY($xAsignatura, $yNotas + 10);
                $pdf->Cell(20, 7, utf8_decode("HB - Nota Habilitación"), 0, 0, "L", false);

            }


        }
        $pdf->Output();
    }



    function x(){



         $jormadas = $this->adminModel->consultarCatalogoAsiganaturasSemestrales();

        $this->exportCSV(["jornada","programa","asignatura","semestre"],$jormadas);


    }




    public function exportCSV($header,$usersData){
        // file name
        $filename = 'users_'.date('Ymd').'.csv';

        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');

        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");

        // get data

        // file creation
        $file = fopen('php://output', 'w');


        fputcsv($file, $header);
        foreach ($usersData as $key=>$line){
            fputcsv($file,$line);
        }
        fclose($file);
        exit;
    }

}

