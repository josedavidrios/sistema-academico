<?php



function encrypt($string)
{
    $key = "lHk954di_-\\";


    $result = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) + ord($keychar));
        $result .= $char;
    }

    $result = base64_encode($result);
     return $result;
}




function decrypt($string)
{

    $key = "lHk954di_-\\";


    $result = '';
    $string = base64_decode($string);
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) - ord($keychar));
        $result .= $char;
    }
    return $result;
}


function password_moodle($clave)
{


    $fasthash = true;

    $options = ($fasthash) ? array('cost' => 4) : array();

    $generatedhash = password_hash($clave, PASSWORD_DEFAULT, $options);


    return $generatedhash;

}


function url_encrypt($string)
{
    $key = "lHk954di_-\\";


    $result = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) + ord($keychar));
        $result .= $char;
    }

    $result = base64_encode($result);
    $result = str_replace(array('+', '/', '='), array('-', '_', '.'), $result);
    return $result;
}




 function url_decrypt($string)
{

    $key = "lHk954di_-\\";

    $string = str_replace(array('-', '_', '.'), array('+', '/', '='), $string);
    $result = '';
    $string = base64_decode($string);
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) - ord($keychar));
        $result .= $char;
    }
    return $result;
}
