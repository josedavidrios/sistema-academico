<?php



function to_excel($query,$fields=null, $filename='exceloutput')
{

    $headers = ''; // just creating the var for field headers to append to below
    $data = ''; // just creating the var for field data to append to below

    $obj =& get_instance();

    header("Content-type: application/x-msdownload");
    header("Content-Disposition: attachment; filename=$filename.xls");


    if (is_null($fields)){


        $fields= $query->list_fields();


    }

    if ($query->num_rows() == 0) {
        echo '<p>The table appears to have no data.</p>';
    } else {
        foreach ($fields as $field) {
            $headers .= $field . "\t";
        }

        foreach ($query->result() as $row) {
            $line = '';
            foreach ($row as $value) {
                if ((!isset($value)) OR ($value == "")) {
                    $value = "\t";
                } else {
                    $value = str_replace('"', '""', $value);
                    $value = '"' . $value . '"' . "\t";
                }
                $line .= $value;
            }
            $data .= trim($line) . "\n";
        }

        $data = str_replace("\r", "", $data);


        echo mb_convert_encoding("$headers\n$data", 'utf-16', 'utf-8');
    }
}



function to_csv($data,$file_name="d.csv")
{
    $file_name = 'student_details_on_'.date('Ymd').'.csv';
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$file_name");
    header("Content-Type: application/csv;");


    // get data
    $student_data = $this->export_csv_model->fetch_data();

    // file creation
    $file = fopen('php://output', 'w');

    $header = array("Student Name","Student Phone");
    fputcsv($file, $header);
    foreach ($data as $key => $value)
    {
        fputcsv($file, $value);
    }
    fclose($file);
    exit;
}


 function exportCSV($usersData, $header){
    // file name
    $filename = 'users_'.date('Ymd').'.csv';
   // header("Content-Description: File Transfer");

    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv; ");


    // get data


    // file creation
    $file = fopen('php://output', 'w');




    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){
        fputcsv($file,$line);
    }
    fclose($file);
    exit;
}



