<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


function input_is_checked($input)
{

    if (is_null($input))

        return 0;

    else

        return 1;


}

function cortes($corte){


    $desc="";

    if ($corte=="1"){

        $desc = "PRIMER CORTE";

    }else if ($corte=="2"){

        $desc = "SEGUNDO CORTE";


    }else if ($corte=="3"){

        $desc = "TERCER CORTE";
    }else{

        $desc = "DEFINITIVA";

    }


    return $desc;
}


function formato_si_no2($valor){


    if($valor==1){


        return '<span class="label label-success" title="Sí"> <i  class="fa fa-check"></i></span>';


    }else if($valor==0){

        return '<span class="label label-danger" title="No"><i class="fa fa-times "></i></span>';


    }


}

function formato_si_no($valor){


    if($valor=="SI"){


        return '<span class="label label-success" title="Sí"> <i  class="fa fa-check"></i></span>';


    }else if($valor=="NO"){

        return '<span class="label label-danger" title="No"><i class="fa fa-times "></i></span>';


    }


}


function estilo_nota_definitiva($nota){



    $aux = '<strong>'.$nota.'</strong>';

    if ($nota<3){

        $aux ='<strong class="text-danger">'.$nota.'</strong>';

    }



    return $aux;

}


function formato_estado_matricula($valor){


    if($valor=="1"){


        return '<span class="label label-success" title="Activa"> <i  class="fa fa-check"></i></span>';

    }else if($valor=="0"){

        return '<span class="label label-danger" title="Cancelada"><i class="fa fa-times "></i></span>';


    }


}

function formato_activo($valor){


    if($valor=="1"){


        return '<span class="label label-success">SI</span>';

    }else if($valor=="0"){

        return '<span class="label label-danger">NO</span>';


    }


}



function isLocalHost(){


    $pos=  strpos(base_url(), "http://localhost");

    return $pos === false ? 0:1;

}



function checked($input)
{

    if ($input==1)

       return "checked";

    else

        return "";


}

function to_uppercase_and_trim($str){


    return mb_strtoupper(trim($str));


}





function remove_accents($cadena) {
    $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
    $texto = str_replace($no_permitidas, $permitidas ,$cadena);
    return $texto;
}



