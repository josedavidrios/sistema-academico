<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'sesion';



$route['docente/matriculas/listado/(:any)'] = 'docente/vistaPlanillas/$1';

$route['estudiante'] = 'estudiante';
$route['estudiante/actualizar-datos-personales'] = 'estudiante/vistaActuliacionDeDatosPersonales';


$route['admin/configuracion'] = 'superAdmin/vistaConfiguracion';
$route['admin/docentes/evaluacion'] = 'superAdmin/vistaEvaluacionDocente';


$route['admin/inscripciones'] = 'admin/vistaListadoDeInscripciones';
$route['admin/matriculas/primer-ingreso'] = 'admin/vistaMatriculasPrimerIngreso';

$route['admin/matriculas/asignatura/grupal'] = 'admin/vistaMatriculaAsignatura';
$route['admin/matriculas/asignatura/individual'] = 'admin/vistaMatriculaAsignaturaIndividual';


$route['admin/matriculas'] = 'admin/vistaMatriculas';
$route['admin/matriculas/listado'] = 'admin/vistaListadoMatricula';
$route['admin/matriculas/consultar'] = 'admin/vistaConsultarMatricula';
$route['admin/matriculas/academicas/matricular'] = 'admin/vistaMatriculasAcademicas';

$route['admin/matriculas/aulavirtual/listado'] = 'admin/vistaMatriculasAulaVirtual';

$route['admin/matriculas/aulavirtual/cursos'] = 'admin/vistaMatriculasCursosAulaVirtual';


$route['admin/estudiante'] = 'admin/vistaEstudiantes';

$route['admin/docentes/cargas-academicas'] = 'admin/vistaCargasAcademicas';
$route['admin/docentes/cargas-academicas/crear'] = 'admin/vistaCrearCargaAcademicasDocentes';
$route['admin/docentes'] = 'admin/vistaGestionarDocente';
$route['admin/docentes/planillas'] = 'admin/vistaPlanillas';


$route['admin/notas/consultar'] = 'admin/vistaConsultarNotas';
$route['admin/notas/reporte/grupal/pdf'] = 'admin/vistaReporteNotasGrupal';
$route['admin/notas/reporte/grupal/excel'] = 'admin/vistaReporteNotasExcel';
$route['admin/notas/reporte/individual/pdf'] = 'admin/vistaReporteNotasIndividual';
$route['admin/notas/habilitar'] = 'admin/vistaRegistrarHabilitacion';

$route['admin/asignaturas'] = 'admin/vistaGestionarAsignaturas';

$route['admin/asignaturas/semestrales'] = 'admin/vistaGestionarAsignaturasSemestrales';

$route['admin/grupos/(:any)'] = 'admin/vistaGestionarGrupos/$1';

$route['admin/periodos'] = 'superAdmin/vistaPeriodos';



$route['admin/notas/editar/(:any)'] = 'superAdmin/vistaEditarNotas/$1';
$route['admin/notas/editar/(:any)'] = 'superAdmin/vistaEditarNotas/$1';






$route['cambiar-clave-de-acceso'] = 'usuario';

$route['inscripciones'] = 'usuario/inscripciones';

$route['404_override'] = 'usuario/error404';
$route['translate_uri_dashes'] = FALSE;


